package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.MattressinfosDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 床垫的实时数据(tst_mattressinfos)数据Mapper
 *
 * @author psk
 * @since 2022-09-14 15:19:57
 * @description 由 psk 创建
*/
@Mapper
public interface MattressinfosMapper extends BaseMapper<MattressinfosDO> {

}
