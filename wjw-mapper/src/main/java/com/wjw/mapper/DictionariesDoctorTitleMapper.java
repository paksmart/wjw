package com.wjw.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesDoctorTitle;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/10/1 15:29
* @version 1.0
*/

@Mapper
public interface DictionariesDoctorTitleMapper extends BaseMapper<DictionariesDoctorTitle> {
}
