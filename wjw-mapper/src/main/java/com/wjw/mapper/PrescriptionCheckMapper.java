package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.PrescriptionCheckDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 15:39
* @version 1.0
*/

@Mapper
public interface PrescriptionCheckMapper extends BaseMapper<PrescriptionCheckDO> {
}
