package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: Mapper 层
* @author pansh
* @date 2022/10/22 10:38
* @version 1.0
*/


@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
}
