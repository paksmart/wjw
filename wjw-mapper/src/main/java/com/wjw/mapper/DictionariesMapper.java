package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesSubject;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* @description:
* @author pansh
* @date 2022/9/23 9:13
* @version 1.0
*/

@Mapper
@Repository
public interface DictionariesMapper extends BaseMapper<DictionariesSubject> {
}
