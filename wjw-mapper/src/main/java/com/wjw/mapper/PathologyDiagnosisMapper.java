package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.PathologyDiagnosisDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/29 15:41
* @version 1.0
*/

@Mapper
public interface PathologyDiagnosisMapper extends BaseMapper<PathologyDiagnosisDO> {
}
