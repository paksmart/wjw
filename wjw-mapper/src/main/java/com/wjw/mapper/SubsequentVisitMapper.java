package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.SubsequentVisitDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 14:23
* @version 1.0
*/

@Mapper
public interface SubsequentVisitMapper extends BaseMapper<SubsequentVisitDO> {
}
