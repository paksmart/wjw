package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.PrescriptionCommentDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/26 15:03
* @version 1.0
*/

@Mapper
public interface PrescriptionCommentMapper extends BaseMapper<PrescriptionCommentDO> {
}
