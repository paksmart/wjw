package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.EvaluateRecordsDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 16:19
* @version 1.0
*/


@Mapper
public interface EvaluateRecordsMapper extends BaseMapper<EvaluateRecordsDO> {
}
