package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.patientinfoPatientsDo;
import org.apache.ibatis.annotations.Mapper;

/**
* @description:
* @author pansh
* @date 2022/9/23 16:02
* @version 1.0
*/


@Mapper
public interface patientinfoPatientsMapper extends BaseMapper<patientinfoPatientsDo> {
}
