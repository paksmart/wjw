package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesIcd;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pansh
 * @version 1.0
 * @description:
 * @date 2022/10/6 13:44
 */

@Mapper
public interface DictionariesIcdMapper extends BaseMapper<DictionariesIcd> {

}
