package com.wjw.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesCertType;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pansh
 * @version 1.0
 * @description: psk
 * @date 2022/10/1 13:49
 */

@Mapper
public interface DictionariesCertTypeMapper extends BaseMapper<DictionariesCertType> {

}
