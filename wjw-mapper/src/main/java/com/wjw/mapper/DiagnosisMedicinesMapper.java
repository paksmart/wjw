package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.DiagnosisMedicinesDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: mapper
* @author psk
* @date 2022/10/18 10:30
* @version 1.0
*/

@Mapper
public interface DiagnosisMedicinesMapper extends BaseMapper<DiagnosisMedicinesDO> {
}
