package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.RemoteClinicRecordDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 10:35
* @version 1.0
*/

@Mapper
public interface RemoteClinicRecordMapper extends BaseMapper<RemoteClinicRecordDO> {
}
