package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.ComplaintRecordsDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/21 9:06
* @version 1.0
*/

@Mapper
public interface ComplaintRecordsMapper extends BaseMapper<ComplaintRecordsDO> {
}
