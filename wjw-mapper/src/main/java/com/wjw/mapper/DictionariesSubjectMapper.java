package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesSubject;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/10/5 15:38
* @version 1.0
*/

@Mapper
public interface DictionariesSubjectMapper extends BaseMapper<DictionariesSubject> {
}
