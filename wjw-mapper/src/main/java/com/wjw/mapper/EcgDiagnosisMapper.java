package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.EcgDiagnosisDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/27 14:12
* @version 1.0
*/

@Mapper
public interface EcgDiagnosisMapper extends BaseMapper<EcgDiagnosisDO> {
}
