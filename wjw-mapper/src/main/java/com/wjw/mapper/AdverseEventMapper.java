package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.AdverseEventDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 14:35
* @version 1.0
*/

@Mapper
public interface AdverseEventMapper extends BaseMapper<AdverseEventDO> {
}
