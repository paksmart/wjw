package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.EcgDiagnosisAppliedListDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/27 14:16
* @version 1.0
*/

@Mapper
public interface EcgDiagnosisAppliedListMapper extends BaseMapper<EcgDiagnosisAppliedListDO> {
}
