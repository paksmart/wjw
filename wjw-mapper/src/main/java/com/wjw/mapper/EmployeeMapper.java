package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: mapper
* @author psk
* @date 2022/10/22 13:40
* @version 1.0
*/

@Mapper
public interface EmployeeMapper  extends BaseMapper<Employee> {
}
