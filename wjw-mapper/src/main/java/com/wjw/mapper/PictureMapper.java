package com.wjw.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
* @description:
* @author pansh
* @date 2022/10/4 15:34
* @version 1.0
*/

@Mapper
public interface PictureMapper {

    String getImageById(String id);
}
