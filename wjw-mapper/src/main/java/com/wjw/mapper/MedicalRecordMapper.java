package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.MedicalRecordDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 15:33
* @version 1.0
*/

@Mapper
public interface MedicalRecordMapper extends BaseMapper<MedicalRecordDO> {
}
