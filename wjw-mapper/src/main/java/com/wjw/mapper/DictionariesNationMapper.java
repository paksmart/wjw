package com.wjw.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesNation;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/10/1 15:11
* @version 1.0
*/

@Mapper
public interface DictionariesNationMapper extends BaseMapper<DictionariesNation> {
}
