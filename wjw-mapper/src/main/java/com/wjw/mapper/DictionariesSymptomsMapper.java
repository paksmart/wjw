package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesSymptoms;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/10/5 13:49
* @version 1.0
*/

@Mapper
public interface DictionariesSymptomsMapper extends BaseMapper<DictionariesSymptoms> {
}
