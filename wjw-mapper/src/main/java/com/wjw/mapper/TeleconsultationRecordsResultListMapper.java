package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.TeleconsultationRecordsResultListDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/29 10:33
* @version 1.0
*/

@Mapper
public interface TeleconsultationRecordsResultListMapper extends BaseMapper<TeleconsultationRecordsResultListDO> {
}
