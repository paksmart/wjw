package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.PatientinfoDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/15 9:19
* @version 1.0
*/

@Mapper
public interface PatientInfoMapper extends BaseMapper<PatientinfoDO> {
}
