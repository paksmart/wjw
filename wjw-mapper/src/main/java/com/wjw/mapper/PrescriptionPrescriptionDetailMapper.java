package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.PrescriptionPrescriptionDetailDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description:
* @author pansh
* @date 2022/9/24 17:37
* @version 1.0
*/


@Mapper
public interface PrescriptionPrescriptionDetailMapper extends BaseMapper<PrescriptionPrescriptionDetailDO> {
}
