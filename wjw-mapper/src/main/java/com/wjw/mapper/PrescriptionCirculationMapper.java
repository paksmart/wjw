package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.PrescriptionCirculationDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 18:53
* @version 1.0
*/

@Mapper
public interface PrescriptionCirculationMapper extends BaseMapper<PrescriptionCirculationDO> {

}
