package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.ImagingDiagnosisDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description:
* @author pansh
* @date 2022/9/27 9:43
* @version 1.0
*/

@Mapper
public interface ImagingDiagnosisMapper extends BaseMapper<ImagingDiagnosisDO> {
}
