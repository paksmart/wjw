package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.ImagingDiagnosisAppliedListDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/27 9:56
* @version 1.0
*/

@Mapper
public interface ImagingDiagnosisAppliedListMapper extends BaseMapper<ImagingDiagnosisAppliedListDO> {
}
