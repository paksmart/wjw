package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.domain.FilingStaffDO;
import org.apache.ibatis.annotations.Mapper;

/**
* @description: psk
* @author pansh
* @date 2022/9/16 16:36
* @version 1.0
*/

@Mapper
public interface FilingStaffMapper extends BaseMapper<FilingStaffDO> {
    String getImageById(Long id);
}
