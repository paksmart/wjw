package com.wjw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjw.center.admin.dictionaries.DictionariesSex;
import org.apache.ibatis.annotations.Mapper;

/**
* @description:
* @author pansh
* @date 2022/10/5 9:43
* @version 1.0
*/

@Mapper
public interface DictionariesSexMapper extends BaseMapper<DictionariesSex> {
}
