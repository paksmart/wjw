package com.wjw.admin;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p>
 * Application
 * </p>
 *
 * @author psk
 * @since 2022-09-14 15:19:57
 * @description 由 psk 创建
 **/
@MapperScan("com.wjw.mapper")
@Slf4j
@SpringBootApplication
//@ServletComponentScan("com.wjw.common.myFilter")
@ComponentScan("com.wjw")
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
        log.info("项目启动");
    }
}
