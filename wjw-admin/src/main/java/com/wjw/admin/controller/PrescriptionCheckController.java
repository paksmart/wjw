package com.wjw.admin.controller;

import com.wjw.center.admin.domain.PrescriptionCheckDO;
import com.wjw.center.admin.param.PrescriptionCheckParam;
import com.wjw.common.admin.PrescriptionCheckDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.PrescriptionCheckService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 15:32
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/prescriptionCheck")
@Api(value = "诊疗管理:互联网在线处方审核信息",tags = "诊疗管理:互联网在线处方审核信息")
public class PrescriptionCheckController {

    private final PrescriptionCheckService prescriptionCheckService;

    @ApiOperation(value = "新增互联网在线处方审核信息")
    @PostMapping("/savePrescriptionCheck")
    public ResponseResult savePrescriptionCheck(@RequestBody PrescriptionCheckDO prescriptionCheckDO) {
        return prescriptionCheckService.savePrescriptionCheck(prescriptionCheckDO);
    }

    @ApiOperation(value = "查询互联网在线处方审核信息")
    @PostMapping("/getPrescriptionCheck")
    public ResponseResult getPrescriptionCheck(@RequestBody PrescriptionCheckParam param) {
        return prescriptionCheckService.getPrescriptionCheck(param);
    }

    @ApiOperation(value = "查询互联网在线处方审核信息详情")
    @PostMapping("/getByIdPrescriptionCheck")
    public ResponseResult getByIdPrescriptionCheck(Long platformId) {
        return prescriptionCheckService.getByIdPrescriptionCheck(platformId);
    }

    @ApiOperation(value = "分页查询互联网在线处方审核信息")
    @PostMapping("/getPagePrescriptionCheck")
    public ResponseResult getPagePrescriptionCheck(@RequestBody PrescriptionCheckDto dto) {
        return prescriptionCheckService.getPagePrescriptionCheck(dto);
    }

    @ApiOperation(value = "删除互联网在线处方审核信息")
    @PostMapping("/deletePrescriptionCheck")
    public ResponseResult deletePrescriptionCheck(Long platformId) {
        return prescriptionCheckService.deletePrescriptionCheck(platformId);
    }

    @ApiOperation(value = "修改互联网在线处方审核信息")
    @PostMapping("/updatePrescriptionCheck")
    public ResponseResult updatePrescriptionCheck(@RequestBody PrescriptionCheckDO prescriptionCheckDO) {
        return prescriptionCheckService.updatePrescriptionCheck(prescriptionCheckDO);
    }
}
