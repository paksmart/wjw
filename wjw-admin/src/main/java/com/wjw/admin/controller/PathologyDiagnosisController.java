package com.wjw.admin.controller;

import com.wjw.center.admin.domain.PathologyDiagnosisDO;
import com.wjw.common.admin.PathologyDiagnosisDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.PathologyDiagnosisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/29 15:39
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@RestController
@RequestMapping("/pathologyDiagnosis")
@Api(value = "诊疗管理:互联网医院远程病理诊断记录监管",tags = "诊疗管理:互联网医院远程病理诊断记录监管")
public class PathologyDiagnosisController {

    private final PathologyDiagnosisService pathologyDiagnosisService;

    @ApiOperation(value = "新增互联网医院远程病理诊断记录监管")
    @PostMapping("/savePathologyDiagnosis")
    public ResponseResult savePathologyDiagnosis(@RequestBody PathologyDiagnosisDO pathologyDiagnosisDO) {
        return pathologyDiagnosisService.savePathologyDiagnosis(pathologyDiagnosisDO);
    }

    @ApiOperation(value = "查询互联网医院远程病理诊断记录监管")
    @PostMapping("/getPathologyDiagnosis")
    public ResponseResult getPathologyDiagnosis(@RequestBody PathologyDiagnosisDO pathologyDiagnosisDO) {
        return pathologyDiagnosisService.getPathologyDiagnosis(pathologyDiagnosisDO);
    }

    @ApiOperation(value = "查询互联网医院远程病理诊断记录监管详情")
    @PostMapping("/getByIdPathologyDiagnosis")
    public ResponseResult getByIdPathologyDiagnosis(Long platformId) {
        return pathologyDiagnosisService.getByIdPathologyDiagnosis(platformId);
    }

    @ApiOperation(value = "分页查询互联网医院远程病理诊断记录监管")
    @PostMapping("/getPagePathologyDiagnosis")
    public ResponseResult getPagePathologyDiagnosis(@RequestBody PathologyDiagnosisDto dto) {
        return pathologyDiagnosisService.getPagePathologyDiagnosis(dto);
    }

    @ApiOperation(value = "删除互联网医院远程病理诊断记录监管")
    @PostMapping("/deletePathologyDiagnosis")
    public ResponseResult deletePathologyDiagnosis(Long platformId) {
        return pathologyDiagnosisService.deletePathologyDiagnosis(platformId);
    }

    @ApiOperation(value = "修改互联网医院远程病理诊断记录监管")
    @PostMapping("/updatePathologyDiagnosis")
    public ResponseResult updatePathologyDiagnosis(@RequestBody PathologyDiagnosisDO pathologyDiagnosisDO) {
        return pathologyDiagnosisService.updatePathologyDiagnosis(pathologyDiagnosisDO);
    }
}
