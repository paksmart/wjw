package com.wjw.admin.controller;

import com.aliyun.oss.model.ObjectMetadata;
import com.wjw.admin.config.OssTemplate;
import com.wjw.admin.config.OssTemplates;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @description: 上传文件控制器
 * @author: nxq email: niuxiangqian163@163.com
 * @createDate: 2020/12/19 4:09 下午
 * @updateUser: nxq email: niuxiangqian163@163.com
 * @updateDate: 2020/12/19 4:09 下午
 * @updateRemark:
 * @version: 1.0
 **/
@RestController
@Api(value = "文件上传",tags = "文件上传")
@Slf4j
public class UpLoadController {

    @Autowired
    private OssTemplate ossTemplate;

    @Autowired
    private OssTemplates ossTemplates;

    @ApiOperation("上传图片")
    @PostMapping("/uploadPic")
    public ResponseResult uploadPic(MultipartFile file) throws IOException {
        if (file.getSize() > 0) {
            //将接受的文件使用oss上传到阿里云
            String filePath = ossTemplate.upload(file.getOriginalFilename(), file.getInputStream());
            System.out.println(filePath);
            //上传之后访问地址返回给前端
            return ResponseResult.okResult(filePath);
        }
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
    }

    @ApiOperation("上传文件")
    @PostMapping("/uploadFile")
    public ResponseResult uploadFile(MultipartFile file) throws IOException {
        if (file.getSize() > 0) {
            //将接受的文件使用oss上传到阿里云
            String filePath = ossTemplates.upload(file.getOriginalFilename(), file.getInputStream());
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentDisposition("attachment");

            System.out.println(filePath);
            //上传之后访问地址返回给前端
            return ResponseResult.okResult(filePath);
        }
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
    }
}

