package com.wjw.admin.controller;

import com.wjw.center.admin.domain.EcgDiagnosisDO;
import com.wjw.common.admin.EcgDiagnosisDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.EcgDiagnosisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description:
* @author pansh
* @date 2022/9/27 14:10
* @version 1.0
*/

@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RequestMapping("/EcgDiagnosis")
@Api(value = "诊疗管理:互联网医院远程心电图诊断记录监管",tags = "诊疗管理:互联网医院远程心电图诊断记录监管")
public class EcgDiagnosisController {

    private final EcgDiagnosisService ecgDiagnosisService;

    @ApiOperation(value = "新增互联网医院远程心电图诊断记录监管")
    @PostMapping("/saveEcgDiagnosis")
    public ResponseResult saveEcgDiagnosis(@RequestBody EcgDiagnosisDO ecgDiagnosisDO) {
        return ecgDiagnosisService.saveEcgDiagnosis(ecgDiagnosisDO);
    }

    @ApiOperation(value = "查询互联网医院远程心电图诊断记录监管")
    @PostMapping("/getEcgDiagnosis")
    public ResponseResult getEcgDiagnosis(@RequestBody EcgDiagnosisDO ecgDiagnosisDO) {
        return ecgDiagnosisService.getEcgDiagnosis(ecgDiagnosisDO);
    }

    @ApiOperation(value = "查询互联网医院远程心电图诊断记录监管详情")
    @PostMapping("/getByIdEcgDiagnosis")
    public ResponseResult getByIdEcgDiagnosis(Long platformId) {
        return ecgDiagnosisService.getByIdEcgDiagnosis(platformId);
    }

    @ApiOperation(value = "分页查询互联网医院远程心电图诊断记录监管")
    @PostMapping("/getPageEcgDiagnosis")
    public ResponseResult getPageEcgDiagnosis(@RequestBody EcgDiagnosisDto dto) {
        return ecgDiagnosisService.getPageEcgDiagnosis(dto);
    }

    @ApiOperation(value = "删除互联网医院远程心电图诊断记录监管")
    @PostMapping("/deleteEcgDiagnosis")
    public ResponseResult deleteEcgDiagnosis(Long platformId) {
        return ecgDiagnosisService.deleteEcgDiagnosis(platformId);
    }

    @ApiOperation(value = "修改互联网医院远程心电图诊断记录监管")
    @PostMapping("/updateEcgDiagnosis")
    public ResponseResult updateEcgDiagnosis(@RequestBody EcgDiagnosisDO ecgDiagnosisDO) {
        return ecgDiagnosisService.updateEcgDiagnosis(ecgDiagnosisDO);
    }
}
