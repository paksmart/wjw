package com.wjw.admin.controller;

import com.wjw.center.admin.domain.FilingOpenDO;
import com.wjw.center.admin.param.FilingOpenParam;
import com.wjw.common.admin.FilingOpenDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.FilingOpenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 8:31
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@RestController
@RequestMapping("/filingOpen")
@Api(value = "备案信息监管服务:医疗人员开通备案",tags = "备案信息监管服务:医疗人员开通备案")
public class FilingOpenController {

    private final FilingOpenService filingOpenService;

    @ApiOperation(value = "新增医疗人员开通备案")
    @PostMapping("/saveFilingOpen")
    public ResponseResult saveFilingOpen(@RequestBody FilingOpenDO filingOpenDO) {
        return filingOpenService.saveFilingOpen(filingOpenDO);
    }

    @ApiOperation(value = "查询医疗人员开通备案详情")
    @PostMapping("/detail")
    public ResponseResult detail(Long platformId) {
        return filingOpenService.detail(platformId);
    }

    @ApiOperation(value = "查询医疗人员开通备案")
    @PostMapping("/getFilingOpen")
    public ResponseResult getFilingOpen(@RequestBody FilingOpenParam param) {
        return filingOpenService.getFilingOpen(param);
    }

    @ApiOperation(value = "分页查询医疗人员开通备案")
    @PostMapping("/getPageFilingOpen")
    public ResponseResult getPageFilingOpen(@RequestBody FilingOpenDto dto) {
        return filingOpenService.getPageFilingOpen(dto);
    }

    @ApiOperation(value = "删除医疗人员开通备案")
    @PostMapping("/deleteFilingOpen")
    public ResponseResult deleteFilingOpen(Long platformId) {
        return filingOpenService.deleteFilingOpen(platformId);
    }

    @ApiOperation(value = "修改医疗人员开通备案")
    @PostMapping("/updateFilingOpen")
    public ResponseResult updateFilingOpen(@RequestBody FilingOpenDO filingOpenD) {
        return filingOpenService.updateFilingOpen(filingOpenD);
    }
}
