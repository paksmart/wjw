package com.wjw.admin.controller;

import com.wjw.center.admin.domain.ComplaintRecordsDO;
import com.wjw.center.admin.param.ComplaintRecordsParam;
import com.wjw.common.admin.ComplaintRecordsDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.ComplaintRecordsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/21 8:59
* @version 1.0
*/



@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/complaintRecords")
@Api(value = "质量信息:互联网医院投诉建议记录",tags = "质量信息:互联网医院投诉建议记录")
public class ComplaintRecordsController {

    private final ComplaintRecordsService complaintRecordsService;

    @ApiOperation(value = "新增互联网医院投诉建议记录")
    @PostMapping("/saveComplaintRecords")
    public ResponseResult saveComplaintRecords(@RequestBody ComplaintRecordsDO complaintRecordsDO) {
        return complaintRecordsService.saveComplaintRecords(complaintRecordsDO);
    }

    @ApiOperation(value = "查询互联网医院投诉建议记录")
    @PostMapping("/getComplaintRecords")
    public ResponseResult getComplaintRecords(@RequestBody ComplaintRecordsParam param) {
        return complaintRecordsService.getComplaintRecords(param);
    }

    @ApiOperation(value = "分页查询互联网医院投诉建议记录")
    @PostMapping("/getPageComplaintRecords")
    public ResponseResult getPageComplaintRecords(@RequestBody ComplaintRecordsDto dto) {
        return complaintRecordsService.getPageComplaintRecords(dto);
    }

    @ApiOperation(value = "查询互联网医院投诉建议记录详情")
    @PostMapping("/getByIdComplaintRecords")
    public ResponseResult getByIdComplaintRecords(Long platformId) {
        return complaintRecordsService.getByIdComplaintRecords(platformId);
    }

    @ApiOperation(value = "删除互联网医院投诉建议记录")
    @PostMapping("/deleteComplaintRecords")
    public ResponseResult deleteComplaintRecords(Long platformId) {
        return complaintRecordsService.deleteComplaintRecords(platformId);
    }

    @ApiOperation(value = "修改互联网医院投诉建议记录")
    @PostMapping("/updateComplaintRecords")
    public ResponseResult updateComplaintRecords(@RequestBody ComplaintRecordsDO complaintRecordsDO) {
        return complaintRecordsService.updateComplaintRecords(complaintRecordsDO);
    }

}
