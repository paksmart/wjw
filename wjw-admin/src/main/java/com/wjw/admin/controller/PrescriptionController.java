package com.wjw.admin.controller;

import com.wjw.center.admin.domain.PrescriptionDO;
import com.wjw.common.admin.PrescriptionDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.PrescriptionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/24 17:26
* @version 1.0
*/


@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RequestMapping("/prescription")
@Api(value = "诊疗管理:互联网在线处方信息",tags = "诊疗管理:互联网在线处方信息")
public class PrescriptionController {

    private final PrescriptionService prescriptionService;

    @ApiOperation(value = "新增互联网在线处方信息")
    @PostMapping("/savePrescription")
    public ResponseResult savePrescription(@RequestBody PrescriptionDO prescriptionDO) {
        return prescriptionService.savePrescription(prescriptionDO);
    }

    @ApiOperation(value = "查询互联网在线处方信息")
    @PostMapping("/getPrescription")
    public ResponseResult getPrescription(@RequestBody PrescriptionDO prescriptionDO) {
        return prescriptionService.getPrescription(prescriptionDO);
    }

    @ApiOperation(value = "查询互联网在线处方信息详情")
    @PostMapping("/getByIdPrescription")
    public ResponseResult getByIdPrescription(Long platformId ) {
        return prescriptionService.getByIdPrescription(platformId);
    }

    @ApiOperation(value = "分页查询互联网在线处方信息")
    @PostMapping("/getPagePrescription")
    public ResponseResult getPagePrescription(@RequestBody PrescriptionDto dto) {
        return prescriptionService.getPagePrescription(dto);
    }

    @ApiOperation(value = "删除互联网在线处方信息")
    @PostMapping("/deletePrescription")
    public ResponseResult deletePrescription(Long platformId ) {
        return prescriptionService.deletePrescription(platformId);
    }

    @ApiOperation(value = "修改互联网在线处方信息")
    @PostMapping("/updatePrescription")
    public ResponseResult updatePrescription(@RequestBody PrescriptionDO prescriptionDO ) {
        return prescriptionService.updatePrescription(prescriptionDO);
    }
}
