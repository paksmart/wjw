package com.wjw.admin.controller;

import com.wjw.center.admin.domain.PatientinfoDO;
import com.wjw.center.admin.param.PatientinfoParam;
import com.wjw.common.admin.PatientinfoDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.PatientInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/15 9:17
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@RestController
@RequestMapping("/userInfo")
@Api(value = "诊疗行为监管:患者信息监管",tags = "诊疗行为监管:患者信息监管")
public class PatientInfoController {

    private final PatientInfoService patientInfoService;

    @ApiOperation(value = "新增患者信息")
    @PostMapping("/savePatientInfo")
    public ResponseResult savePatientInfo(@RequestBody PatientinfoDO patientinfoDO) {
        return patientInfoService.savePatientInfo(patientinfoDO);
    }

    @ApiOperation(value = "查询患者信息")
    @PostMapping("/getPatientInfo")
    public ResponseResult getPatientInfo(@RequestBody PatientinfoParam param) {
        return patientInfoService.getPatientInfo(param);
    }

    @ApiOperation(value = "查询患者信息详情")
    @PostMapping("/getById")
    public ResponseResult getById(Long platformId) {
        return patientInfoService.getById(platformId);
    }

    @ApiOperation(value = "分页查询患者信息")
    @PostMapping("/getPagePatientInfo")
    public ResponseResult getPagePatientInfo(@RequestBody PatientinfoDto dto) {
        return patientInfoService.getPagePatientInfo(dto);
    }

    @ApiOperation(value = "删除患者信息")
    @PostMapping("/delete")
    public ResponseResult delete(Long platformId) {
        return patientInfoService.delete(platformId);
    }

    @ApiOperation(value = "修改患者信息")
    @PostMapping("/update")
    public ResponseResult update(@RequestBody PatientinfoDO patientinfoDO) {
        return patientInfoService.update(patientinfoDO);
    }
}
