package com.wjw.admin.controller;

import com.wjw.center.admin.domain.EvaluateRecordsDO;
import com.wjw.center.admin.param.EvaluateRecordsParam;
import com.wjw.common.admin.EvaluateRecordsDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.EvaluateRecordsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 16:15
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/evaluateRecords")
@Api(value = "质量信息:互联网医院业务评价记录",tags = "质量信息:互联网医院业务评价记录")
public class EvaluateRecordsController {

    private final EvaluateRecordsService evaluateRecordsService;

    @ApiOperation(value = "新增互联网医院业务评价记录")
    @ApiImplicitParam(name = "evaluateRecordsDO", value = "请求参数", required = true, dataType = "EvaluateRecordsDO")
    @PostMapping("/saveEvaluateRecords")
    public ResponseResult saveEvaluateRecords(@RequestBody EvaluateRecordsDO evaluateRecordsDO) {
        return evaluateRecordsService.saveEvaluateRecords(evaluateRecordsDO);
    }

    @ApiOperation(value = "查询互联网医院业务评价记录")
    @PostMapping("/getEvaluateRecords")
    public ResponseResult getEvaluateRecords(@RequestBody EvaluateRecordsParam param) {
        return evaluateRecordsService.getEvaluateRecords(param);
    }

    @ApiOperation(value = "分页查询互联网医院业务评价记录")
    @ApiImplicitParam(name = "param", value = "请求参数", required = true, dataType = "EvaluateRecordsParam")
    @PostMapping("/getPageEvaluateRecords")
    public ResponseResult getPageEvaluateRecords(@RequestBody EvaluateRecordsDto dto) {
        return evaluateRecordsService.getPageEvaluateRecords(dto);
    }

    @ApiOperation(value = "查询互联网医院业务评价记录详情")
    @ApiImplicitParam(name = "platformId",value = "请求参数",required = true,dataType = "Long")
    @PostMapping("/getByIdEvaluateRecords")
    public ResponseResult getByIdEvaluateRecords(Long platformId) {
        return evaluateRecordsService.getByIdEvaluateRecords(platformId);
    }

    @ApiOperation(value = "删除互联网医院业务评价记录")
    @ApiImplicitParam(name = "platformId",value = "请求参数",required = true,dataType = "Long")
    @PostMapping("/deleteEvaluateRecords")
    public ResponseResult deleteEvaluateRecords(Long platformId) {
        return evaluateRecordsService.deleteEvaluateRecords(platformId);
    }

    @ApiOperation(value = "修改互联网医院业务评价记录")
    @ApiImplicitParam(name = "evaluateRecordsDO",value = "请求参数",required = true,dataType = "EvaluateRecordsDO")
    @PostMapping("/updateEvaluateRecords")
    public ResponseResult updateEvaluateRecords(@RequestBody EvaluateRecordsDO evaluateRecordsDO) {
        return evaluateRecordsService.updateEvaluateRecords(evaluateRecordsDO);
    }
}
