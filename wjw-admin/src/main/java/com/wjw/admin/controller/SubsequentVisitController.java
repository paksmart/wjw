package com.wjw.admin.controller;

import com.wjw.center.admin.domain.SubsequentVisitDO;
import com.wjw.center.admin.param.SubsequentVisitParam;
import com.wjw.common.admin.SubsequentVisitDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.SubsequentVisitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 11:09
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/subsequentVisit")
@Api(value = "诊疗管理:互联网在线复诊信息",tags = "诊疗管理:互联网在线复诊信息")
public class SubsequentVisitController {

    private final SubsequentVisitService subsequentVisitService;

    @ApiOperation(value = "新增互联网在线复诊信息")
    @PostMapping("/saveSubsequentVisit")
    public ResponseResult saveSubsequentVisit(@RequestBody SubsequentVisitDO subsequentVisitDO) {
        return subsequentVisitService.saveSubsequentVisit(subsequentVisitDO);
    }

    @ApiOperation(value = "查询互联网在线复诊信息")
    @PostMapping("/getSubsequentVisit")
    public ResponseResult getSubsequentVisit(@RequestBody SubsequentVisitParam param) {
        return subsequentVisitService.getSubsequentVisit(param);
    }

    @ApiOperation(value = "查询互联网在线复诊信息详情")
    @PostMapping("/getByIdSubsequentVisit")
    public ResponseResult getByIdSubsequentVisit(Long platformId) {
        return subsequentVisitService.getByIdSubsequentVisit(platformId);
    }

    @ApiOperation(value = "分页查询互联网在线复诊信息")
    @PostMapping("/getPageSubsequentVisit")
    public ResponseResult getPageSubsequentVisit(@RequestBody SubsequentVisitDto dto) {
        return subsequentVisitService.getPageSubsequentVisit(dto);
    }

    @ApiOperation(value = "查询互联网在线复诊信息详情")
    @PostMapping("/deleteSubsequentVisit")
    public ResponseResult deleteSubsequentVisit(Long platformId) {
        return subsequentVisitService.deleteSubsequentVisit(platformId);
    }

    @ApiOperation(value = "修改互联网在线复诊信息")
    @PostMapping("/updateSubsequentVisit")
    public ResponseResult updateSubsequentVisit(@RequestBody SubsequentVisitDO subsequentVisitDO) {
        return subsequentVisitService.updateSubsequentVisit(subsequentVisitDO);
    }
}
