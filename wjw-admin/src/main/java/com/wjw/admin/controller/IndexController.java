package com.wjw.admin.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wjw.common.utils.Sm4Util;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Api(value = "接口加解密规则",tags = "接口加解密规则")
public class IndexController {

    @PostMapping("/encryptDate")
    public  void encryptDate() throws JsonProcessingException {
        String paramStr = "{\"id\":1,\"platformId\":\"666666888888\",\"staffId\":1,\"staffName\":\"atangs\",\"staffIdCard\":\"341223199105040711\",\"businessType\":10,\"filingMark\":1,\"updateTime\":\"2021-10-0611:16:48\"}";
        // 转为jsonObject（接口加密的数据需要是JSONObject)
        JSONObject jsonObject = JSONObject.parseObject(paramStr);
        // Object 转为JSON String
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
        String res = null;
        try {
            res = Sm4Util.encrypt("1D646DC1A0EB0CA05C5AA5CA86378B33", jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(res);
    }


    @PostMapping("/decryptDate")
    public  void decryptDate() throws Exception {
        String str = "nlEqEYLWqobCuC/OjJHV+z2zoqzrOEv0yu0DhwPJPMuWEXEmKPR8p4jxeUkisDhsXQ7bu1kY81g2VkS2u/eUAT1eWrlRUf9zC6/sI++g1LjPbN7QbdnVsBS+jz/WoY3T3CP1wQ9XiWsM2FgVbmZwVzmiX9sRCo0WTxXHXrgu9KE6WPqWa+f99Srdc4NmDV1OeQo3aG+/fowHiuAUCvLioOBhQ2hSgDyMoFmbTxyC6Wz4SEklZWd8qDksEV+dMj4wkBtPYMg6eRscKKO9coDBNjyJCZd/FIpvYF0JPIqVI4k=";
        String decrypt = Sm4Util.decrypt("1D646DC1A0EB0CA05C5AA5CA86378B33", str);
        System.out.println(decrypt);
    }

}
