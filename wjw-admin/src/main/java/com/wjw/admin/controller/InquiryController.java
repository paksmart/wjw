package com.wjw.admin.controller;

import com.wjw.center.admin.domain.InquiryDO;
import com.wjw.center.admin.param.InquiryParam;
import com.wjw.common.admin.InquiryDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.InquiryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 17:42
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/inquiry")
@Api(value = "诊疗管理:患者在线咨询信息",tags = "诊疗管理:患者在线咨询信息")
public class InquiryController {

    private final InquiryService inquiryService;

    @ApiOperation(value = "新增患者在线咨询信息")
    @PostMapping("/saveInquiry")
    public ResponseResult saveInquiry(@RequestBody InquiryDO inquiryDO) {
        return inquiryService.saveInquiry(inquiryDO);
    }

    @ApiOperation(value = "查询患者在线咨询信息")
    @PostMapping("/getInquiry")
    public ResponseResult getInquiry(@RequestBody InquiryParam param) {
        return inquiryService.getInquiry(param);
    }

    @ApiOperation(value = "分页查询者在线咨询信息")
    @PostMapping("/getPageInquiry")
    public ResponseResult getPageInquiry(@RequestBody InquiryDto dto) {
        return inquiryService.getPageInquiry(dto);
    }

    @ApiOperation(value = "查询患者在线咨询信息详情")
    @PostMapping("/getByIdInquiry")
    public ResponseResult getByIdInquiry(Long platformId) {
        return inquiryService.getByIdInquiry(platformId);
    }

    @ApiOperation(value = "删除患者在线咨询信息")
    @PostMapping("/deleteInquiry")
    public ResponseResult deleteInquiry(Long platformId) {
        return inquiryService.deleteInquiry(platformId);
    }

    @ApiOperation(value = "修改患者在线咨询信息")
    @PostMapping("/updateInquiry")
    public ResponseResult updateInquiry(@RequestBody InquiryDO inquiryDO) {
        return inquiryService.updateInquiry(inquiryDO);
}
}
