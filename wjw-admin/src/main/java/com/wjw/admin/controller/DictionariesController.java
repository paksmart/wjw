package com.wjw.admin.controller;

import com.wjw.service.DictionariesService;
import com.wjw.center.admin.dictionaries.*;
import com.wjw.common.dto.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* @description: 字典数据
* @author psk
* @date 2022/10/17 9:11
* @version 1.0
*/

@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RequestMapping("/dictionaries")
@Api(value = "字典数据",tags = "字典数据")
public class DictionariesController {

    private final DictionariesService dictionaries;

    @ApiOperation(value = "民族字典数据")
    @PostMapping("/dictionaries")
    public ResponseResult dictionaries(@RequestBody DictionariesNation dictionariesNation) {
        return dictionaries.dictionaries( dictionariesNation);
    }

    @ApiOperation(value = "科目代码字典数据")
    @PostMapping("/dictionariesSubjectCode")
    public ResponseResult dictionariesSubjectCode(@RequestBody DictionariesSubject dictionariesSubject) {
        return dictionaries.dictionariesSubjectCode(dictionariesSubject);
    }

    @ApiOperation(value = "医疗人员职称字典数据")
    @PostMapping("/dictionariesTitle")
    public ResponseResult dictionariesTitle(@RequestBody DictionariesDoctorTitle dictionariesDoctorTitle) {
        return dictionaries.dictionariesTitle(dictionariesDoctorTitle);
    }

    @ApiOperation(value = "证件类型字典")
    @PostMapping("/dictionariesCertType")
    public ResponseResult dictionariesCertType(@RequestBody DictionariesCertType dictionariesCertType) {
        return dictionaries.dictionariesCertType(dictionariesCertType);
    }

    @ApiOperation(value = "业务类型字典")
    @PostMapping("/dictionariesBusiness")
    public ResponseResult dictionariesBusiness(@RequestBody DictionariesBusiness dictionariesBusiness) {
        return dictionaries.dictionariesBusiness(dictionariesBusiness);
    }

    @ApiOperation(value = "性别字典")
    @PostMapping("/dictionariesSex")
    public ResponseResult dictionariesSex(@RequestBody DictionariesSex dictionariesSex) {
        return dictionaries.dictionariesSex(dictionariesSex);
    }

    @ApiOperation(value = "地区字典")
    @PostMapping("/dictionariesArea")
    public ResponseResult dictionariesArea(@RequestBody DictionariesArea dictionariesArea) {
        return dictionaries.dictionariesArea(dictionariesArea);
    }

    @ApiOperation(value = "疾病字典")
    @PostMapping("/dictionariesIcd")
    public ResponseResult dictionariesIcd(@RequestBody DictionariesIcd dictionariesIcd) {
        return dictionaries.dictionariesIcd(dictionariesIcd);
    }
}
