package com.wjw.admin.controller;

import com.wjw.service.PictureService;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
* @description: 编码转换
* @author pansh
* @date 2022/10/4 15:24
* @version 1.0
*/

@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Api(value = "编码转换bas64",tags = "编码转换bas64")
public class PicController {

    private final PictureService pictureService;

    @RequestMapping(value = "/image/{id}", method = RequestMethod.POST)
    public ResponseEntity<byte[]> image(@PathVariable String id) throws IOException {
        String image = pictureService.getImageById(id);
        if (StringUtils.isBlank(image)) {
            return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<byte[]>(image.getBytes(), headers, HttpStatus.OK);
    }
}
