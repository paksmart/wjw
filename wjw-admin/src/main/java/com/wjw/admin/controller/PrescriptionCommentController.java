package com.wjw.admin.controller;

import com.wjw.center.admin.domain.PrescriptionCommentDO;
import com.wjw.common.admin.PrescriptionCommentDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.PrescriptionCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description:
* @author pansh
* @date 2022/9/26 14:59
* @version 1.0
*/

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RequestMapping("/prescriptionComment")
@Api(value = "诊疗管理:互联网在线处方点评监管",tags = "诊疗管理:互联网在线处方点评监管")
public class PrescriptionCommentController {

    private final PrescriptionCommentService prescriptionCommentService;

    @ApiOperation(value = "新增互联网在线处方点评监管")
    @PostMapping("/savePrescriptionComment")
    public ResponseResult savePrescriptionComment(@RequestBody PrescriptionCommentDO prescriptionCommentDO) {
        return prescriptionCommentService.savePrescriptionComment(prescriptionCommentDO);
    }

    @ApiOperation(value = "查询互联网在线处方点评监管")
    @PostMapping("/getPrescriptionComment")
    public ResponseResult getPrescriptionComment(@RequestBody PrescriptionCommentDO prescriptionCommentDO) {
        return prescriptionCommentService.getPrescriptionComment(prescriptionCommentDO);
    }

    @ApiOperation(value = "分页查询互联网在线处方点评监管")
    @PostMapping("/getPagePrescriptionComment")
    public ResponseResult getPagePrescriptionComment(@RequestBody PrescriptionCommentDto dto) {
        return prescriptionCommentService.getPagePrescriptionComment(dto);
    }

    @ApiOperation(value = "查询互联网在线处方点评监管详情")
    @PostMapping("/getByIdPrescriptionComment")
    public ResponseResult getByIdPrescriptionComment(Long platformId) {
        return prescriptionCommentService.getByIdPrescriptionComment(platformId);
    }

    @ApiOperation(value = "删除互联网在线处方点评监管")
    @PostMapping("/deletePrescriptionComment")
    public ResponseResult deletePrescriptionComment(Long platformId ) {
        return prescriptionCommentService.deletePrescriptionComment(platformId);
    }

    @ApiOperation(value = "修改互联网在线处方点评监管")
    @PostMapping("/updatePrescriptionComment")
    public ResponseResult updatePrescriptionComment(@RequestBody PrescriptionCommentDO prescriptionCommentDO) {
        return prescriptionCommentService.updatePrescriptionComment(prescriptionCommentDO);
    }
}
