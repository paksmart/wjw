package com.wjw.admin.controller;

import com.wjw.center.admin.domain.TeleconsultationRecordsDO;
import com.wjw.common.admin.TeleconsultationRecordsDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.TeleconsultationRecordsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description:
* @author pansh
* @date 2022/9/29 10:28
* @version 1.0
*/

@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RequestMapping("/TeleconsultationRecords")
@Api(value = "诊疗管理:互联网医院远程会诊记录监管",tags = "诊疗管理:互联网医院远程会诊记录监管")
public class TeleconsultationRecordsController {

    private final TeleconsultationRecordsService teleconsultationRecordsService;

    @ApiOperation(value = "新增互联网医院远程会诊记录监管")
    @PostMapping("/saveTeleconsultationRecords")
    public ResponseResult saveTeleconsultationRecords(@RequestBody TeleconsultationRecordsDO teleconsultationRecordsDO) {
        return teleconsultationRecordsService.saveTeleconsultationRecords(teleconsultationRecordsDO);
    }

    @ApiOperation(value = "查询互联网医院远程会诊记录监管")
    @PostMapping("/getTeleconsultationRecords")
    public ResponseResult getTeleconsultationRecords(@RequestBody TeleconsultationRecordsDO teleconsultationRecordsDO) {
        return teleconsultationRecordsService.getTeleconsultationRecords(teleconsultationRecordsDO);
    }

    @ApiOperation(value = "查询互联网医院远程会诊记录监管详情")
    @PostMapping("/getByIdTeleconsultationRecords")
    public ResponseResult getByIdTeleconsultationRecords(Long platformId ) {
        return teleconsultationRecordsService.getByIdTeleconsultationRecords(platformId);
    }

    @ApiOperation(value = "分页查询互联网医院远程会诊记录监管")
    @PostMapping("/getPageTeleconsultationRecords")
    public ResponseResult getPageTeleconsultationRecords(@RequestBody TeleconsultationRecordsDto dto) {
        return teleconsultationRecordsService.getPageTeleconsultationRecords(dto);
    }

    @ApiOperation(value = "删除互联网医院远程会诊记录监管")
    @PostMapping("/deleteTeleconsultationRecords")
    public ResponseResult deleteTeleconsultationRecords(Long platformId ) {
        return teleconsultationRecordsService.deleteTeleconsultationRecords(platformId);
    }

    @ApiOperation(value = "修改互联网医院远程会诊记录监管")
    @PostMapping("/updateTeleconsultationRecords")
    public ResponseResult updateTeleconsultationRecords(@RequestBody TeleconsultationRecordsDO teleconsultationRecordsDO) {
        return teleconsultationRecordsService.updateTeleconsultationRecords(teleconsultationRecordsDO);
    }
}
