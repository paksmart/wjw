package com.wjw.admin.controller;

import com.wjw.center.admin.domain.FilingStaffDO;
import com.wjw.common.admin.FilingStaffDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.FilingStaffService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* @description: psk
* @author pansh
* @date 2022/9/16 16:22
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@RestController
@RequestMapping("/filingStaff")
@Api(tags = "备案信息监管服务:医疗人员备案")
public class FilingStaffController {

    private final FilingStaffService filingStaffService;

    @ApiOperation(value = "新增医疗人员备案",notes = "author:psk")
    @ApiImplicitParam(name = "filingStaffDO",value = "请求对象",required = true,dataType = "FilingStaffDO")
    @PostMapping("/saveFilingStaff")
    public ResponseResult<Boolean> saveFilingStaff(@RequestBody FilingStaffDO filingStaffDO) {
        return filingStaffService.saveFilingStaff(filingStaffDO);
    }

    @ApiOperation(value = "查询医疗人员备案详情")
    @PostMapping("/getById")
    public ResponseResult getById(Long platformId) {
        return filingStaffService.getById(platformId);
    }

    @ApiOperation(value = "分页查询查询医疗人员备案",notes = "author:psk")
    @ApiImplicitParam(name = "dto", value = "请求对象", required = true, dataType = "FilingStaffDto")
    @PostMapping("/getPageFilingStaff")
    public ResponseResult getPageFilingStaff(@RequestBody FilingStaffDto dto) {
        return filingStaffService.getPageFilingStaff(dto);
    }

    @ApiOperation(value = "删除医疗人员备案")
    @PostMapping("/delete")
    public ResponseResult delete( Long platformId) {
        return filingStaffService.delete(platformId);
    }

    @ApiOperation(value = "修改医疗人员备案")
    @PostMapping("/update")
    public ResponseResult update(@RequestBody FilingStaffDO filingStaffDO) {
        return filingStaffService.update(filingStaffDO);
    }
}
