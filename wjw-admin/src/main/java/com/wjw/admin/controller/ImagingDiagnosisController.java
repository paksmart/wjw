package com.wjw.admin.controller;

import com.wjw.center.admin.domain.ImagingDiagnosisDO;
import com.wjw.common.admin.ImagingDiagnosisDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.ImagingDiagnosisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/27 9:37
* @version 1.0
*/


@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RequestMapping("/imagingDiagnosis")
@Api(value = "诊疗管理:互联网医院远程影像诊断记录监管",tags = "诊疗管理:互联网医院远程影像诊断记录监管")
public class ImagingDiagnosisController {

    private final ImagingDiagnosisService imagingDiagnosisService;

    @ApiOperation(value = "新增互联网医院远程影像诊断记录监管")
    @PostMapping("/saveImagingDiagnosis")
    public ResponseResult saveImagingDiagnosis(@RequestBody ImagingDiagnosisDO imagingDiagnosisDO) {
        return imagingDiagnosisService.saveImagingDiagnosis(imagingDiagnosisDO);
    }

    @ApiOperation(value = "查询互联网医院远程影像诊断记录监管")
    @PostMapping("/getImagingDiagnosis")
    public ResponseResult getImagingDiagnosis(@RequestBody ImagingDiagnosisDO imagingDiagnosisDO) {
        return imagingDiagnosisService.getImagingDiagnosis(imagingDiagnosisDO);
    }

    @ApiOperation(value = "查询互联网医院远程影像诊断记录监管详情")
    @PostMapping("/getByIdImagingDiagnosis")
    public ResponseResult getByIdImagingDiagnosis(Long platformId) {
        return imagingDiagnosisService.getByIdImagingDiagnosis(platformId);
    }

    @ApiOperation(value = "分页查询互联网医院远程影像诊断记录监管详情")
    @PostMapping("/getPageImagingDiagnosis")
    public ResponseResult getPageImagingDiagnosis(@RequestBody ImagingDiagnosisDto dto) {
        return imagingDiagnosisService.getPageImagingDiagnosis(dto);
    }

    @ApiOperation(value = "删除互联网医院远程影像诊断记录监管")
    @PostMapping("/deleteImagingDiagnosis")
    public ResponseResult deleteImagingDiagnosis(Long platformId) {
        return imagingDiagnosisService.deleteImagingDiagnosis(platformId);
    }

    @ApiOperation(value = "修改互联网医院远程影像诊断记录监管")
    @PostMapping("/updateImagingDiagnosis")
    public ResponseResult updateImagingDiagnosis(@RequestBody ImagingDiagnosisDO imagingDiagnosisDO) {
        return imagingDiagnosisService.updateImagingDiagnosis(imagingDiagnosisDO);
    }
}
