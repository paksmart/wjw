package com.wjw.admin.controller;

import com.wjw.center.admin.domain.PrescriptionCirculationDO;
import com.wjw.center.admin.param.PrescriptionCirculationParam;
import com.wjw.common.admin.PrescriptionCirculationDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.PrescriptionCirculationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 18:40
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/prescriptionCirculation")
@Api(value = "诊疗管理:互联网在线处方流转(配送,发药,购药)信息",tags = "诊疗管理:互联网在线处方流转(配送,发药,购药)信息")
public class PrescriptionCirculationController {

    private final PrescriptionCirculationService prescriptionCirculationService;

    @ApiOperation(value = "新增互联网在线处方流转信息",notes = "author:psk")
    @ApiImplicitParam(name = "prescriptionCirculationDO",value = "请求对象",required = true,dataType = "PrescriptionCirculationDO")
    @PostMapping("/savePrescriptionCirculation")
    public ResponseResult savePrescriptionCirculation(@RequestBody PrescriptionCirculationDO prescriptionCirculationDO) {
        return prescriptionCirculationService.savePrescriptionCirculation(prescriptionCirculationDO);
    }

    @ApiOperation(value = "查询互联网在线处方流转信息")
    @PostMapping("/getPrescriptionCirculation")
    public ResponseResult getPrescriptionCirculation(@RequestBody PrescriptionCirculationDO param) {
        return prescriptionCirculationService.getPrescriptionCirculation(param);
    }

    @ApiOperation(value = "查询互联网在线处方流转信息详情")
    @PostMapping("/getByIdPrescriptionCirculation")
    public ResponseResult getByIdPrescriptionCirculation(Long platformId) {
        return prescriptionCirculationService.getByIdPrescriptionCirculation(platformId);
    }

    @ApiOperation(value = "分页查询互联网在线处方流转信息")
    @PostMapping("/getPagePrescriptionCirculation")
    public ResponseResult getPagePrescriptionCirculation(@RequestBody PrescriptionCirculationDto dto) {
        return prescriptionCirculationService.getPagePrescriptionCirculation(dto);
    }

    @ApiOperation(value = "删除互联网在线处方流转信息")
    @PostMapping("/deletePrescriptionCirculation")
    public ResponseResult deletePrescriptionCirculation(Long platformId) {
        return prescriptionCirculationService.deletePrescriptionCirculation(platformId);
    }

    @ApiOperation(value = "修改互联网在线处方流转信息")
    @PostMapping("/updatePrescriptionCirculation")
    public ResponseResult updatePrescriptionCirculation(@RequestBody PrescriptionCirculationDO prescriptionCirculationDO) {
        return prescriptionCirculationService.updatePrescriptionCirculation(prescriptionCirculationDO);
    }
}
