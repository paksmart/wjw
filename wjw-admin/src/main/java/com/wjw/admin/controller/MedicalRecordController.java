package com.wjw.admin.controller;

import com.wjw.center.admin.domain.MedicalRecordDO;
import com.wjw.center.admin.param.MedicalRecordParam;
import com.wjw.common.admin.MedicalRecordDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.MedicalRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description:
* @author pansh
* @date 2022/9/17 15:28
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@RestController
@RequestMapping("/medicalRecord")
@Api(value = "诊疗管理:患者电子病例信监管",tags = "诊疗管理:患者电子病例信息监管")
public class MedicalRecordController {

    private final MedicalRecordService medicalRecordService;

    @ApiOperation(value = "新增患者电子病例信息")
    @PostMapping("/saveMedicalRecord")
    public ResponseResult saveMedicalReport(@RequestBody MedicalRecordDO medicalRecordDO) {
        return medicalRecordService.saveMedicalRepord(medicalRecordDO);
    }

    @ApiOperation(value = "查询患者电子病例信息")
    @PostMapping("/getMedicalRecord")
    public ResponseResult getMedicalRepord(@RequestBody MedicalRecordParam param) {
        return medicalRecordService.getMedicalRepord(param);
    }

    @ApiOperation(value = "分页查询患者电子病例信息")
    @PostMapping("/getPageMedicalRecord")
    public ResponseResult getPageMedicalRepord(@RequestBody MedicalRecordDto dto) {
        return medicalRecordService.getPageMedicalRepord(dto);
    }

    @ApiOperation(value = "查询患者电子病例信息详情")
    @PostMapping("/getByIdMedicalRecord")
    public ResponseResult getByIdMedicalRepord(Long platformId) {
        return medicalRecordService.getByIdMedicalRepord(platformId);
    }

    @ApiOperation(value = "修改患者电子病例信息")
    @PostMapping("/updateMedicalRecord")
    public ResponseResult updateMedicalRepord(@RequestBody MedicalRecordDO medicalRecordDO) {
        return medicalRecordService.updateMedicalRepord(medicalRecordDO);
    }

    @ApiOperation(value = "删除患者电子病例信息")
    @PostMapping("/deleteMedicalRecord")
    public ResponseResult deleteMedicalRepord(Long platformId) {
        return medicalRecordService.deleteMedicalRepord(platformId);
    }
}
