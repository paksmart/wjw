package com.wjw.admin.controller;

import com.wjw.center.admin.domain.RemoteClinicRecordDO;
import com.wjw.center.admin.param.RemoteClinicRecordParam;
import com.wjw.common.admin.RemoteClinicRecordDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.RemoteClinicRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 10:32
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/remoteClinicRecord")
@Api(value = "诊疗管理:互联网医院远程门诊记录",tags = "诊疗管理:互联网医院远程门诊记录")
public class RemoteClinicRecordController {

    private final RemoteClinicRecordService remoteClinicRecordService;

    @ApiOperation(value = "新增互联网医院远程门诊记录")
    @PostMapping("/saveRemoteClinicRecord")
    public ResponseResult saveRemoteClinicRecord(@RequestBody RemoteClinicRecordDO remoteClinicRecordDO) {
        return remoteClinicRecordService.saveRemoteClinicRecord(remoteClinicRecordDO);
    }

    @ApiOperation(value = "查询互联网医院远程门诊记录")
    @PostMapping("/getRemoteClinicRecord")
    public ResponseResult getRemoteClinicRecord(@RequestBody RemoteClinicRecordParam param) {
        return remoteClinicRecordService.getRemoteClinicRecord(param);
    }

    @ApiOperation(value = "查询互联网医院远程门诊记录详情")
    @PostMapping("/getByIdRemoteClinicRecord")
    public ResponseResult getByIdRemoteClinicRecord(Long id) {
        return remoteClinicRecordService.getByIdRemoteClinicRecord(id);
    }

    @ApiOperation(value ="分页查询互联网医院远程门诊记录" )
    @PostMapping("/getPageRemoteClinicRecord")
    public ResponseResult getPageRemoteClinicRecord(@RequestBody RemoteClinicRecordDto dto) {
        return remoteClinicRecordService.getPageRemoteClinicRecord(dto);
    }

    @ApiOperation(value = "删除互联网医院远程门诊记录")
    @PostMapping("/deleteRemoteClinicRecord")
    public ResponseResult deleteRemoteClinicRecord(Long id) {
        return remoteClinicRecordService.deleteRemoteClinicRecord(id);
    }

    @ApiOperation(value = "修改互联网医院远程门诊记录")
    @PostMapping("/updateRemoteClinicRecord")
    public ResponseResult updateRemoteClinicRecord(@RequestBody RemoteClinicRecordDO remoteClinicRecordDO) {
        return remoteClinicRecordService.updateRemoteClinicRecord(remoteClinicRecordDO);
    }
}
