package com.wjw.admin.controller;

import com.wjw.common.admin.AdverseEventDto;
import com.wjw.service.AdverseEventService;
import com.wjw.center.admin.domain.AdverseEventDO;
import com.wjw.center.admin.param.AdverseEventParam;
import com.wjw.common.dto.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description:
* @author pansh
* @date 2022/9/20 14:30
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/adverseEvent")
@Api(value = "质量信息:互联网医院不良事件记录",tags = "质量信息:互联网医院不良事件记录")
public class AdverseEventController {

    private final AdverseEventService adverseEventService;

    @ApiOperation(value = "新增互联网医院不良事件记录")
    @ApiImplicitParam(name = "adverseEventDO", value = "请求参数", required = true, dataType = "AdverseEventDO")
    @PostMapping("/saveAdverseEvent")
    public ResponseResult saveAdverseEvent(@RequestBody AdverseEventDO adverseEventDO ) {
        return adverseEventService.saveAdverseEvent(adverseEventDO);
    }

    @ApiOperation(value = "查询互联网医院不良事件记录")
    @PostMapping("/getAdverseEvent")
    public ResponseResult getAdverseEvent(@RequestBody AdverseEventDO adverseEventDO) {
        return adverseEventService.getAdverseEvent(adverseEventDO);
    }

    @ApiOperation(value = "分页查询互联网医院不良事件记录")
    @ApiImplicitParam(name = "pageNum,pageSize,param,",value = "请求参数",required = true,dataType = "AdverseEventParam")
    @PostMapping("/getPageAdverseEvent")
    public ResponseResult getPageAdverseEvent(@RequestBody AdverseEventDto dto) {
        return adverseEventService.getPageAdverseEvent(dto);
    }

    @ApiOperation(value = "查询互联网医院不良事件记录详情")
    @ApiImplicitParam(name = "platformId",value = "请求参数",required = true,dataType = "Long")
    @PostMapping("/getByIdAdverseEvent")
    public ResponseResult getByIdAdverseEvent(Long platformId) {
        return adverseEventService.getByIdAdverseEvent(platformId);
    }

    @ApiOperation(value = "删除互联网医院不良事件记录")
    @ApiImplicitParam(name = "platformId", value = "请求参数", required = true, dataType = "Long")
    @PostMapping("/deleteAdverseEvent")
    public ResponseResult deleteAdverseEvent(Long platformId) {
        return adverseEventService.deleteAdverseEvent(platformId);
    }

    @ApiOperation(value = "修改互联网医院不良事件记录")
    @ApiImplicitParam(name = "adverseEventDO", value = "请求参数", required = true, dataType = "AdverseEventDO")
    @PostMapping("/updateAdverseEvent")
    public ResponseResult updateAdverseEvent(@RequestBody AdverseEventDO adverseEventDO) {
        return adverseEventService.updateAdverseEvent(adverseEventDO);
    }
}
