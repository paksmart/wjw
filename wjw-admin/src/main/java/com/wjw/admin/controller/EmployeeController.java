package com.wjw.admin.controller;

import com.wjw.center.admin.domain.Employee;
import com.wjw.common.admin.EmployeeDto;
import com.wjw.common.dto.ResponseResult;
import com.wjw.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* @description: 登录模块
* @author pansh
* @date 2022/10/22 10:28
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/employee")
@Api(value = "系统用户管理登录",tags = "系统用户管理登录")
public class EmployeeController {

    private final EmployeeService employeeService;

    //登录
    @ApiOperation(value = "员工登入")
    @PostMapping("/login")
    public ResponseResult login(@RequestBody Employee employee) {
        return employeeService.login(employee);
    }

    @ApiOperation(value = "新增员工")
    @PostMapping("/saveEmployee")
    public ResponseResult saveEmployee(@RequestBody Employee employee) {
        return employeeService.saveEmployee(employee);
    }

    @ApiOperation(value = "分页查询员工")
    @PostMapping("/getPageEmployee")
    public ResponseResult getPageEmployee(@RequestBody EmployeeDto dto) {
        return employeeService.getPageEmployee(dto);
    }

    @ApiOperation(value = "查询员工")
    @PostMapping("/getByIdEmployee")
    public ResponseResult getByIdEmployee(Long id) {
        return employeeService.getByIdEmployee(id);
    }

    @ApiOperation(value = "删除员工")
    @PostMapping("/deleteByIdEmployee")
    public ResponseResult deleteByIdEmployee(Long id) {
        return employeeService.deleteByIdEmployee(id);
    }

    @ApiOperation(value = "修改员工")
    @PostMapping("/updateEmployee")
    public ResponseResult updateEmployee(@RequestBody Employee employee) {
        return employeeService.updateEmployee(employee);
    }
}
