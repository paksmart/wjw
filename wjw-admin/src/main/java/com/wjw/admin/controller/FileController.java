package com.wjw.admin.controller;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wjw.common.litener.ExcelListener;
import com.wjw.center.admin.dictionaries.DictionariesSubject;
import com.wjw.common.utils.JsonData;
import com.wjw.mapper.DictionariesMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lcy
 * @since 2021-02-25
 */
@RestController
@RequestMapping("/myImport")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Api(value = "基础数据字典",tags = "基础数据字典")
public class FileController {

    private final DictionariesMapper dictionariesMapper;

    @SneakyThrows
    @ResponseBody
    @ApiOperation(value = "导入字典")
    @ApiImplicitParam(name = "file",value = "文件")
    @PostMapping("/saveMyImport")
    public JsonData myImport(MultipartFile file) {
        try {
            //获取文件名
            String filename = file.getOriginalFilename();
            //获取文件流
            InputStream inputStream = file.getInputStream();
            //实例化实现了AnalysisEventListener接口的类
            ExcelListener listener = new ExcelListener();
            EasyExcelFactory.read(inputStream, DictionariesSubject.class, listener).headRowNumber(1).build().readAll();
            //获取数据
            List<Object> list = listener.getDatas();
            if (list.size() > 1) {
                for (int i = 0; i < list.size(); i++) {
                    DictionariesSubject user = (DictionariesSubject) list.get(i);
                    dictionariesMapper.insert(user);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JsonData.buildSuccess();
    }

    @PostMapping("/listMyImport")
    public void myExport(HttpServletResponse response, HttpServletRequest request) {
        try {
            String filenames = "111111";
            String userAgent = request.getHeader("Admin-Agent");
            if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
                filenames = URLEncoder.encode(filenames, "UTF-8");
            } else {
                filenames = new String(filenames.getBytes("UTF-8"), "ISO-8859-1");
            }
            response.setContentType("application/json.ms-exce");
            response.setCharacterEncoding("utf-8");
            response.addHeader("Content-Disposition", "filename=" + filenames + ".xlsx");
            // Step1：创建一个 QueryWrapper 对象
            QueryWrapper<DictionariesSubject> queryWrapper = new QueryWrapper<>();
            // Step2： 构造查询条件
            queryWrapper
                    .select("*");
            // Step3：执行查询
            List<DictionariesSubject> userList = dictionariesMapper.selectList(queryWrapper);
            EasyExcel.write(response.getOutputStream(), DictionariesSubject.class).sheet("sheet").doWrite(userList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


