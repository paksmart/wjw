package com.wjw.admin.exception;

import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Auther: psk
 * @Date: 2022/11/23 16:14
 * @Description: 通用异常处理
 */


//@RestControllerAdvice
//public class BaseException {
//
//    @ExceptionHandler(value = Exception.class)
//    public ResponseResult handleException(Exception e) {
//        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
//    }
//}
