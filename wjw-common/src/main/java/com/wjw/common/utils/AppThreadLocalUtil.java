package com.wjw.common.utils;


import com.wjw.common.dto.Admin;

public class AppThreadLocalUtil {

    private static ThreadLocal<Admin> threadLocal = new ThreadLocal<>();

    /**
     * 获取用户
     *
     * @return
     */
    public static Admin get() {
        return threadLocal.get();
    }

    /**
     * 设置用户
     *
     * @param admin
     */
    public static void set(Admin admin) {
        threadLocal.set(admin);
    }

    /**
     * 清理本地线程,防止出现内存泄露
     */
    public static void remove() {
        threadLocal.remove();
    }
}
