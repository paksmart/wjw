package com.wjw.common.interceptor;

import com.wjw.common.dto.Admin;
import com.wjw.common.utils.AppThreadLocalUtil;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//登录拦截器
@Component
public class LoginCheckInterceptor implements HandlerInterceptor {

    /**
    * @description: 前置过滤
    * @param:
    * @return:
    * @author psk
    * @date:
    */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 从请求头中获取用户信息  employeeId
        String employeeId = request.getHeader("employeeId");
        // 将用户信息存入到ThreadLocal
        if (!StringUtils.isEmpty(employeeId)) {
            Admin admin = new Admin();
            admin.setAdminId(Integer.parseInt(employeeId));
            AppThreadLocalUtil.set(admin);
        }
        return true;
    }

    /**
    * @description: 后置过滤
    * @param:
    * @return:
    * @author psk
    * @date:
    */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        AppThreadLocalUtil.remove();
    }
}
