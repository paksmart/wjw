package com.wjw.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数字枚举
 *
 * @author 18518
 */
@Getter
@AllArgsConstructor
public enum NumberEnum {
  /**
   * 数字枚举
   */
  ZERO(0, "0"),
  ONE(1, "1"),
  TWO(2, "2"),
  THREE(3, "3"),
  FOUR(4, "4"),
  FIVE(5, "5"),
  SIX(6, "6"),
  SEVEN(7, "7"),
  NINE(9, "9"),
  TEN(10, "10"),
  TWENTY(20, "20"),
  ONE_HUNDRED(100, "100"),
  ONE_HUNDRED_ONE(101, "101"),
  TWENTY_THOUSAND(20000, "20000"),
  LOAD_ONE(-1, "-1"),
  LOAD_TWO(-2, "-2"),
  NEGATIVE_TWO(-2, "-2"),
  ;

  private Integer value;
  private String desc;

}
