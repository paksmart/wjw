package com.wjw.common.exception;


import com.wjw.common.dto.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *  自定义异常处理
 * @Date 2021/8/26 10:20
 * @Author lianDongCui
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class TstRuntimeException extends RuntimeException{

    private static final long serialVersionUID = -2470461654663264392L;

    private Integer errorCode;
    private String message;

    public TstRuntimeException() {
        super();
    }

    public TstRuntimeException(String message) {
        super(message);
        this.message = message;
    }

    public TstRuntimeException(Integer errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }

    public TstRuntimeException(ResultCode apiCode) {
        super(apiCode.getMessage());
        this.errorCode = apiCode.getCode();
        this.message = apiCode.getMessage();
    }

    public TstRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TstRuntimeException(Throwable cause) {
        super(cause);
    }

}
