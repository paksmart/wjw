package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 13:19
 * @Description:
 */

@Data
public class FilingOpenDto extends PageRequestDto {
    /**
     * 医疗人员姓名
     */
    @ApiModelProperty("医疗人员姓名")
    private String staffName;
}
