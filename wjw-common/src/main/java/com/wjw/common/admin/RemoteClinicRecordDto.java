package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 14:16
 * @Description:
 */

@Data
public class RemoteClinicRecordDto extends PageRequestDto {
    private String patientName;
}
