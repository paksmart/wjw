package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 14:23
 * @Description:
 */

@Data
public class TeleconsultationRecordsDto extends PageRequestDto {
    private String diagianName;
}
