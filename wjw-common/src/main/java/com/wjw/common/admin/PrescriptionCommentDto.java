package com.wjw.common.admin;
import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 14:09
 * @Description:
 */

@Data
public class PrescriptionCommentDto extends PageRequestDto {
    private String patientName;
}
