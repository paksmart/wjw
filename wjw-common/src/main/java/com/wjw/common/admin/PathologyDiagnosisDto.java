package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 13:54
 * @Description:
 */

@Data
public class PathologyDiagnosisDto extends PageRequestDto {
    private String patientName;
}
