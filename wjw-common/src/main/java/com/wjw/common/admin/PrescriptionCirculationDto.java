package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 14:04
 * @Description:
 */

@Data
public class PrescriptionCirculationDto extends PageRequestDto {
    private String prescriptionNo;
}
