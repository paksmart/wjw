package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Auther: psk
 * @Date: 2023/1/4 9:36
 * @Description:
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ComplaintRecordsDto extends PageRequestDto {


}
