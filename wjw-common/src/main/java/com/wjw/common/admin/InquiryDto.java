package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 13:45
 * @Description:
 */

@Data
public class InquiryDto extends PageRequestDto {
    private String patientName;
}
