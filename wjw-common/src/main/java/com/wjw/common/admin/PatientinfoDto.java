package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 13:58
 * @Description:
 */

@Data
public class PatientinfoDto extends PageRequestDto {
    private String userName;
}
