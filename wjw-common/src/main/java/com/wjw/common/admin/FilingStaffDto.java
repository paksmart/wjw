package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @description filing_staff
 * @author zhengkai.blog.csdn.net
 * @date 2022-11-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FilingStaffDto extends PageRequestDto {

    private static final long serialVersionUID = 1L;

    /**
    * 医疗人员姓名
    */
    @ApiModelProperty("医疗人员姓名")
    private String staffName;

}
