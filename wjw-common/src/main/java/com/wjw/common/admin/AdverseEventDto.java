package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 13:05
 * @Description:
 */
@Data
public class AdverseEventDto extends PageRequestDto {

    private String patientName;
}
