package com.wjw.common.admin;

import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 14:01
 * @Description:
 */

@Data
public class PrescriptionCheckDto extends PageRequestDto {
    private String pharName;
}
