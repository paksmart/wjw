package com.wjw.common.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjw.common.dto.PageRequestDto;
import lombok.Data;

/**
 * @Auther: psk
 * @Date: 2023/1/4 13:48
 * @Description:
 */

@Data
public class MedicalRecordDto extends PageRequestDto {
    private String patientName;
}
