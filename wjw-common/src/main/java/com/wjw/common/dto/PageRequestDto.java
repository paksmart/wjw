package com.wjw.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Setter;

/**
 * 公共分页请求对象
 */
@Setter
public class PageRequestDto {
    /**
     * 每页条数
     */
    @ApiModelProperty("每页条数")
    protected Integer pageSize;
    /**
     * 当前页
     */
    @ApiModelProperty("当前页")
    protected Long page;

    public Integer getPageSize() {
        if (this.pageSize == null || this.pageSize <= 0 || this.pageSize > 100) {
            setPageSize(10);
        }
        return pageSize;
    }

    public Long getPage() {
        if (this.page == null || this.page <= 0) {
            setPage(1L);
        }
        return page;
    }
}
