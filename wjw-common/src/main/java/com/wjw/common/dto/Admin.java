package com.wjw.common.dto;

import lombok.Data;

@Data
public class Admin {
    /**
     * 用户编号
     */
    private Integer adminId;
}
