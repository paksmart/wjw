package com.wjw.common.dto;

import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * 查询参数基类，含分页参数等
 * @author liandongCui
 */
public class BaseParam implements Serializable {

    private static final long  serialVersionUID     = -6608380835822261826L;
    /**
     * 最大页面尺寸
     */
    private static final int   MAX_PAGE_SIZE        = 5000;
    /**
     * 缺省页面尺寸
     */
    private static final int   DEFAULT_RESULT_COUNT = 10;
    /**
     * 缺省页号
     */
    private static final int   DEFAULT_PAGE_NUM     = 1;
    /**
     * 排序趋势 ASC-升序势
     */
    public static final String ORDER_BY_TREND_ASC   = "ASC";
    /**
     * 排序趋势 DESC-降序
     */
    public static final String ORDER_BY_TREND_DESC  = "DESC";

    /**
     * 页号
     */
    @ApiModelProperty("页号")
    private int                page                 = DEFAULT_PAGE_NUM;
    /**
     * 页面尺寸
     */
    @ApiModelProperty("每页展示几条")
    private int                pageSize             = DEFAULT_RESULT_COUNT;

    /**
     * 获取起始记录数
     * @return
     */
    public int getStartNum() {

        this.adjustPage();

        int start = (page - 1) * pageSize;
        return start;
    }

    /**
     * 获取结束记录数
     * @return
     */
    public int getEndNum() {
        int end = getStartNum() + pageSize - 1;
        return end;
    }

    /**
     * 获取页号
     * @return
     */
    public int getPage() {
        return page;
    }

    /**
     * 设置页号
     * @param page
     */
    public void setPage(int page) {
        this.page = page >= DEFAULT_PAGE_NUM ? page : DEFAULT_PAGE_NUM;
    }

    /**
     * 获取页面尺寸
     * @return
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 设置页面尺寸
     * @param pageSize
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize <= 0 ? DEFAULT_RESULT_COUNT : pageSize > MAX_PAGE_SIZE ? MAX_PAGE_SIZE : pageSize;
    }

    /**
     * 获取最大页面尺寸
     * @return
     */
    public int maxResultCount() {
        return MAX_PAGE_SIZE;
    }

    /**
     * 校正页面尺寸和页号
     */
    private void adjustPage() {
        // 校正页面尺寸
        if (pageSize <= 0 || pageSize > MAX_PAGE_SIZE) {
            pageSize = DEFAULT_RESULT_COUNT;
        }
        // 校正页号
        if (page <= 0) {
            page = 1;
        }
    }

    /**
     * 输出属性和值
     * @return
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
