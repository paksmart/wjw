package com.wjw.service;

import com.wjw.center.admin.domain.AdverseEventDO;
import com.wjw.center.admin.param.AdverseEventParam;
import com.wjw.common.admin.AdverseEventDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description:
* @author pansh
* @date 2022/9/20 14:34
* @version 1.0
*/

public interface AdverseEventService {
    ResponseResult saveAdverseEvent(AdverseEventDO adverseEventDO);

    ResponseResult getAdverseEvent(AdverseEventDO adverseEventDO);

    ResponseResult getByIdAdverseEvent(Long platformId);

    ResponseResult deleteAdverseEvent(Long platformId);

    ResponseResult updateAdverseEvent(AdverseEventDO adverseEventDO);

    ResponseResult getPageAdverseEvent( AdverseEventDto dto);
}
