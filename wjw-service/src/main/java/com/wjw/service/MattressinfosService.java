package com.wjw.service;


import com.wjw.center.admin.domain.MattressinfosDO;
import com.wjw.center.admin.param.MattressinfosParam;
import com.wjw.common.dto.ResponseResult;

/**
 * 床垫的实时数据服务接口
 *
 * @author psk
 * @since 2022-09-14 15:19:57
 * @description 由 psk 创建
 */
public interface MattressinfosService  {

    ResponseResult saveMattressInfos(MattressinfosParam param);

    ResponseResult GetMattressInfos(MattressinfosParam param);

    ResponseResult getById(Long deviceModelId);

    ResponseResult delete(Long deviceModelId);

    ResponseResult update(MattressinfosDO mattressinfosDO);


//    ResponseResult getLogPath(Long deviceModelId);
}
