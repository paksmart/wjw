package com.wjw.service;

import com.wjw.center.admin.domain.MedicalRecordDO;
import com.wjw.center.admin.param.MedicalRecordParam;
import com.wjw.common.admin.MedicalRecordDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 15:32
* @version 1.0
*/
public interface MedicalRecordService {
    ResponseResult saveMedicalRepord(MedicalRecordDO medicalRecordDO);

    ResponseResult getMedicalRepord(MedicalRecordParam param);

    ResponseResult getByIdMedicalRepord(Long platformId);

    ResponseResult updateMedicalRepord(MedicalRecordDO medicalRecordDO);

    ResponseResult deleteMedicalRepord(Long platformId);

    ResponseResult getPageMedicalRepord(MedicalRecordDto dto);
}
