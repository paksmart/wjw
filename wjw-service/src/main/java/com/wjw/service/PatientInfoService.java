package com.wjw.service;

import com.wjw.center.admin.domain.PatientinfoDO;
import com.wjw.center.admin.param.PatientinfoParam;
import com.wjw.common.admin.PatientinfoDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/15 9:18
* @version 1.0
*/
public interface PatientInfoService {
    ResponseResult savePatientInfo(PatientinfoDO patientinfoDO);

    ResponseResult getPatientInfo(PatientinfoParam param);

    ResponseResult getById(Long platformId);

    ResponseResult delete(Long platformId);

    ResponseResult update(PatientinfoDO patientinfoDO);

    ResponseResult getPagePatientInfo(PatientinfoDto dto);
}
