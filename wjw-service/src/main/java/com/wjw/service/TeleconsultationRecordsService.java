package com.wjw.service;

import com.wjw.center.admin.domain.TeleconsultationRecordsDO;
import com.wjw.common.admin.TeleconsultationRecordsDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/29 10:34
* @version 1.0
*/
public interface TeleconsultationRecordsService {
    ResponseResult saveTeleconsultationRecords(TeleconsultationRecordsDO teleconsultationRecordsDO);

    ResponseResult getTeleconsultationRecords(TeleconsultationRecordsDO teleconsultationRecordsDO);

    ResponseResult getByIdTeleconsultationRecords(Long platformId);

    ResponseResult deleteTeleconsultationRecords(Long platformId);

    ResponseResult updateTeleconsultationRecords(TeleconsultationRecordsDO teleconsultationRecordsDO);

    ResponseResult getPageTeleconsultationRecords(TeleconsultationRecordsDto dto);
}
