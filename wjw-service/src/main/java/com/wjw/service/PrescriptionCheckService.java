package com.wjw.service;

import com.wjw.center.admin.domain.PrescriptionCheckDO;
import com.wjw.center.admin.param.PrescriptionCheckParam;
import com.wjw.common.admin.PrescriptionCheckDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 15:35
* @version 1.0
*/


public interface PrescriptionCheckService {
    ResponseResult savePrescriptionCheck(PrescriptionCheckDO prescriptionCheckDO);

    ResponseResult getPrescriptionCheck(PrescriptionCheckParam param);

    ResponseResult getByIdPrescriptionCheck(Long platformId);

    ResponseResult deletePrescriptionCheck(Long platformId);

    ResponseResult updatePrescriptionCheck(PrescriptionCheckDO prescriptionCheckDO);

    ResponseResult getPagePrescriptionCheck(PrescriptionCheckDto dto);
}
