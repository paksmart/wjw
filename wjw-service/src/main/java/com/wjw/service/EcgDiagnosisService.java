package com.wjw.service;


import com.wjw.center.admin.domain.EcgDiagnosisDO;
import com.wjw.common.admin.EcgDiagnosisDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/27 14:11
* @version 1.0
*/


public interface EcgDiagnosisService {
    ResponseResult saveEcgDiagnosis(EcgDiagnosisDO ecgDiagnosisDO);

    ResponseResult getEcgDiagnosis(EcgDiagnosisDO ecgDiagnosisDO);

    ResponseResult getByIdEcgDiagnosis(Long platformId);

    ResponseResult deleteEcgDiagnosis(Long platformId);

    ResponseResult updateEcgDiagnosis(EcgDiagnosisDO ecgDiagnosisDO);

    ResponseResult getPageEcgDiagnosis(EcgDiagnosisDto dto);
}
