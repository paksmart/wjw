package com.wjw.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wjw.center.admin.domain.FilingStaffDO;
import com.wjw.common.admin.FilingStaffDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/16 16:24
* @version 1.0
*/
public interface FilingStaffService extends IService<FilingStaffDO> {
    ResponseResult saveFilingStaff(FilingStaffDO filingStaffDO);

    ResponseResult getById(Long platformId);

    ResponseResult delete(Long platformId);

    ResponseResult update(FilingStaffDO filingStaffDO);

    ResponseResult getPageFilingStaff(FilingStaffDto dto);
}
