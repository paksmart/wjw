package com.wjw.service;

import com.wjw.center.admin.domain.SubsequentVisitDO;
import com.wjw.center.admin.param.SubsequentVisitParam;
import com.wjw.common.admin.SubsequentVisitDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 11:11
* @version 1.0
*/
public interface SubsequentVisitService {

    ResponseResult getSubsequentVisit(SubsequentVisitParam param);

    ResponseResult getByIdSubsequentVisit(Long platformId);

    ResponseResult deleteSubsequentVisit(Long platformId);

    ResponseResult updateSubsequentVisit(SubsequentVisitDO subsequentVisitDO);

    ResponseResult getPageSubsequentVisit(SubsequentVisitDto dto);

    ResponseResult saveSubsequentVisit(SubsequentVisitDO subsequentVisitDO);
}
