package com.wjw.service;
/**
* @description: psk
* @author pansh
* @date 2022/10/4 15:32
* @version 1.0
*/
public interface PictureService {
    String getImageById(String id);
}
