package com.wjw.service;

import com.wjw.center.admin.dictionaries.*;
import com.wjw.common.dto.ResponseResult;

/**
* @description: service
* @author psk
* @date 2022/10/17 9:16
* @version 1.0
*/
public interface  DictionariesService {
    ResponseResult dictionaries(DictionariesNation dictionariesNation);

    ResponseResult dictionariesSubjectCode(DictionariesSubject dictionariesSubject);

    ResponseResult dictionariesTitle(DictionariesDoctorTitle dictionariesDoctorTitle);

    ResponseResult dictionariesCertType(DictionariesCertType dictionariesCertType);

    ResponseResult dictionariesBusiness(DictionariesBusiness dictionariesBusiness);

    ResponseResult dictionariesSex(DictionariesSex dictionariesSex);

    ResponseResult dictionariesArea(DictionariesArea dictionariesArea);

    ResponseResult dictionariesIcd(DictionariesIcd dictionariesIcd);
}
