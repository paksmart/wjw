package com.wjw.service;

import com.wjw.center.admin.domain.PrescriptionCommentDO;
import com.wjw.common.admin.PrescriptionCommentDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description:
* @author pansh
* @date 2022/9/26 15:02
* @version 1.0
*/
public interface PrescriptionCommentService {
    ResponseResult savePrescriptionComment(PrescriptionCommentDO prescriptionCommentDO);

    ResponseResult getPrescriptionComment(PrescriptionCommentDO prescriptionCommentDO);

    ResponseResult getPagePrescriptionComment(PrescriptionCommentDto dto);

    ResponseResult getByIdPrescriptionComment(Long platformId);

    ResponseResult deletePrescriptionComment(Long platformId);

    ResponseResult updatePrescriptionComment(PrescriptionCommentDO prescriptionCommentDO);
}
