package com.wjw.service;


import com.wjw.center.admin.domain.FilingOpenDO;
import com.wjw.center.admin.param.FilingOpenParam;
import com.wjw.common.admin.FilingOpenDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 8:34
* @version 1.0
*/
public interface FilingOpenService {
    ResponseResult saveFilingOpen(FilingOpenDO filingOpenDO);

    ResponseResult detail(Long platformId);

    ResponseResult getFilingOpen(FilingOpenParam param);

    ResponseResult deleteFilingOpen(Long platformId);

    ResponseResult  updateFilingOpen(FilingOpenDO filingOpenD);

    ResponseResult getPageFilingOpen( FilingOpenDto dto);
}
