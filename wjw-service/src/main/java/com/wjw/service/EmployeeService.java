package com.wjw.service;

import com.wjw.center.admin.domain.Employee;
import com.wjw.common.admin.EmployeeDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: service
* @author pansh
* @date 2022/10/22 13:41
* @version 1.0
*/
public interface EmployeeService {
    ResponseResult login(Employee employee);

    ResponseResult saveEmployee(Employee employee);

    ResponseResult getPageEmployee(EmployeeDto dto);

    ResponseResult getByIdEmployee(Long id);

    ResponseResult deleteByIdEmployee(Long id);

    ResponseResult updateEmployee(Employee employee);
}
