package com.wjw.service;

import com.wjw.center.admin.domain.InquiryDO;
import com.wjw.center.admin.param.InquiryParam;
import com.wjw.common.admin.InquiryDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 17:43
* @version 1.0
*/
public interface InquiryService {

    ResponseResult saveInquiry(InquiryDO inquiryDO);

    ResponseResult getInquiry(InquiryParam param);

    ResponseResult getByIdInquiry(Long platformId);

    ResponseResult deleteInquiry(Long platformId);

    ResponseResult updateInquiry(InquiryDO inquiryDO);

    ResponseResult getPageInquiry(InquiryDto dto);
}
