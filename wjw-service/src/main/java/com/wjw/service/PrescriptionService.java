package com.wjw.service;


import com.wjw.center.admin.domain.PrescriptionDO;
import com.wjw.common.admin.PrescriptionDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/24 17:30
* @version 1.0
*/
public interface PrescriptionService {
    ResponseResult savePrescription(PrescriptionDO prescriptionDO);

    ResponseResult getPrescription(PrescriptionDO prescriptionDO);


    ResponseResult getByIdPrescription(Long platformId);

    ResponseResult deletePrescription(Long platformId);

    ResponseResult updatePrescription(PrescriptionDO prescriptionDO);

    ResponseResult getPagePrescription(PrescriptionDto dto);
}
