package com.wjw.service;


import com.wjw.center.admin.domain.EvaluateRecordsDO;
import com.wjw.center.admin.param.EvaluateRecordsParam;
import com.wjw.common.admin.EvaluateRecordsDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 16:20
* @version 1.0
*/

public interface EvaluateRecordsService {
    ResponseResult saveEvaluateRecords(EvaluateRecordsDO evaluateRecordsDO);

    ResponseResult getEvaluateRecords(EvaluateRecordsParam param);

    ResponseResult getByIdEvaluateRecords(Long platformId);

    ResponseResult deleteEvaluateRecords(Long platformId);

    ResponseResult updateEvaluateRecords(EvaluateRecordsDO evaluateRecordsDO);

    ResponseResult getPageEvaluateRecords(EvaluateRecordsDto dto);
}
