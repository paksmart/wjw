package com.wjw.service;

import com.wjw.center.admin.domain.ImagingDiagnosisDO;
import com.wjw.common.admin.ImagingDiagnosisDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/27 9:41
* @version 1.0
*/
public interface ImagingDiagnosisService {
    ResponseResult saveImagingDiagnosis(ImagingDiagnosisDO imagingDiagnosisDO);

    ResponseResult getImagingDiagnosis(ImagingDiagnosisDO imagingDiagnosisDO);

    ResponseResult getByIdImagingDiagnosis(Long platformId);

    ResponseResult deleteImagingDiagnosis(Long platformId);

    ResponseResult updateImagingDiagnosis(ImagingDiagnosisDO imagingDiagnosisDO);

    ResponseResult getPageImagingDiagnosis(ImagingDiagnosisDto dto);
}
