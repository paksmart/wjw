package com.wjw.service;

import com.wjw.center.admin.domain.PrescriptionCirculationDO;
import com.wjw.center.admin.param.PrescriptionCirculationParam;
import com.wjw.common.admin.PrescriptionCirculationDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 18:43
* @version 1.0
*/

public interface PrescriptionCirculationService {
    ResponseResult savePrescriptionCirculation(PrescriptionCirculationDO prescriptionCirculationDO);

    ResponseResult getPrescriptionCirculation(PrescriptionCirculationDO param);

    ResponseResult getByIdPrescriptionCirculation(Long platformId);

    ResponseResult deletePrescriptionCirculation(Long platformId);

    ResponseResult updatePrescriptionCirculation(PrescriptionCirculationDO prescriptionCirculationDO);

    ResponseResult getPagePrescriptionCirculation(PrescriptionCirculationDto dto);
}
