package com.wjw.service;

import com.wjw.center.admin.domain.PathologyDiagnosisDO;
import com.wjw.common.admin.PathologyDiagnosisDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/29 15:44
* @version 1.0
*/
public interface PathologyDiagnosisService {
    ResponseResult savePathologyDiagnosis(PathologyDiagnosisDO pathologyDiagnosisDO);

    ResponseResult getPathologyDiagnosis(PathologyDiagnosisDO pathologyDiagnosisDO);

    ResponseResult getByIdPathologyDiagnosis(Long platformId);

    ResponseResult deletePathologyDiagnosis(Long platformId);

    ResponseResult updatePathologyDiagnosis(PathologyDiagnosisDO pathologyDiagnosisDO);

    ResponseResult getPagePathologyDiagnosis(PathologyDiagnosisDto dto);
}
