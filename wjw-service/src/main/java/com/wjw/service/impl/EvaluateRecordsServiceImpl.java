package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.EvaluateRecordsDO;
import com.wjw.center.admin.param.EvaluateRecordsParam;
import com.wjw.common.admin.EvaluateRecordsDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.EvaluateRecordsMapper;
import com.wjw.service.EvaluateRecordsService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author pansh
 * @version 1.0
 * @description: psk
 * @date 2022/9/20 16:20
 */

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class EvaluateRecordsServiceImpl implements EvaluateRecordsService {

    private final EvaluateRecordsMapper evaluateRecordsMapper;

    @Override
    public ResponseResult saveEvaluateRecords(EvaluateRecordsDO evaluateRecordsDO) {
            //如果参数为空返回错误
            if (evaluateRecordsDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            evaluateRecordsDO.setEvaluateId(IdUtil.getSnowflake(1,1).nextId());
            evaluateRecordsDO.setBusinessId(IdUtil.getSnowflake(1,1).nextId());
            evaluateRecordsDO.setEvaluatorId(IdUtil.getSnowflake(1,1).nextId());
            evaluateRecordsDO.setStaffId(IdUtil.getSnowflake(1,1).nextId());
            evaluateRecordsDO.setCreateUser(evaluateRecordsDO.getEvaluateId());
            //新增失败显示错误提示
            if (!SqlHelper.retBool(evaluateRecordsMapper.insert(evaluateRecordsDO))) {
                throw new TstRuntimeException("新增数据失败");
        }
        //把成功数据返回前端
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getEvaluateRecords(EvaluateRecordsParam param) {
        //如果参数为空的话,返回错误
        if (param == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        LambdaQueryWrapper<EvaluateRecordsDO> evaluateRecordsWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(param.getPlatformId())
                && StringUtils.isNotEmpty(param.getEvaluateId())) {
            evaluateRecordsWrapper.eq(EvaluateRecordsDO::getPlatformId, param.getPlatformId())
                                .eq(EvaluateRecordsDO::getEvaluateId, param.getEvaluateId());
        }
        List<EvaluateRecordsDO> evaluateRecordsDOList = evaluateRecordsMapper.selectList(evaluateRecordsWrapper);
        //把列表返回给前端
        return ResponseResult.okResult(evaluateRecordsDOList);
    }

    @Override
    public ResponseResult getByIdEvaluateRecords(Long platformId) {
        //根据Id来查询对象
        EvaluateRecordsDO evaluateRecordsDO = evaluateRecordsMapper.selectById(platformId);
        //如果对象为空,返回错误
        if (evaluateRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //把基本信息返回给前端
        return ResponseResult.okResult(evaluateRecordsDO);
    }

    @Override
    public ResponseResult deleteEvaluateRecords(Long platformId) {
        //如果删除失败,会显示错误信息
        if (!SqlHelper.retBool(evaluateRecordsMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateEvaluateRecords(EvaluateRecordsDO evaluateRecordsDO) {
        //如果对象为空,返回错误
        if (evaluateRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
       evaluateRecordsDO.setUpdateUser(evaluateRecordsDO.getEvaluateId());
        //如果修改失败,会显示错误信息
        if (!SqlHelper.retBool(evaluateRecordsMapper.updateById(evaluateRecordsDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(evaluateRecordsDO);
    }

    @Override
    public ResponseResult getPageEvaluateRecords(EvaluateRecordsDto dto) {
        //设置分页条件
        Page<EvaluateRecordsDO> page = new Page<>(dto.getPage(),dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<EvaluateRecordsDO> evaluateRecordsWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getStaffName())) {
            evaluateRecordsWrapper.like(EvaluateRecordsDO::getStaffName, dto.getStaffName());
        }
        IPage<EvaluateRecordsDO> evaluateRecordsDOIPage = evaluateRecordsMapper.selectPage(page, evaluateRecordsWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), evaluateRecordsDOIPage.getTotal(), evaluateRecordsDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
