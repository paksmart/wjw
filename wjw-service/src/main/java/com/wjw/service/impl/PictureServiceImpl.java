package com.wjw.service.impl;

import com.wjw.service.PictureService;
import com.wjw.mapper.PictureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @description: psk
* @author pansh
* @date 2022/10/4 16:01
* @version 1.0
*/
@Service
public class PictureServiceImpl implements PictureService {
    @Autowired
    private PictureMapper pictureMapper;
    @Override
    public String getImageById(String id) {
        return pictureMapper.getImageById(id);
    }
}
