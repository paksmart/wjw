package com.wjw.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.PatientinfoDO;
import com.wjw.center.admin.domain.patientinfoPatientsDo;
import com.wjw.center.admin.param.PatientinfoParam;
import com.wjw.common.admin.PatientinfoDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.PatientInfoMapper;
import com.wjw.mapper.patientinfoPatientsMapper;
import com.wjw.service.PatientInfoService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/15 9:18
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class PatientInfoServiceImpl implements PatientInfoService {

    private final PatientInfoMapper patientInfoMapper;
    private final patientinfoPatientsMapper patientinfoPatientsMapper;

    @Override
    public ResponseResult savePatientInfo(PatientinfoDO patientinfoDO) {
            //校验参数
            if (patientinfoDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //如果为新生儿童证件号为母亲证件号+胎数
            //定义胎数
            String fetusNum = "";
            if (patientinfoDO.getAge() == 1) {
                patientinfoDO.getCertNo().concat(fetusNum);
            }
            if (patientinfoDO.getAge() < NumberEnum.SIX.getValue()) {
                if (StringUtils.isEmpty(patientinfoDO.getGuardianCardNo())
                        && StringUtils.isEmpty(patientinfoDO.getGuardianName())) {
                    return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
                }
                patientinfoDO.setGuardianCardNo(patientinfoDO.getGuardianCardNo());
                patientinfoDO.setGuardianName(patientinfoDO.getGuardianName());
            }

            List<patientinfoPatientsDo> patientinfoPatientsList = patientinfoDO.getPatients();
            if (CollectionUtils.isNotEmpty(patientinfoPatientsList)) {
                for (patientinfoPatientsDo patients : patientinfoPatientsList) {
                    patients.setPatientId(patientinfoDO.getUserId());
                    if (patients.getPatientAge() < NumberEnum.SIX.getValue()) {
                        if (StringUtils.isEmpty(patients.getPatientGuardianCertNo()) && StringUtils.isEmpty(patients.getPatientGuardianName())) {
                            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
                        }
                        patients.setPatientGuardianCertNo(patientinfoDO.getGuardianCardNo());
                        patients.setPatientGuardianName(patientinfoDO.getGuardianName());
                    }
                    if (!SqlHelper.retBool(patientinfoPatientsMapper.insert(patients))) {
                        throw new TstRuntimeException("新增数据失败");
                    }
                }
            }
        return ResponseResult.okResult();
}

    @Override
    public ResponseResult getPatientInfo(PatientinfoParam param) {
        PatientinfoDO patientinfoDO = new PatientinfoDO();
        LambdaQueryWrapper<PatientinfoDO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(PatientinfoDO::getPlatformId,param.getPlatformId());
        wrapper.eq(PatientinfoDO::getUserId,param.getUserId());
        if (param.getUserName()!= null) {
            wrapper.like(PatientinfoDO::getUserName, param.getUserName());
        }
        List<patientinfoPatientsDo> patients = param.getPatients();
        for (patientinfoPatientsDo patient : patients) {
            LambdaQueryWrapper<patientinfoPatientsDo> patientinfoPatientsWrapper  = Wrappers.lambdaQuery();
            if (patient.getPatientId() != null) {
                patientinfoPatientsWrapper.eq(patientinfoPatientsDo::getPatientId, patient.getPatientId());
            }
            List<patientinfoPatientsDo> patientinfoPatientsDoList = patientinfoPatientsMapper.selectList(patientinfoPatientsWrapper);
            patientinfoDO.setPatients(patientinfoPatientsDoList);
        }
        List<PatientinfoDO> patientinfoDOList = patientInfoMapper.selectList(wrapper);
        patientinfoDOList.add(patientinfoDO);
        return ResponseResult.okResult(patientinfoDOList);
    }

    @Override
    public ResponseResult getById(Long platformId) {
        PatientinfoDO patientinfoDO = patientInfoMapper.selectById(platformId);
        if (patientinfoDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(patientinfoDO);
    }

    @Override
    public ResponseResult delete(Long platformId) {
        //根据id来删除
        if (!SqlHelper.retBool(patientInfoMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult update(PatientinfoDO patientinfoDO) {
       //更新患者信息
        patientInfoMapper.updateById(patientinfoDO);
        //根据就诊人的ID删除就诊人基本信息
        LambdaQueryWrapper<patientinfoPatientsDo> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(patientinfoPatientsDo::getPatientId, patientinfoDO.getUserId());
        patientinfoPatientsMapper.delete(wrapper);
        //将用户就诊人信息存在就诊人基本信息表中
        List<patientinfoPatientsDo> patientinfoPatientsDoList = patientinfoDO.getPatients();
        for (patientinfoPatientsDo patientinfoPatientsDo : patientinfoPatientsDoList) {
            patientinfoPatientsDo.setPatientId(patientinfoDO.getUserId());
            patientinfoPatientsMapper.insert(patientinfoPatientsDo);
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPagePatientInfo(PatientinfoDto dto) {
        //设置分页对象
        Page<PatientinfoDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<PatientinfoDO> patientinfoWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getUserName())) {
            patientinfoWrapper.like(PatientinfoDO::getUserName, dto.getUserName());
        }
        IPage<PatientinfoDO> patientinfoDOIPage = patientInfoMapper.selectPage(page, patientinfoWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), patientinfoDOIPage.getTotal(), patientinfoDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
