package com.wjw.service.impl;


import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.PrescriptionCheckDO;
import com.wjw.center.admin.param.PrescriptionCheckParam;
import com.wjw.common.admin.PrescriptionCheckDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.DictionariesDoctorTitleMapper;
import com.wjw.mapper.PrescriptionCheckMapper;
import com.wjw.service.PrescriptionCheckService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/19 15:35
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class PrescriptionCheckServiceImpl implements PrescriptionCheckService {

    private final PrescriptionCheckMapper prescriptionCheckMapper;
    private final DictionariesDoctorTitleMapper dictionariesDoctorTitleMapper;


    @Override
    public ResponseResult savePrescriptionCheck(PrescriptionCheckDO prescriptionCheckDO) {
            //如果参数为空,返回错误
            if (prescriptionCheckDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            prescriptionCheckDO.setPharId(IdUtil.getSnowflake(1,1).nextId());
            prescriptionCheckDO.setSubsequentVisitId(IdUtil.getSnowflake(1,1).nextId());
            prescriptionCheckDO.setPrescriptionNo(IdUtil.getSnowflake(1,1).nextId());
            if (!SqlHelper.retBool(prescriptionCheckMapper.insert(prescriptionCheckDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPrescriptionCheck(PrescriptionCheckParam param) {
        if (param == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<PrescriptionCheckDO> prescriptionCheckDOQueryWrapper  = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(param.getPlatformId()) && StringUtils.isNotEmpty(param.getPrescriptionNo())) {
            prescriptionCheckDOQueryWrapper.eq(PrescriptionCheckDO::getPlatformId, param.getPlatformId());
            prescriptionCheckDOQueryWrapper.eq(PrescriptionCheckDO::getPrescriptionNo, param.getPrescriptionNo());
        }
        List<PrescriptionCheckDO> prescriptionCheckDOList = prescriptionCheckMapper.selectList(prescriptionCheckDOQueryWrapper);
        return ResponseResult.okResult(prescriptionCheckDOList);
    }

    @Override
    public ResponseResult getByIdPrescriptionCheck(Long platformId) {
        //根据平台机构id查询对象
        PrescriptionCheckDO prescriptionCheckDO = prescriptionCheckMapper.selectById(platformId);
        //判断对象是否为空
        if (prescriptionCheckDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //返回给前端
        return ResponseResult.okResult(prescriptionCheckDO);
    }

    @Override
    public ResponseResult deletePrescriptionCheck(Long platformId) {
      //如果删除数据失败,显示错误信息
        if (!SqlHelper.retBool(prescriptionCheckMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updatePrescriptionCheck(PrescriptionCheckDO prescriptionCheckDO) {
        if (prescriptionCheckDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        prescriptionCheckDO.setUpdateTime(new Date());
        prescriptionCheckDO.setUpdateUser(1L);
        if (!SqlHelper.retBool(prescriptionCheckMapper.updateById(prescriptionCheckDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(prescriptionCheckDO);
    }

    @Override
    public ResponseResult getPagePrescriptionCheck(PrescriptionCheckDto dto) {
        //设置分页对象
        Page<PrescriptionCheckDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页查询条件
        LambdaQueryWrapper<PrescriptionCheckDO> prescriptionCheckWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPharName())) {
            prescriptionCheckWrapper.like(PrescriptionCheckDO::getPharName, dto.getPharName());
        }
        IPage<PrescriptionCheckDO> prescriptionCheckDOIPage = prescriptionCheckMapper.selectPage(page, prescriptionCheckWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), prescriptionCheckDOIPage.getTotal(), prescriptionCheckDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
