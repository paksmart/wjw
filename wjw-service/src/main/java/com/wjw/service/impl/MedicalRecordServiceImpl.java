package com.wjw.service.impl;


import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.MedicalRecordDO;
import com.wjw.center.admin.param.MedicalRecordParam;
import com.wjw.common.admin.MedicalRecordDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.MedicalRecordMapper;
import com.wjw.service.MedicalRecordService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 15:33
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class MedicalRecordServiceImpl implements MedicalRecordService {

    private final MedicalRecordMapper medicalRecordMapper;

    @Override
    public ResponseResult saveMedicalRepord(MedicalRecordDO medicalRecordDO) {
            if (medicalRecordDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            medicalRecordDO.setPatientId(IdUtil.getSnowflake(1,1).nextId());
            medicalRecordDO.setVisitSerialNo(IdUtil.getSnowflake(1,1).nextId());
            medicalRecordDO.setCaseNo(IdUtil.getSnowflake(1,1).nextId());
            medicalRecordDO.setCreateUser(medicalRecordDO.getPatientId());
            //如果数据新增失败,显示错误信息
            if (!SqlHelper.retBool(medicalRecordMapper.insert(medicalRecordDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getMedicalRepord(MedicalRecordParam param) {
        if (param == null) {
            ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        LambdaQueryWrapper<MedicalRecordDO> wrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(param.getPlatformId())) {
            wrapper.eq(MedicalRecordDO::getPlatformId, param.getPlatformId());
        }
        if (StringUtils.isNotEmpty(param.getCaseNo())) {
            wrapper.eq(MedicalRecordDO::getCaseNo, param.getCaseNo());
        }
        List<MedicalRecordDO> medicalRecordDOList = medicalRecordMapper.selectList(wrapper);
        if (medicalRecordDOList == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        return ResponseResult.okResult(medicalRecordDOList);
    }

    @Override
    public ResponseResult getByIdMedicalRepord(Long platformId) {
        //根据id查询
        MedicalRecordDO medicalRecordDO = medicalRecordMapper.selectById(platformId);
        if (medicalRecordDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(medicalRecordDO);
    }

    @Override
    public ResponseResult updateMedicalRepord(MedicalRecordDO medicalRecordDO) {
        if (medicalRecordDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        medicalRecordDO.setUpdateUser(medicalRecordDO.getPatientId());
        if (!SqlHelper.retBool(medicalRecordMapper.updateById(medicalRecordDO))) {
            throw new TstRuntimeException("修改数据错误");
        }
        return ResponseResult.okResult(medicalRecordDO);
    }

    @Override
    public ResponseResult deleteMedicalRepord(Long platformId) {
        //根据id来删除
        if (!SqlHelper.retBool(medicalRecordMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        //返回前端
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPageMedicalRepord(MedicalRecordDto dto) {
        //设置分页对象
        Page<MedicalRecordDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<MedicalRecordDO> medicalRecordDOLambdaQueryWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            medicalRecordDOLambdaQueryWrapper.like(MedicalRecordDO::getPatientName, dto.getPatientName());
        }
        IPage<MedicalRecordDO> medicalRecordDOIPage = medicalRecordMapper.selectPage(page, medicalRecordDOLambdaQueryWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), medicalRecordDOIPage.getTotal(), medicalRecordDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
