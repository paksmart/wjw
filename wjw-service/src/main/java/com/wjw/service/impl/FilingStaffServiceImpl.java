package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.FilingStaffDO;
import com.wjw.common.admin.FilingStaffDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.FilingStaffMapper;
import com.wjw.service.FilingStaffService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @description: psk
* @author pansh
* @date 2022/9/16 16:24
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@Service
@Transactional
public class FilingStaffServiceImpl extends ServiceImpl<FilingStaffMapper,FilingStaffDO> implements FilingStaffService {

    private final FilingStaffMapper filingStaffMapper;

    @Override
    public ResponseResult saveFilingStaff(FilingStaffDO filingStaffDO) {
            //如果参数为空,返回错误
            if (filingStaffDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            filingStaffDO.setStaffId(IdUtil.getSnowflake(1,1).nextId());
            filingStaffDO.setCreateUser(filingStaffDO.getPlatformId());
        //如果新增数据失败,显示错误信息
            if (!SqlHelper.retBool(filingStaffMapper.insert(filingStaffDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
            //返回给前端
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getById(Long platformId) {
        FilingStaffDO filingStaffDO = filingStaffMapper.selectById(platformId);
        if (filingStaffDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(filingStaffDO);
    }

    @Override
    public ResponseResult delete(Long platformId) {
        //如果删除数据失败,显示错误信息
        if (!SqlHelper.retBool(filingStaffMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        //返回给前端
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult update(FilingStaffDO filingStaffDO) {
        //如果修改数据失败,显示错误信息
        if (!SqlHelper.retBool(filingStaffMapper.updateById(filingStaffDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        //返回给前端
        return ResponseResult.okResult(filingStaffDO);
    }

    @Override
    public ResponseResult getPageFilingStaff(FilingStaffDto dto) {
        //设置分页对象
        Page<FilingStaffDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<FilingStaffDO> filingStaffWrapper = Wrappers.lambdaQuery();
        //判断医疗人员姓名是否为空,不为空,添加查询条件
        if (StringUtils.isNotEmpty(dto.getStaffName())) {
            filingStaffWrapper.eq(FilingStaffDO::getStaffName, dto.getStaffName());
        }
        //执行分页查询
        IPage<FilingStaffDO> iPage = this.page(page, filingStaffWrapper);
        //构造通用的分页响应
        PageResponseResult pageResponseResult =  new PageResponseResult(dto.getPage(), dto.getPageSize(), iPage.getTotal(), iPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}

