package com.wjw.service.impl;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.ImagingDiagnosisAppliedListDO;
import com.wjw.center.admin.domain.ImagingDiagnosisDO;
import com.wjw.common.admin.ImagingDiagnosisDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.*;
import com.wjw.service.ImagingDiagnosisService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/27 9:42
* @version 1.0
*/

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ImagingDiagnosisServiceImpl implements ImagingDiagnosisService {

    private final ImagingDiagnosisMapper imagingDiagnosisMapper;
    private final ImagingDiagnosisAppliedListMapper imagingDiagnosisAppliedListMapper;

    @Override
    public ResponseResult saveImagingDiagnosis(ImagingDiagnosisDO imagingDiagnosisDO) {
            //如果为空,返回错误信息
            if (imagingDiagnosisDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //新增互联网医院远程影像诊断记录监管
            imagingDiagnosisDO.setImagingDiagnosisId(IdUtil.getSnowflake(1, 1).nextId());
            imagingDiagnosisDO.setPatientId(IdUtil.getSnowflake(1, 1).nextId());
            imagingDiagnosisDO.setApplyDocId(IdUtil.getSnowflake(1,1).nextId());
            //影像诊断价格保留两位小数
            imagingDiagnosisDO.setImagingPrice(Double.valueOf(imagingDiagnosisDO.getImagingPrice()));
            if (!SqlHelper.retBool(imagingDiagnosisMapper.insert(imagingDiagnosisDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
            //新增互联网医院远程影像诊断记录监管接收的申请详情列表
            List<ImagingDiagnosisAppliedListDO> appliedList = imagingDiagnosisDO.getAppliedList();
            for (ImagingDiagnosisAppliedListDO imagingDiagnosisAppliedListDO : appliedList) {
                imagingDiagnosisAppliedListDO.setId(imagingDiagnosisDO.getImagingDiagnosisId());
                if (!SqlHelper.retBool(imagingDiagnosisAppliedListMapper.insert(imagingDiagnosisAppliedListDO))) {
                    throw new TstRuntimeException("新增数据失败");
                }
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getImagingDiagnosis(ImagingDiagnosisDO imagingDiagnosisDO) {
        //如果为空返回错误
        if (imagingDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<ImagingDiagnosisDO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ImagingDiagnosisDO::getPlatformId, imagingDiagnosisDO.getPlatformId());
        wrapper.eq(ImagingDiagnosisDO::getImagingDiagnosisId, imagingDiagnosisDO.getImagingDiagnosisId());
        //根据构造条件来查询所有
        List<ImagingDiagnosisDO> imagingDiagnosisDOList = imagingDiagnosisMapper.selectList(wrapper);
        //获取申请详情列表
        List<ImagingDiagnosisAppliedListDO> appliedList = imagingDiagnosisDO.getAppliedList();
        if (CollectionUtils.isNotEmpty(appliedList)) {
            for (ImagingDiagnosisAppliedListDO imagingDiagnosisAppliedListDO : appliedList) {
                LambdaQueryWrapper<ImagingDiagnosisAppliedListDO> wrapper1 = Wrappers.lambdaQuery();
                wrapper1.eq(ImagingDiagnosisAppliedListDO::getId, imagingDiagnosisAppliedListDO.getId());
                List<ImagingDiagnosisAppliedListDO> imagingDiagnosisAppliedListDOList = imagingDiagnosisAppliedListMapper.selectList(wrapper1);
                //把详情表放到集合中
                imagingDiagnosisDO.setAppliedList(imagingDiagnosisAppliedListDOList);
            }
        }
        imagingDiagnosisDOList.add(imagingDiagnosisDO);
        //把列表返回给前端
        return ResponseResult.okResult(imagingDiagnosisDOList);
    }

    @Override
    public ResponseResult getByIdImagingDiagnosis(Long platformId) {
        //根据ID来查询
        ImagingDiagnosisDO imagingDiagnosisDO = imagingDiagnosisMapper.selectById(platformId);
        //如果对象为空返回错误
        if (imagingDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询申请详情表
        LambdaQueryWrapper<ImagingDiagnosisAppliedListDO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ImagingDiagnosisAppliedListDO::getId, imagingDiagnosisDO.getImagingDiagnosisId());
        List<ImagingDiagnosisAppliedListDO> imagingDiagnosisAppliedListDOList = imagingDiagnosisAppliedListMapper.selectList(wrapper);
        imagingDiagnosisDO.setAppliedList(imagingDiagnosisAppliedListDOList);
        //把主列表返回给前端
        return ResponseResult.okResult(imagingDiagnosisDO);
    }

    @Override
    public ResponseResult deleteImagingDiagnosis(Long platformId) {
        //如果数据删除失败,显示错误信息
        if (!SqlHelper.retBool(imagingDiagnosisMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateImagingDiagnosis(ImagingDiagnosisDO imagingDiagnosisDO) {
        //更新互联网医院远程影像诊断记录监管
        if (!SqlHelper.retBool(imagingDiagnosisMapper.updateById(imagingDiagnosisDO))) {
            throw new TstRuntimeException("跟新数据失败");
        }
            //更新申请详情表
            //根据主键来删除申请详情表
            LambdaQueryWrapper<ImagingDiagnosisAppliedListDO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ImagingDiagnosisAppliedListDO::getId, imagingDiagnosisDO.getImagingDiagnosisId());
        imagingDiagnosisAppliedListMapper.delete(wrapper);
        //将更新过的数据存入详情表中
        List<ImagingDiagnosisAppliedListDO> appliedList = imagingDiagnosisDO.getAppliedList();
        if (CollectionUtils.isNotEmpty(appliedList)) {
            for (ImagingDiagnosisAppliedListDO imagingDiagnosisAppliedListDO : appliedList) {
                imagingDiagnosisAppliedListDO.setId(imagingDiagnosisDO.getImagingDiagnosisId());
                imagingDiagnosisAppliedListMapper.insert(imagingDiagnosisAppliedListDO);
            }
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPageImagingDiagnosis(ImagingDiagnosisDto dto) {
        //设置分页对象
        Page<ImagingDiagnosisDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<ImagingDiagnosisDO> imagingDiagnosisWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            imagingDiagnosisWrapper.like(ImagingDiagnosisDO::getPatientName, dto.getPatientName());
        }
        IPage<ImagingDiagnosisDO> imagingDiagnosisDOIPage = imagingDiagnosisMapper.selectPage(page, imagingDiagnosisWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), imagingDiagnosisDOIPage.getTotal(), imagingDiagnosisDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
