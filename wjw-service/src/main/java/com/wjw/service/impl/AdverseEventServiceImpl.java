package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.AdverseEventDO;
import com.wjw.common.admin.AdverseEventDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.AdverseEventMapper;
import com.wjw.service.AdverseEventService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author psk
 * @version 1.0
 * @description: psk
 * @date 2022/9/20 14:34
 */

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
public class AdverseEventServiceImpl implements AdverseEventService {

    private final AdverseEventMapper adverseEventMapper;

    @Override
    public ResponseResult saveAdverseEvent(AdverseEventDO adverseEventDO ) {
            //判断参数是否为空
            if (adverseEventDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            adverseEventDO.setEventId(IdUtil.getSnowflake(1,1).nextId());
            adverseEventDO.setPatientId(IdUtil.getSnowflake(1,1).nextId());
            adverseEventDO.setReportDeptId(IdUtil.getSnowflake(1,1).nextId());
            adverseEventDO.setCreateUser(adverseEventDO.getPatientId());
            if (!SqlHelper.retBool(adverseEventMapper.insert(adverseEventDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        return ResponseResult.okResult();
    }


    @Override
    public ResponseResult getAdverseEvent(AdverseEventDO adverseEventDO) {
        //如果参数为空,返回错误
        if (adverseEventDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<AdverseEventDO> adverseEventWrapper = Wrappers.lambdaQuery();
        //判断查询条件不能为空
            adverseEventWrapper.eq(AdverseEventDO::getPlatformId, adverseEventDO.getPlatformId())
                            .eq(AdverseEventDO::getEventId, adverseEventDO.getEventId());
        //根据查询条件来查询列表
        List<AdverseEventDO> adverseEventDOList = adverseEventMapper.selectList(adverseEventWrapper);
        //把列表返回给前端
        return ResponseResult.okResult(adverseEventDOList);
    }


    @Override
    public ResponseResult getByIdAdverseEvent(Long platformId) {
        //根据id来查询对象
        AdverseEventDO adverseEventDO = adverseEventMapper.selectById(platformId);
        //如果参数为空,返回错误
        if (adverseEventDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //把基本信息返回给前端
        return ResponseResult.okResult(adverseEventDO);
    }

    @Override
    public ResponseResult deleteAdverseEvent(Long platformId) {

        //如果删除失败,会显示删除失败信息
        if (!SqlHelper.retBool(adverseEventMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        //返回删除成功
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateAdverseEvent(AdverseEventDO adverseEventDO) {
        //如果对象为空的话,返回错误
        if (adverseEventDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        adverseEventDO.setUpdateUser(adverseEventDO.getPlatformId());
        //修改失败会显示信息
        if (!SqlHelper.retBool(adverseEventMapper.updateById(adverseEventDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(adverseEventDO);
    }

    @Override
    public ResponseResult getPageAdverseEvent(AdverseEventDto dto) {
        //设置分页对象
        Page<AdverseEventDO> page = new Page<>(dto.getPage(),dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<AdverseEventDO> adverseEventWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            adverseEventWrapper.like(AdverseEventDO::getPatientName, dto.getPatientName());
        }
        IPage<AdverseEventDO> adverseEventDOIPage = adverseEventMapper.selectPage(page, adverseEventWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), adverseEventDOIPage.getTotal(), adverseEventDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
