package com.wjw.service.impl;


import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.PrescriptionDO;
import com.wjw.center.admin.domain.PrescriptionPrescriptionDetailDO;
import com.wjw.common.admin.PrescriptionDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.PrescriptionMapper;
import com.wjw.mapper.PrescriptionPrescriptionDetailMapper;
import com.wjw.service.PrescriptionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/24 17:30
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class PrescriptionServiceImpl implements PrescriptionService {

    private final PrescriptionMapper prescriptionMapper;
    private final PrescriptionPrescriptionDetailMapper prescriptionPrescriptionDetailMapper;

    @Override
    public ResponseResult savePrescription(PrescriptionDO prescriptionDO) {
            if (prescriptionDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //如果合理用药审核结果根据合理用药的状态决定的
            if (prescriptionDO.getRationalDrugMark().equals(NumberEnum.ONE.getValue())) {
                prescriptionDO.setRationalDrugResult(prescriptionDO.getRationalDrugResult());
            }
            //如果诊断码和诊断名称为多个用逗号分隔
            if (prescriptionDO.getIcdCode().length() > 1 && prescriptionDO.getIcdName().length() > 1) {
                prescriptionDO.getIcdCode().split(",");
                prescriptionDO.getIcdName().split(",");
            }
            //如果处方类型为中药,帖数必填
            if (prescriptionDO.getPrescriptionType().equals(NumberEnum.THREE.getValue())) {
                prescriptionDO.setPacketsNum(prescriptionDO.getPacketsNum());
            }
            //处方金额,保留两位数
            prescriptionDO.setTotalFee(Double.valueOf(prescriptionDO.getTotalFee()));
            prescriptionDO.setPrescriptionNo(IdUtil.getSnowflake(1,1).nextId());
            prescriptionDO.setDocId(IdUtil.getSnowflake(1,1).nextId());
            prescriptionDO.setCheckDocId(IdUtil.getSnowflake(1,1).nextId());
            prescriptionDO.setCreateUser(prescriptionDO.getPlatformId());
            prescriptionDO.setPrescriptionRecordNo(IdUtil.getSnowflake(1,1).nextId());
            //新增数据
            prescriptionDO.setCreateTime(new Date());
            if (!SqlHelper.retBool(prescriptionMapper.insert(prescriptionDO))) {
                throw new TstRuntimeException("新增数据失败");
            }

            //新增处方明细信息
            List<PrescriptionPrescriptionDetailDO> prescriptionDetailList = prescriptionDO.getPrescriptionDetailList();
            for (PrescriptionPrescriptionDetailDO prescriptionPrescriptionDetailDO : prescriptionDetailList) {
                prescriptionPrescriptionDetailDO.setId(prescriptionDO.getPrescriptionNo());
                //药品代码"0o00"
                prescriptionPrescriptionDetailDO.setDrugCode("0000");
                //医院药品代码**CY-(1000+id)
                prescriptionPrescriptionDetailDO.setHospDrugCode("**CY-(1000"+prescriptionPrescriptionDetailDO.getId()+")");
                if (!SqlHelper.retBool(prescriptionPrescriptionDetailMapper.insert(prescriptionPrescriptionDetailDO))) {
                    throw new TstRuntimeException("新增数据失败");
                }
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPrescription(PrescriptionDO prescriptionDO) {
        //如果参数为空的,返回错误信息
        if (prescriptionDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<PrescriptionDO> prescriptionWrapper = Wrappers.lambdaQuery();
        prescriptionWrapper.eq(PrescriptionDO::getPlatformId, prescriptionDO.getPlatformId());

        //查询处方详细信息
        List<PrescriptionPrescriptionDetailDO> prescriptionDetailList = prescriptionDO.getPrescriptionDetailList();
        for (PrescriptionPrescriptionDetailDO prescriptionPrescriptionDetailDO : prescriptionDetailList) {
            LambdaQueryWrapper<PrescriptionPrescriptionDetailDO> prescriptionPrescriptionDetailWrapper = Wrappers.lambdaQuery();
            prescriptionPrescriptionDetailWrapper.eq(PrescriptionPrescriptionDetailDO::getId, prescriptionPrescriptionDetailDO.getId());
            List<PrescriptionPrescriptionDetailDO> prescriptionPrescriptionDetailDOList = prescriptionPrescriptionDetailMapper.selectList(prescriptionPrescriptionDetailWrapper);
            prescriptionDO.setPrescriptionDetailList(prescriptionPrescriptionDetailDOList);
        }
        //根据查询条件查询所有
        List<PrescriptionDO> prescriptionDOList = prescriptionMapper.selectList(prescriptionWrapper);
        prescriptionDOList.add(prescriptionDO);
        return ResponseResult.okResult(prescriptionDOList);
    }

    @Override
    public ResponseResult getByIdPrescription(Long platformId) {
        //根据id来查询对象
        PrescriptionDO prescriptionDO = prescriptionMapper.selectById(platformId);
        //如果对象为空的话,返回错误信息
        if (prescriptionDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(prescriptionDO);
    }

    @Override
    public ResponseResult deletePrescription(Long platformId) {
        //根据id来查询对象
        PrescriptionDO prescriptionDO = prescriptionMapper.selectById(platformId);
        //如果对象为空,返回错误
        if (prescriptionDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //如果删除状态为1的话,数据不存在
        if (prescriptionDO.getIsDeleted().equals(NumberEnum.ONE)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //设置删除
        prescriptionDO.setIsDeleted(1);
        //todo
        //删除处方详细信息
        List<PrescriptionPrescriptionDetailDO> prescriptionDetailList = prescriptionDO.getPrescriptionDetailList();
        for (PrescriptionPrescriptionDetailDO prescriptionPrescriptionDetailDO : prescriptionDetailList) {
            prescriptionPrescriptionDetailDO.setIsDeleted(1);
            prescriptionPrescriptionDetailMapper.deleteById(prescriptionPrescriptionDetailDO);
        }
        //如果删除失败的话显示错误信息
        if (!SqlHelper.retBool(prescriptionMapper.deleteById(prescriptionDO))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updatePrescription(PrescriptionDO prescriptionDO) {
        //更新处方信息
        prescriptionMapper.updateById(prescriptionDO);
        //根据主键来删除处方基本信息
        LambdaQueryWrapper<PrescriptionPrescriptionDetailDO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(PrescriptionPrescriptionDetailDO::getId, prescriptionDO.getPrescriptionNo());
        prescriptionPrescriptionDetailMapper.delete(wrapper);

        //将处方的信息存到处方的基本信息中
        List<PrescriptionPrescriptionDetailDO> prescriptionDetailList = prescriptionDO.getPrescriptionDetailList();
        for (PrescriptionPrescriptionDetailDO prescriptionPrescriptionDetailDO : prescriptionDetailList) {
            prescriptionPrescriptionDetailDO.setId(prescriptionDO.getPrescriptionNo());
            prescriptionPrescriptionDetailMapper.insert(prescriptionPrescriptionDetailDO);
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPagePrescription(PrescriptionDto dto) {
        //设置分页对象
        Page<PrescriptionDO> page = new Page<>(dto.getPage(), dto.getPageSize());

        //分页查询条件
        LambdaQueryWrapper<PrescriptionDO> prescriptionWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            prescriptionWrapper.like(PrescriptionDO::getPatientName, dto.getPatientName());
        }
        IPage<PrescriptionDO> prescriptionDOIPage = prescriptionMapper.selectPage(page, prescriptionWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), prescriptionDOIPage.getTotal(), prescriptionDOIPage.getRecords());
        //给全端返回响应的格式
        return ResponseResult.okResult(pageResponseResult);
    }
}
