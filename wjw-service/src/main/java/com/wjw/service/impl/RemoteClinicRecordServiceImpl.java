package com.wjw.service.impl;


import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.RemoteClinicRecordDO;
import com.wjw.center.admin.param.RemoteClinicRecordParam;
import com.wjw.center.admin.vo.RemoteClinicRecordVO;
import com.wjw.common.admin.RemoteClinicRecordDto;
import com.wjw.common.dto.PageRequestDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.*;
import com.wjw.service.RemoteClinicRecordService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 10:34
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class RemoteClinicRecordServiceImpl implements RemoteClinicRecordService {

    private final RemoteClinicRecordMapper remoteClinicRecordMapper;

    @Override
    public ResponseResult saveRemoteClinicRecord(RemoteClinicRecordDO remoteClinicRecordDO) {
        //判断参数是否为空,为空返回错误信息
            if (remoteClinicRecordDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //门诊价格保留两位小数
            remoteClinicRecordDO.setClinicPrice(Double.valueOf(remoteClinicRecordDO.getClinicPrice()));
            //远程诊疗全程留痕记录标识
            remoteClinicRecordDO.setRecordsMark(IdUtil.getSnowflake(1,1).nextId());
            remoteClinicRecordDO.setClinicRecordId(IdUtil.getSnowflake(1, 1).nextId());
            remoteClinicRecordDO.setAppliedDocId(IdUtil.getSnowflake(1,1).nextId());
            remoteClinicRecordDO.setApplyDocId(IdUtil.getSnowflake(1, 1).nextId());
            remoteClinicRecordDO.setPatientId(IdUtil.getSnowflake(1, 1).nextId());
            //取消描述(取消时必填)
            if (remoteClinicRecordDO.getStatus().equals(NumberEnum.TWO.getValue())) {
                remoteClinicRecordDO.setCancelDesc(remoteClinicRecordDO.getCancelDesc());
            }
            if (!SqlHelper.retBool(remoteClinicRecordMapper.insert(remoteClinicRecordDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getRemoteClinicRecord(RemoteClinicRecordParam param) {
        //判断参数是否为空
        if (param == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        LambdaQueryWrapper<RemoteClinicRecordDO> remoteClinicRecordDOWrapper  = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(param.getClinicRecordId())
                && param.getRecordType() != null) {
            remoteClinicRecordDOWrapper.eq(RemoteClinicRecordDO::getClinicRecordId, param.getClinicRecordId());
            remoteClinicRecordDOWrapper.eq(RemoteClinicRecordDO::getRecordType, param.getRecordType());
        }
        List<RemoteClinicRecordDO> remoteClinicRecordDOList = remoteClinicRecordMapper.selectList(remoteClinicRecordDOWrapper);
        return ResponseResult.okResult(remoteClinicRecordDOList);
    }

    @Override
    public ResponseResult getByIdRemoteClinicRecord(Long id) {
        //根据id来查询列表
        RemoteClinicRecordDO remoteClinicRecordDO = remoteClinicRecordMapper.selectById(id);
        //判断对象是否为空,为空返回错误
        if (remoteClinicRecordDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //把对象设置到基本信息中
        RemoteClinicRecordVO remoteClinicRecordVO = getRemoteClinicRecordVO(remoteClinicRecordDO);
        return ResponseResult.okResult(remoteClinicRecordVO);
    }

    @Override
    public ResponseResult deleteRemoteClinicRecord(Long id) {
        if (!SqlHelper.retBool(remoteClinicRecordMapper.deleteById(id))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateRemoteClinicRecord(RemoteClinicRecordDO remoteClinicRecordDO) {
        //判断参数是否为空
        if (remoteClinicRecordDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        remoteClinicRecordDO.setUpdateTime(new Date());
        remoteClinicRecordDO.setUpdateUser(1L);
        //如果修改失败,会显示出错误
        if (!SqlHelper.retBool(remoteClinicRecordMapper.updateById(remoteClinicRecordDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(remoteClinicRecordDO);
    }

    @Override
    public ResponseResult getPageRemoteClinicRecord(RemoteClinicRecordDto dto) {
        //设置分页对象
        Page<RemoteClinicRecordDO> page = new Page<>(dto.getPage(), dto.getPageSize());

        //分页条件查询
        LambdaQueryWrapper<RemoteClinicRecordDO> remoteClinicRecordWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            remoteClinicRecordWrapper.like(RemoteClinicRecordDO::getPatientName, dto.getPatientName());
        }
        IPage<RemoteClinicRecordDO> remoteClinicRecordDOIPage = remoteClinicRecordMapper.selectPage(page, remoteClinicRecordWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), remoteClinicRecordDOIPage.getTotal(), remoteClinicRecordDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }


    private RemoteClinicRecordVO getRemoteClinicRecordVO(RemoteClinicRecordDO remoteClinicRecordDO) {
        RemoteClinicRecordVO remoteClinicRecordVO = new RemoteClinicRecordVO();
        remoteClinicRecordVO.setClinicRecordId(remoteClinicRecordDO.getClinicRecordId());
        remoteClinicRecordVO.setRecordType(remoteClinicRecordDO.getRecordType());
        remoteClinicRecordVO.setOrderNum(remoteClinicRecordDO.getOrderNum());
        remoteClinicRecordVO.setStatus(remoteClinicRecordDO.getStatus());
        remoteClinicRecordVO.setMedicalInsuranceMark(remoteClinicRecordDO.getMedicalInsuranceMark());
        remoteClinicRecordVO.setApplyOrgCode(remoteClinicRecordDO.getApplyOrgCode());
        remoteClinicRecordVO.setApplyOrgName(remoteClinicRecordDO.getApplyOrgName());
        remoteClinicRecordVO.setApplyDocId(remoteClinicRecordDO.getApplyDocId());
        remoteClinicRecordVO.setApplyDocName(remoteClinicRecordDO.getApplyDocName());
        remoteClinicRecordVO.setApplyDocIdMobile(remoteClinicRecordDO.getApplyDocIdMobile());
        remoteClinicRecordVO.setApplyDocCertType(remoteClinicRecordDO.getApplyDocCertType());
        remoteClinicRecordVO.setApplyDocCertNo(remoteClinicRecordDO.getApplyDocCertNo());
        remoteClinicRecordVO.setApplyDocTitle(remoteClinicRecordDO.getApplyDocTitle());
        remoteClinicRecordVO.setApplyDoctorSubjectCode(remoteClinicRecordDO.getApplyDoctorSubjectCode());
        remoteClinicRecordVO.setApplyDoctorSubjectName(remoteClinicRecordDO.getApplyDoctorSubjectName());
        remoteClinicRecordVO.setApplyDeptId(remoteClinicRecordDO.getApplyDeptId());
        remoteClinicRecordVO.setApplyDeptName(remoteClinicRecordDO.getApplyDeptName());
        remoteClinicRecordVO.setAppliedDocId(remoteClinicRecordDO.getAppliedDocId());
        remoteClinicRecordVO.setAppliedDocName(remoteClinicRecordDO.getAppliedDocName());
        remoteClinicRecordVO.setAppliedDocIdMobile(remoteClinicRecordDO.getAppliedDocIdMobile());
        remoteClinicRecordVO.setAppliedDocCertNo(remoteClinicRecordDO.getAppliedDocCertNo());
        remoteClinicRecordVO.setAppliedDocTitle(remoteClinicRecordDO.getAppliedDocTitle());
        remoteClinicRecordVO.setAppliedDoctorSubjectCode(remoteClinicRecordDO.getAppliedDoctorSubjectCode());
        remoteClinicRecordVO.setAppliedDoctorSubjectName(remoteClinicRecordDO.getAppliedDoctorSubjectName());
        remoteClinicRecordVO.setAppliedOrgCode(remoteClinicRecordDO.getAppliedOrgCode());
        remoteClinicRecordVO.setAppliedOrgName(remoteClinicRecordDO.getAppliedOrgName());
        remoteClinicRecordVO.setAppliedDeptId(remoteClinicRecordDO.getAppliedDeptId());
        remoteClinicRecordVO.setAppliedDeptName(remoteClinicRecordDO.getAppliedDeptName());
        remoteClinicRecordVO.setApplyDate(remoteClinicRecordDO.getApplyDate());
        remoteClinicRecordVO.setStartTime(remoteClinicRecordDO.getStartTime());
        remoteClinicRecordVO.setEndTime(remoteClinicRecordDO.getEndTime());
        remoteClinicRecordVO.setClinicPrice(remoteClinicRecordDO.getClinicPrice());
        remoteClinicRecordVO.setPayStatus(remoteClinicRecordDO.getPayStatus());
        remoteClinicRecordVO.setDiagnosisInfo(remoteClinicRecordDO.getDiagnosisInfo());
        remoteClinicRecordVO.setApplySummary(remoteClinicRecordDO.getApplySummary());
        remoteClinicRecordVO.setAppliedSummary(remoteClinicRecordDO.getAppliedSummary());
        remoteClinicRecordVO.setPaySerialNo(remoteClinicRecordDO.getPaySerialNo());
        remoteClinicRecordVO.setCancelDesc(remoteClinicRecordDO.getCancelDesc());
        remoteClinicRecordVO.setPatientId(remoteClinicRecordDO.getPatientId());
        remoteClinicRecordVO.setPatientName(remoteClinicRecordDO.getPatientName());
        remoteClinicRecordVO.setPatientCertType(remoteClinicRecordDO.getPatientCertType());
        remoteClinicRecordVO.setPatientCertNo(remoteClinicRecordDO.getPatientCertNo());
        remoteClinicRecordVO.setPatientHealthCard(remoteClinicRecordDO.getPatientHealthCard());
        remoteClinicRecordVO.setPatientAge(remoteClinicRecordDO.getPatientAge());
        remoteClinicRecordVO.setPatientBirthday(remoteClinicRecordDO.getPatientBirthday());
        remoteClinicRecordVO.setPatientSex(remoteClinicRecordDO.getPatientSex());
        remoteClinicRecordVO.setPatientMobile(remoteClinicRecordDO.getPatientMobile());
        remoteClinicRecordVO.setRecordsMark(remoteClinicRecordDO.getRecordsMark());
        remoteClinicRecordVO.setCreateTime(new Date());
        return remoteClinicRecordVO;
    }
}
