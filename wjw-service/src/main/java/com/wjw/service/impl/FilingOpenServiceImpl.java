package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.FilingOpenDO;
import com.wjw.center.admin.param.FilingOpenParam;
import com.wjw.center.admin.vo.FilingOpenVO;
import com.wjw.common.admin.FilingOpenDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.FilingOpenMapper;
import com.wjw.service.FilingOpenService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/17 8:34
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@Service
@Transactional
public class FilingOpenServiceImpl implements FilingOpenService {

    private final FilingOpenMapper filingOpenMapper;

    @Override
    public ResponseResult saveFilingOpen(FilingOpenDO filingOpenDO) {
            if (filingOpenDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            filingOpenDO.setStaffId(IdUtil.getSnowflake(1,1).nextId());
            filingOpenDO.setCreateUser(filingOpenDO.getStaffId());
            //如果新增失败显示错误信息
            if (!SqlHelper.retBool(filingOpenMapper.insert(filingOpenDO))) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult detail(Long platformId) {
        //根据id来查询
        FilingOpenDO filingOpenDO = filingOpenMapper.selectById(platformId);
        if (filingOpenDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //把对象封装返回前端
        FilingOpenVO filingOpenVO = new FilingOpenVO();
        filingOpenVO.setPlatformId(filingOpenDO.getPlatformId());
        filingOpenVO.setStaffId(filingOpenDO.getStaffId());
        filingOpenVO.setStaffName(filingOpenDO.getStaffName());
        filingOpenVO.setStaffCertType(filingOpenDO.getStaffCertType());
        filingOpenVO.setStaffCertNo(filingOpenDO.getStaffCertNo());
        filingOpenVO.setBusinessType(filingOpenDO.getBusinessType());
        filingOpenVO.setFilingMark(filingOpenDO.getFilingMark());
        return ResponseResult.okResult(filingOpenVO);
    }

    @Override
    public ResponseResult getFilingOpen(FilingOpenParam param) {
        if (param == null) {
            ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        LambdaQueryWrapper<FilingOpenDO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(FilingOpenDO::getPlatformId, param.getPlatformId());
        wrapper.eq(FilingOpenDO::getStaffId, param.getStaffId());
        List<FilingOpenDO> filingOpenDOList = filingOpenMapper.selectList(wrapper);
        return ResponseResult.okResult(filingOpenDOList);
    }

    @Override
    public ResponseResult deleteFilingOpen(Long platformId) {
        //根据id删除
        if (!SqlHelper.retBool(filingOpenMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        //给前端返回成功
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateFilingOpen(FilingOpenDO filingOpenD) {
        //参数为空,返回参数无效
        if (filingOpenD==null) {
            ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //如果修改失败,显示错误信息
        if (!SqlHelper.retBool(filingOpenMapper.updateById(filingOpenD))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(filingOpenD);
    }

    @Override
    public ResponseResult getPageFilingOpen( FilingOpenDto dto) {
        //设置分页对象
        Page<FilingOpenDO> page = new Page<>(dto.getPage(),dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<FilingOpenDO> filingOpenDOWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getStaffName())) {
            filingOpenDOWrapper.like(FilingOpenDO::getStaffName, dto.getStaffName());
        }
        IPage<FilingOpenDO> filingOpenDOIPage = filingOpenMapper.selectPage(page, filingOpenDOWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), filingOpenDOIPage.getTotal(), filingOpenDOIPage.getRecords());
        //把分页信息返回给前端
        return ResponseResult.okResult(pageResponseResult);
    }
}
