package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.ComplaintRecordsDO;
import com.wjw.center.admin.param.ComplaintRecordsParam;
import com.wjw.common.admin.ComplaintRecordsDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.ComplaintRecordsMapper;
import com.wjw.service.ComplaintRecordsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/21 9:04
* @version 1.0
*/

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Transactional
public class ComplaintRecordsServiceImpl implements ComplaintRecordsService {

    private final ComplaintRecordsMapper complaintRecordsMapper;

    @Override
    public ResponseResult saveComplaintRecords(ComplaintRecordsDO complaintRecordsDO) {
            //如果参数为空,返回错误
            if (complaintRecordsDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            complaintRecordsDO.setComplaintId(IdUtil.getSnowflake(1,1).nextId());
            complaintRecordsDO.setCreateUser(complaintRecordsDO.getComplaintId());
            //如果新增数据失败,显示错误信息
            if (!SqlHelper.retBool(complaintRecordsMapper.insert(complaintRecordsDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getComplaintRecords(ComplaintRecordsParam param) {
        //如果参数为空,返回错误
        if (param == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<ComplaintRecordsDO> complaintRecordsWrapper = Wrappers.lambdaQuery();
        //判断对象不能为空
        if (StringUtils.isNotEmpty(param.getPlatformId()) && StringUtils.isNotEmpty(param.getComplaintId())) {
            complaintRecordsWrapper.eq(ComplaintRecordsDO::getPlatformId, param.getPlatformId())
                                   .eq(ComplaintRecordsDO::getComplaintId, param.getComplaintId());
        }
        //根据构建条件查询列表
        List<ComplaintRecordsDO> complaintRecordsDOList = complaintRecordsMapper.selectList(complaintRecordsWrapper);
        //把列表返回给前端
        return ResponseResult.okResult(complaintRecordsDOList);
    }

    @Override
    public ResponseResult getByIdComplaintRecords(Long platformId) {
        //根据id查询对象
        ComplaintRecordsDO complaintRecordsDO = complaintRecordsMapper.selectById(platformId);
        if (complaintRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(complaintRecordsDO);
    }

    @Override
    public ResponseResult deleteComplaintRecords(Long platformId) {
        //如果删除失败显示错误信息
        if (!SqlHelper.retBool(complaintRecordsMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateComplaintRecords(ComplaintRecordsDO complaintRecordsDO) {
        if (complaintRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        complaintRecordsDO.setUpdateUser(complaintRecordsDO.getComplaintId());
        //如果修改失败显示错误信息
        if (!SqlHelper.retBool(complaintRecordsMapper.updateById(complaintRecordsDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(complaintRecordsDO);
    }

    @Override
    public ResponseResult getPageComplaintRecords(ComplaintRecordsDto dto) {
        //设置分页对象
        Page<ComplaintRecordsDO> page = new Page<>(dto.getPage(),dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<ComplaintRecordsDO> complaintRecordsWrapper = Wrappers.lambdaQuery();
        complaintRecordsWrapper.eq(ComplaintRecordsDO::getIsDeleted, NumberEnum.ZERO.getValue());
        IPage<ComplaintRecordsDO> iPage = complaintRecordsMapper.selectPage(page, complaintRecordsWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), iPage.getTotal(), iPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
