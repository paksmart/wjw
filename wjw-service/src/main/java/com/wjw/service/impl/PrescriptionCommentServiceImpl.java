package com.wjw.service.impl;


import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.PrescriptionCommentDO;
import com.wjw.center.admin.domain.PrescriptionPrescriptionDetailDO;
import com.wjw.common.admin.PrescriptionCommentDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.PrescriptionCommentMapper;
import com.wjw.mapper.PrescriptionPrescriptionDetailMapper;
import com.wjw.service.PrescriptionCommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/26 15:02
* @version 1.0
*/

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PrescriptionCommentServiceImpl implements PrescriptionCommentService {

    private final PrescriptionCommentMapper prescriptionCommentMapper;
    private final PrescriptionPrescriptionDetailMapper prescriptionPrescriptionDetailMapper;

    @Override
    public ResponseResult savePrescriptionComment(PrescriptionCommentDO prescriptionCommentDO) {
        //如果对象为空的话,返回错误信息
        if (prescriptionCommentDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        prescriptionCommentDO.setCommentId(IdUtil.getSnowflake(1, 1).nextId());
        prescriptionCommentDO.setVisitId(IdUtil.getSnowflake(1, 1).nextId());
        prescriptionCommentDO.setPrescriptionNo(IdUtil.getSnowflake(1, 1).nextId());
        //新增处方点评信息
        //如果新增数据失败,显示错误信息
        if (!SqlHelper.retBool(prescriptionCommentMapper.insert(prescriptionCommentDO))) {
            throw new TstRuntimeException("新增数据失败");
        }
        //如果整点icd为多个使用逗号分隔
        if (prescriptionCommentDO.getIcdCode().length() > 1 && prescriptionCommentDO.getIcdName().length() > 1) {
            prescriptionCommentDO.getIcdCode().split(",");
            prescriptionCommentDO.getIcdName().split(",");
        }

        //新增处方明细信息
        List<PrescriptionPrescriptionDetailDO> prescriptionDetailList = prescriptionCommentDO.getPrescriptionDetailList();
        for (PrescriptionPrescriptionDetailDO prescriptionPrescriptionDetailDO : prescriptionDetailList) {
            //药品代码"0000"
            prescriptionPrescriptionDetailDO.setDrugCode("0000");
            //医院药品代码统一用**CY-(1000+id)
            prescriptionPrescriptionDetailDO.setHospDrugCode("**CY-(1000" + prescriptionPrescriptionDetailDO.getId() + ")");
            //如果新增数据失败,显示错误
            if (!SqlHelper.retBool(prescriptionPrescriptionDetailMapper.insert(prescriptionPrescriptionDetailDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        }
        //给前端返回成功
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPrescriptionComment(PrescriptionCommentDO prescriptionCommentDO) {
        //如果参数为空,返回错误
        if (prescriptionCommentDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        LambdaQueryWrapper<PrescriptionCommentDO> prescriptionCommentDOLambdaQueryWrapper = Wrappers.lambdaQuery();
        prescriptionCommentDOLambdaQueryWrapper.eq(PrescriptionCommentDO::getPlatformId, prescriptionCommentDO.getPlatformId());
        prescriptionCommentDOLambdaQueryWrapper.eq(prescriptionCommentDO.getPatientName() != null, PrescriptionCommentDO::getPatientName, prescriptionCommentDO.getPatientName());
        List<PrescriptionCommentDO> prescriptionCommentDOList = prescriptionCommentMapper.selectList(prescriptionCommentDOLambdaQueryWrapper);
        //查询处方明细信息
        LambdaQueryWrapper<PrescriptionPrescriptionDetailDO> wrapper = Wrappers.lambdaQuery();
        List<PrescriptionPrescriptionDetailDO> prescriptionPrescriptionDetailDOS = prescriptionPrescriptionDetailMapper.selectList(wrapper);
        prescriptionCommentDO.setPrescriptionDetailList(prescriptionPrescriptionDetailDOS);
        prescriptionCommentDOList.add(prescriptionCommentDO);
        return ResponseResult.okResult(prescriptionCommentDOList);
    }

    @Override
    public ResponseResult getPagePrescriptionComment(PrescriptionCommentDto dto) {
        //设置分页对象
        Page<PrescriptionCommentDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //构建分页查询
        LambdaQueryWrapper<PrescriptionCommentDO> prescriptionCommentDOLambdaQueryWrapper = Wrappers.lambdaQuery();
        prescriptionCommentDOLambdaQueryWrapper.eq(dto.getPatientName() != null, PrescriptionCommentDO::getPatientName, dto.getPatientName());
        IPage<PrescriptionCommentDO> prescriptionCommentDOIPage = prescriptionCommentMapper.selectPage(page, prescriptionCommentDOLambdaQueryWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), prescriptionCommentDOIPage.getTotal(), prescriptionCommentDOIPage.getRecords());
        //分页查询处方明细表
//        List<PrescriptionCommentDO> commentDOList = prescriptionCommentDOIPage.getRecords();
//        for (PrescriptionCommentDO commentDO : commentDOList) {
//            LambdaQueryWrapper<PrescriptionPrescriptionDetailDO> prescriptionDetailDOLambdaQueryWrapper = Wrappers.lambdaQuery();
//            prescriptionDetailDOLambdaQueryWrapper.eq(PrescriptionPrescriptionDetailDO::getId, commentDO.getPlatformId());
//            List<PrescriptionPrescriptionDetailDO> prescriptionPrescriptionDetailDOS = prescriptionPrescriptionDetailMapper.selectList(prescriptionDetailDOLambdaQueryWrapper);
//            commentDO.setPrescriptionDetailList(prescriptionPrescriptionDetailDOS);
//        }
        //返回给前端
        return ResponseResult.okResult(pageResponseResult);
    }

    @Override
    public ResponseResult getByIdPrescriptionComment(Long platformId) {
        //根据id来查询详情并获取对象
        PrescriptionCommentDO prescriptionCommentDO = prescriptionCommentMapper.selectById(platformId);
        //如果为空返回错误
        if (prescriptionCommentDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询处方明细表
        LambdaQueryWrapper<PrescriptionPrescriptionDetailDO> prescriptionPrescriptionDetailDOLambdaQueryWrapper = Wrappers.lambdaQuery();
        List<PrescriptionPrescriptionDetailDO> prescriptionPrescriptionDetailDOS = prescriptionPrescriptionDetailMapper.selectList(prescriptionPrescriptionDetailDOLambdaQueryWrapper);
        prescriptionCommentDO.setPrescriptionDetailList(prescriptionPrescriptionDetailDOS);
        //返回前端
        return ResponseResult.okResult(prescriptionCommentDO);
    }

    @Override
    public ResponseResult deletePrescriptionComment(Long platformId) {
        if (!SqlHelper.retBool(prescriptionCommentMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updatePrescriptionComment(PrescriptionCommentDO prescriptionCommentDO) {
        //如果参数为空返回错误
        if (prescriptionCommentDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //修改处方点评信息
        if (!SqlHelper.retBool(prescriptionCommentMapper.updateById(prescriptionCommentDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        //修改处方明细表
        //先根据id删除
        LambdaQueryWrapper<PrescriptionPrescriptionDetailDO> prescriptionPrescriptionDetailDOLambdaQueryWrapper = Wrappers.lambdaQuery();
        prescriptionPrescriptionDetailMapper.delete(prescriptionPrescriptionDetailDOLambdaQueryWrapper);
        //将处方明细表放到点评信息中
        List<PrescriptionPrescriptionDetailDO> prescriptionDetailList = prescriptionCommentDO.getPrescriptionDetailList();
        if (CollectionUtils.isNotEmpty(prescriptionDetailList)) {
            for (PrescriptionPrescriptionDetailDO prescriptionPrescriptionDetailDO : prescriptionDetailList) {
                prescriptionPrescriptionDetailMapper.insert(prescriptionPrescriptionDetailDO);
            }
        }
        return ResponseResult.okResult();
    }
}
