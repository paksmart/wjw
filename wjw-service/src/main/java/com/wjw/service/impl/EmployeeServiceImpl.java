package com.wjw.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.google.common.collect.Maps;
import com.wjw.center.admin.domain.Employee;
import com.wjw.common.admin.Constant;
import com.wjw.common.admin.EmployeeDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.common.utils.AppJwtUtil;
import com.wjw.mapper.EmployeeMapper;
import com.wjw.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.HashMap;

/**
* @description: psk
* @author pansh
* @date 2022/10/22 13:41
* @version 1.0
*/

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    /**
     * 盐值，混淆密码
     */
    private static final String SALT = "scss";

    private final EmployeeMapper employeeMapper;

    @Override
    public ResponseResult login(Employee employee) {
       //判断用户名和密码是否为空
        if ( StrUtil.isEmpty(employee.getUsername()) || StrUtil.isEmpty(employee.getPassword())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //根据用户名查询用户
        LambdaQueryWrapper<Employee> employeeLambdaQueryWrapper = Wrappers.lambdaQuery();
        employeeLambdaQueryWrapper.eq(Employee::getUsername, employee.getUsername());
        Employee one = employeeMapper.selectOne(employeeLambdaQueryWrapper);
        //不存在提示用户不存在
        if (one == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_USER);
        }
        //校验用户名
        if (!one.getUsername().equals(employee.getUsername())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_USER);
        }
        //密码进行md5加密
        String password = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes());
        //对比数据库中的密码,密码不一致,提示密码错误
        if (!password.equals(one.getPassword())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
        }
//        //校验是否管理员登录
//        if (employee.getStatus().equals(NumberEnum.ZERO.getValue())) {
//            return ResponseResult.errorResult(AppHttpCodeEnum.NO_OPERATOR_AUTH);
//        }
        //密码一致,使用jwt生成token
        HashMap<String, Object> userInfo = Maps.newHashMap();
        userInfo.put("employeeId", one.getId());
        String token = AppJwtUtil.getToken(userInfo);
        //给前端返回对应的格式数据
        Employee employees = new Employee();
        employees.setName(one.getName());
        HashMap<String, Object> map = Maps.newHashMap();
        map.put("employees", employees);
        map.put("token", token);
        return ResponseResult.okResult(map);
    }

    @Override
    public ResponseResult saveEmployee(Employee employee) {
            //如果参数为空,返回无效参数
            if (employee == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //设置密码 使用md5工具加密
            employee.setPassword(DigestUtils.md5DigestAsHex(Constant.INIT_PASSWORD.getBytes())); //默认密码123456
//        employee.setPassword(DigestUtils.md5DigestAsHex((SALT + employee.getPassword()).getBytes()));
            employee.setCreateUser(employee.getId());
            //如果新增数据失败
            if (!SqlHelper.retBool(employeeMapper.insert(employee))) {
                throw new TstRuntimeException("新增数据失败");
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPageEmployee(EmployeeDto dto) {
        //设置分页
        Page<Employee> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<Employee> employeeLambdaQueryWrapper = Wrappers.lambdaQuery();
        employeeLambdaQueryWrapper.eq(Employee::getIsDeleted, NumberEnum.ZERO.getValue());
        IPage<Employee> employeeIPage = employeeMapper.selectPage(page, employeeLambdaQueryWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), employeeIPage.getTotal(), employeeIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }

    @Override
    public ResponseResult getByIdEmployee(Long id) {
        //根据id来查询
        Employee employee = employeeMapper.selectById(id);
        //如果对象为空,返回无效对象
        if (employee == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(employee);
    }

    @Override
    public ResponseResult deleteByIdEmployee(Long id) {
        //如果删除失败,显示错误信息
        if (!SqlHelper.retBool(employeeMapper.deleteById(id))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateEmployee(Employee employee) {
        //如果参数为空返回无效参数
        if (employee == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        employee.setUpdateUser(employee.getId());
        //如果修改失败,显示错误信息
        if (!SqlHelper.retBool(employeeMapper.updateById(employee))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(employee);
    }
}
