package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.InquiryDO;
import com.wjw.center.admin.param.InquiryParam;
import com.wjw.common.admin.InquiryDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.InquiryMapper;
import com.wjw.service.InquiryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * @author pansh
 * @version 1.0
 * @description: psk
 * @date 2022/9/17 17:44
 */

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class InquiryServiceImpl implements InquiryService {

    private final InquiryMapper inquiryMapper;

    @Override
    public ResponseResult saveInquiry(InquiryDO inquiryDO) {
            //如果参数为空的话,返回无效参数
            if (inquiryDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //判断就诊人是否小于6岁
            if (inquiryDO.getPatientAge() < NumberEnum.SIX.getValue()) {
                if (StringUtils.isEmpty(inquiryDO.getGuardianCertNo())
                        && StringUtils.isEmpty(inquiryDO.getGuardianIdName())
                        && StringUtils.isEmpty(inquiryDO.getGuardianMobile())) {
                    return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"监护人信息不能为空");
                }
                inquiryDO.setGuardianIdName(inquiryDO.getGuardianIdName());
                inquiryDO.setGuardianCertNo(inquiryDO.getGuardianCertNo());
                inquiryDO.setGuardianMobile(inquiryDO.getGuardianMobile());
            }
            //如果咨询属性状态为3的话必填
            if (inquiryDO.getInquiryAttribute().equals(NumberEnum.THREE.getValue())) {
                if (inquiryDO.getInquiryAttribute() == null) {
                    return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "咨询属性为医护咨询,咨询护理分类不能为空");
                }
                inquiryDO.setInquiryCategory(inquiryDO.getInquiryCategory());
            }
            //咨询价格保留两位小数
            inquiryDO.setInquiryPrice(Double.valueOf(inquiryDO.getInquiryPrice()));
            inquiryDO.setInquiryId(IdUtil.getSnowflake(1, 1).nextId());
            inquiryDO.setDeptId(IdUtil.getSnowflake(1, 1).nextId());
            inquiryDO.setStaffId(IdUtil.getSnowflake(1, 1).nextId());
            inquiryDO.setRecordsMark(IdUtil.getSnowflake(1,1).nextId());
            inquiryDO.setCreateUser(inquiryDO.getInquiryId());
            //判断数据是否添加成功
            if (!SqlHelper.retBool(inquiryMapper.insert(inquiryDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        //返回给前端
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getInquiry(InquiryParam param) {
        //判断参数是否为空,如果为空的话返回参数无效
        if (param == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<InquiryDO> inquiryDOWrapper = Wrappers.lambdaQuery();
        //判断对象是否为空来查询
        if (StringUtils.isNotEmpty(param.getPlatformId())
                && StringUtils.isNotEmpty(param.getInquiryId())
                && StringUtils.isNotEmpty(param.getSubjectCode())) {
            inquiryDOWrapper.eq(InquiryDO::getPlatformId, param.getPlatformId())
                    .eq(InquiryDO::getInquiryId, param.getInquiryId())
                    .eq(InquiryDO::getSubjectCode, param.getSubjectCode());
        }
        List<InquiryDO> inquiryDOList = inquiryMapper.selectList(inquiryDOWrapper);
        //把信息列表返回给前端
        return ResponseResult.okResult(inquiryDOList);
    }

    @Override
    public ResponseResult getByIdInquiry(Long platformId) {
        //根据id来查询出对象
        InquiryDO inquiryDO = inquiryMapper.selectById(platformId);
        //判断参数是否为空
        if (inquiryDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(inquiryDO);
    }

    @Override
    public ResponseResult deleteInquiry(Long platformId) {
        //如果删除失败,显示错误信息
        if (!SqlHelper.retBool(inquiryMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateInquiry(InquiryDO inquiryDO) {
        //判断参数是否为空
        if (inquiryDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        inquiryDO.setUpdateTime(new Date());
        inquiryDO.setUpdateUser(1L);
        //判断数据是否修改
        if (!SqlHelper.retBool(inquiryMapper.updateById(inquiryDO))) {
            throw new TstRuntimeException("修改失败");
        }
        return ResponseResult.okResult(inquiryDO);
    }

    @Override
    public ResponseResult getPageInquiry(InquiryDto dto) {
        //设置分页对象
        Page<InquiryDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<InquiryDO> inquiryWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            inquiryWrapper.like(InquiryDO::getPatientName, dto.getPatientName());
        }
        IPage<InquiryDO> inquiryDOIPage = inquiryMapper.selectPage(page, inquiryWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), inquiryDOIPage.getTotal(), inquiryDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
