package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.PathologyDiagnosisAppliedListDO;
import com.wjw.center.admin.domain.PathologyDiagnosisDO;
import com.wjw.common.admin.PathologyDiagnosisDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.PathologyDiagnosisAppliedListMapper;
import com.wjw.mapper.PathologyDiagnosisMapper;
import com.wjw.service.PathologyDiagnosisService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/29 15:44
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
public class PathologyDiagnosisServiceImpl implements PathologyDiagnosisService {

    private final PathologyDiagnosisMapper pathologyDiagnosisMapper;
    private final PathologyDiagnosisAppliedListMapper pathologyDiagnosisAppliedListMapper;

    @Override
    public ResponseResult savePathologyDiagnosis(PathologyDiagnosisDO pathologyDiagnosisDO) {
        //如果为空返回错误
        if (pathologyDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //支付价格保留两位小数
        pathologyDiagnosisDO.setPathologyPrice(Double.valueOf(pathologyDiagnosisDO.getPathologyPrice()));

        pathologyDiagnosisDO.setPathologyDiagnosisId(IdUtil.getSnowflake(1, 2).nextId());
        pathologyDiagnosisDO.setPatientId(IdUtil.getSnowflake(1,2).nextId());
        pathologyDiagnosisDO.setApplyDocId(IdUtil.getSnowflake(1, 2).nextId());
        //新增互联网医院远程病理诊断记录监管
        //如果新增数据失败显示错误信息
        if (!SqlHelper.retBool(pathologyDiagnosisMapper.insert(pathologyDiagnosisDO))) {
            throw new TstRuntimeException("新增数据失败");
        }

        //新增互联网医院远程病理诊断记录监管(接受申请信息列表)
        //先获取申请信息列表
        List<PathologyDiagnosisAppliedListDO> appliedList = pathologyDiagnosisDO.getAppliedList();
        if (CollectionUtils.isNotEmpty(appliedList)) {
            for (PathologyDiagnosisAppliedListDO pathologyDiagnosisAppliedListDO : appliedList) {
                pathologyDiagnosisAppliedListDO.setId(pathologyDiagnosisDO.getPathologyDiagnosisId());
                if (!SqlHelper.retBool(pathologyDiagnosisAppliedListMapper.insert(pathologyDiagnosisAppliedListDO))) {
                    throw new TstRuntimeException("新增数据失败");
                }
            }
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPathologyDiagnosis(PathologyDiagnosisDO pathologyDiagnosisDO) {
        //如果为空返回错误
        if (pathologyDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        //查询互联网医院远程病理诊断记录监管
        LambdaQueryWrapper<PathologyDiagnosisDO> pathologyDiagnosisDWrapper = Wrappers.lambdaQuery();
        pathologyDiagnosisDWrapper.eq(PathologyDiagnosisDO::getPlatformId, pathologyDiagnosisDO.getPlatformId());
        pathologyDiagnosisDWrapper.eq(PathologyDiagnosisDO::getPathologyDiagnosisId, pathologyDiagnosisDO.getPathologyDiagnosisId());
        List<PathologyDiagnosisDO> pathologyDiagnosisDOList = pathologyDiagnosisMapper.selectList(pathologyDiagnosisDWrapper);
        //查询互联网医院远程病理诊断记录监管(接受申请信息列表)
        LambdaQueryWrapper<PathologyDiagnosisAppliedListDO> pathologyDiagnosisAppliedListWrapper = Wrappers.lambdaQuery();
        pathologyDiagnosisAppliedListWrapper.eq(PathologyDiagnosisAppliedListDO::getAppliedDocId, pathologyDiagnosisDO.getApplyDocId());
        List<PathologyDiagnosisAppliedListDO> pathologyDiagnosisAppliedListDOS = pathologyDiagnosisAppliedListMapper.selectList(pathologyDiagnosisAppliedListWrapper);
        pathologyDiagnosisDO.setAppliedList(pathologyDiagnosisAppliedListDOS);
        //把申请信息列表添加到主表中
        pathologyDiagnosisDOList.add(pathologyDiagnosisDO);
        //返回给前端
        return ResponseResult.okResult(pathologyDiagnosisDOList);
    }

    @Override
    public ResponseResult getByIdPathologyDiagnosis(Long platformId) {
        //根据id来查询
        PathologyDiagnosisDO pathologyDiagnosisDO = pathologyDiagnosisMapper.selectById(platformId);
        //如果为空,返回错误
        if (pathologyDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //根据id查询互联网医院远程病理诊断记录监管(接受申请信息列表)
        LambdaQueryWrapper<PathologyDiagnosisAppliedListDO> pathologyDiagnosisAppliedListWrapper = Wrappers.lambdaQuery();
        pathologyDiagnosisAppliedListWrapper.eq(PathologyDiagnosisAppliedListDO::getAppliedDocId, pathologyDiagnosisDO.getApplyDocId());
        List<PathologyDiagnosisAppliedListDO> pathologyDiagnosisAppliedListDOList = pathologyDiagnosisAppliedListMapper.selectList(pathologyDiagnosisAppliedListWrapper);
        pathologyDiagnosisDO.setAppliedList(pathologyDiagnosisAppliedListDOList);
        //返回给前端
        return ResponseResult.okResult(pathologyDiagnosisDO);
    }

    @Override
    public ResponseResult deletePathologyDiagnosis(Long platformId) {
        //根据id来删除信息
        if (!SqlHelper.retBool(pathologyDiagnosisMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updatePathologyDiagnosis(PathologyDiagnosisDO pathologyDiagnosisDO) {
        //如果对象为空返回错误
        if (pathologyDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //修改互联网医院远程病理诊断记录监管
        pathologyDiagnosisMapper.updateById(pathologyDiagnosisDO);
        //修改互联网医院远程病理诊断记录监管(接受申请信息列表)
        //先删除原信息
        LambdaQueryWrapper<PathologyDiagnosisAppliedListDO> pathologyDiagnosisAppliedListWrapper = Wrappers.lambdaQuery();
        pathologyDiagnosisAppliedListWrapper.eq(PathologyDiagnosisAppliedListDO::getAppliedDocId, pathologyDiagnosisDO.getApplyDocId());
        pathologyDiagnosisAppliedListMapper.delete(pathologyDiagnosisAppliedListWrapper);
        //新增修改的数据
        //获取申请信息列表
        List<PathologyDiagnosisAppliedListDO> appliedList = pathologyDiagnosisDO.getAppliedList();
        if (CollectionUtils.isNotEmpty(appliedList)) {
            for (PathologyDiagnosisAppliedListDO pathologyDiagnosisAppliedListDO : appliedList) {
                pathologyDiagnosisAppliedListDO.setAppliedDocId(pathologyDiagnosisDO.getApplyDocId());
                pathologyDiagnosisAppliedListMapper.insert(pathologyDiagnosisAppliedListDO);
            }
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPagePathologyDiagnosis(PathologyDiagnosisDto dto) {
        //设置分页对象
        Page<PathologyDiagnosisDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页条件查询
        LambdaQueryWrapper<PathologyDiagnosisDO> pathologyDiagnosisWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            pathologyDiagnosisWrapper.like(PathologyDiagnosisDO::getPatientName, dto.getPatientName());
        }
        IPage<PathologyDiagnosisDO> pathologyDiagnosisDOIPage = pathologyDiagnosisMapper.selectPage(page, pathologyDiagnosisWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), pathologyDiagnosisDOIPage.getTotal(), pathologyDiagnosisDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
