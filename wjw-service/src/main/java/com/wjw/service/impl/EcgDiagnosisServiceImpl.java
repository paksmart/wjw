package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.EcgDiagnosisAppliedListDO;
import com.wjw.center.admin.domain.EcgDiagnosisDO;
import com.wjw.common.admin.EcgDiagnosisDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.EcgDiagnosisAppliedListMapper;
import com.wjw.mapper.EcgDiagnosisMapper;
import com.wjw.service.EcgDiagnosisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @description:
* @author pansh
* @date 2022/9/27 14:11
* @version 1.0
 *
*/

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Transactional
public class EcgDiagnosisServiceImpl implements EcgDiagnosisService {

    private final EcgDiagnosisMapper ecgDiagnosisMapper;
    private final EcgDiagnosisAppliedListMapper ecgDiagnosisAppliedListMapper;

    @Override
    public ResponseResult saveEcgDiagnosis(EcgDiagnosisDO ecgDiagnosisDO) {
            //如果为空返回错误
            if (ecgDiagnosisDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //支付价格保留两位小数
            ecgDiagnosisDO.setEcgPrice(Double.valueOf(ecgDiagnosisDO.getEcgPrice()));

            ecgDiagnosisDO.setPatientId(IdUtil.getSnowflake(1, 1).nextId());
            ecgDiagnosisDO.setEcgDiagnosisId(IdUtil.getSnowflake(1, 1).nextId());
            ecgDiagnosisDO.setApplyDeptId(IdUtil.getSnowflake(1, 1).nextId());
            ecgDiagnosisDO.setApplyDocId(IdUtil.getSnowflake(1, 1).nextId());
            //新增互联网医院远程心电图诊断记录监管
            if (!SqlHelper.retBool(ecgDiagnosisMapper.insert(ecgDiagnosisDO))) {
                throw new TstRuntimeException("新增数据失败");
            }

            //新增互联网医院远程心电图诊断记录监管接受申请信息列表
            List<EcgDiagnosisAppliedListDO> appliedList = ecgDiagnosisDO.getAppliedList();
            if (CollectionUtils.isNotEmpty(appliedList)) {
                for (EcgDiagnosisAppliedListDO ecgDiagnosisAppliedListDO : appliedList) {
                    ecgDiagnosisAppliedListDO.setId(ecgDiagnosisDO.getEcgDiagnosisId());
                    //如果新增数据失败显示数据
                    if (!SqlHelper.retBool(ecgDiagnosisAppliedListMapper.insert(ecgDiagnosisAppliedListDO))) {
                        throw new TstRuntimeException("新增数据失败");
                    }
                }
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getEcgDiagnosis(EcgDiagnosisDO ecgDiagnosisDO) {
        //如果为空返回错误
        if (ecgDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<EcgDiagnosisDO> ecgDiagnosisWrapper = Wrappers.lambdaQuery();
        ecgDiagnosisWrapper.eq(EcgDiagnosisDO::getPlatformId, ecgDiagnosisDO.getPlatformId());
        ecgDiagnosisWrapper.eq(EcgDiagnosisDO::getEcgDiagnosisId, ecgDiagnosisDO.getEcgDiagnosisId());
        //根据条件查询所有
        List<EcgDiagnosisDO> ecgDiagnosisDOList = ecgDiagnosisMapper.selectList(ecgDiagnosisWrapper);

        //根据主表的主键和副表的主键来查询
        LambdaQueryWrapper<EcgDiagnosisAppliedListDO> ecgDiagnosisAppliedListWrapper = Wrappers.lambdaQuery();
        ecgDiagnosisAppliedListWrapper.eq(EcgDiagnosisAppliedListDO::getId, ecgDiagnosisDO.getEcgDiagnosisId());
        List<EcgDiagnosisAppliedListDO> ecgDiagnosisAppliedListDOList = ecgDiagnosisAppliedListMapper.selectList(ecgDiagnosisAppliedListWrapper);
        ecgDiagnosisDO.setAppliedList(ecgDiagnosisAppliedListDOList);
        //把申请信息列表添加到主表中
        ecgDiagnosisDOList.add(ecgDiagnosisDO);
        return ResponseResult.okResult(ecgDiagnosisDOList);
    }

    @Override
    public ResponseResult getByIdEcgDiagnosis(Long platformId) {
        //根据id来查询列表
        EcgDiagnosisDO ecgDiagnosisDO = ecgDiagnosisMapper.selectById(platformId);
        //如果为空返回参数错误
        if (ecgDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //返回给前端
        return ResponseResult.okResult(ecgDiagnosisDO);
    }

    @Override
    public ResponseResult deleteEcgDiagnosis(Long platformId) {
        if (!SqlHelper.retBool(ecgDiagnosisMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateEcgDiagnosis(EcgDiagnosisDO ecgDiagnosisDO) {
        //如果对象为空返回错误信息
        if (ecgDiagnosisDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //修改互联网医院远程心电图诊断记录监管
        if (!SqlHelper.retBool(ecgDiagnosisMapper.updateById(ecgDiagnosisDO))) {
            throw new TstRuntimeException("修改数据失败");
        }

        //修改互联网医院远程心电图诊断记录监管接受申请信息列表
        //先删除接收申请信息列表
        LambdaQueryWrapper<EcgDiagnosisAppliedListDO> ecgDiagnosisAppliedListWrapper  = Wrappers.lambdaQuery();
        ecgDiagnosisAppliedListWrapper.eq(EcgDiagnosisAppliedListDO::getId, ecgDiagnosisDO.getEcgDiagnosisId());
        ecgDiagnosisAppliedListMapper.delete(ecgDiagnosisAppliedListWrapper);

        //将更新的数据存入申请信息列表中
        List<EcgDiagnosisAppliedListDO> appliedList = ecgDiagnosisDO.getAppliedList();
        if (CollectionUtils.isNotEmpty(appliedList)) {
            for (EcgDiagnosisAppliedListDO ecgDiagnosisAppliedListDO : appliedList) {
                ecgDiagnosisAppliedListDO.setId(ecgDiagnosisDO.getEcgDiagnosisId());
                ecgDiagnosisAppliedListMapper.insert(ecgDiagnosisAppliedListDO);
            }
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPageEcgDiagnosis(EcgDiagnosisDto dto) {
        //设置分页对象
        Page<EcgDiagnosisDO> page = new Page<>(dto.getPage(),dto.getPageSize());

        //分页条件查询
        LambdaQueryWrapper<EcgDiagnosisDO> ecgDiagnosisWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            ecgDiagnosisWrapper.like(EcgDiagnosisDO::getPatientName, dto.getPatientName());
        }
        IPage<EcgDiagnosisDO> ecgDiagnosisDOIPage = ecgDiagnosisMapper.selectPage(page, ecgDiagnosisWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), ecgDiagnosisDOIPage.getTotal(), ecgDiagnosisDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }
}
