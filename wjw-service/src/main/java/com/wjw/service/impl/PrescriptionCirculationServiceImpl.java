package com.wjw.service.impl;


import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.PrescriptionCirculationDO;
import com.wjw.center.admin.param.PrescriptionCirculationParam;
import com.wjw.center.admin.vo.PrescriptionCirculationVO;
import com.wjw.common.admin.PrescriptionCirculationDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.PrescriptionCirculationMapper;
import com.wjw.service.PrescriptionCirculationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author pansh
 * @version 1.0
 * @description: psk
 * @date 2022/9/19 18:43
 */

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class PrescriptionCirculationServiceImpl implements PrescriptionCirculationService {

    private final PrescriptionCirculationMapper prescriptionCirculationMapper;

    @Override
    public ResponseResult savePrescriptionCirculation(PrescriptionCirculationDO prescriptionCirculationDO) {
            //判断参数是否为空
            if (prescriptionCirculationDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //如果配饰方式是医院药房快递配送或药企物流配送需要传入
            if (prescriptionCirculationDO.getDeliveryType().equals(NumberEnum.TWO.getValue()) || prescriptionCirculationDO.getDeliveryType().equals(NumberEnum.THREE.getValue())) {
                prescriptionCirculationDO.setDeliveryPeople(prescriptionCirculationDO.getDeliveryPeople());
                prescriptionCirculationDO.setDeliveryStartDate(prescriptionCirculationDO.getDeliveryStartDate());
                prescriptionCirculationDO.setDeliveryEndDate(prescriptionCirculationDO.getDeliveryEndDate());
                prescriptionCirculationDO.setDeliveryFee(prescriptionCirculationDO.getDeliveryFee());
            }
            //如果配送方式不为医院取药或药店自取配送时间生效
            if (!prescriptionCirculationDO.getDeliveryType().equals(NumberEnum.ONE.getValue()) || !prescriptionCirculationDO.getDeliveryType().equals(NumberEnum.FIVE.getValue())) {
                prescriptionCirculationDO.getDeliveryTime();
            }
            //处方总金额保留两位小数
            prescriptionCirculationDO.setTotalFee(prescriptionCirculationDO.getTotalFee());
            prescriptionCirculationDO.setSubsequentVisitId(IdUtil.getSnowflake(1, 1).nextId());
            prescriptionCirculationDO.setPrescriptionNo(IdUtil.getSnowflake(1, 1).nextId());
            prescriptionCirculationDO.setCreateUser(prescriptionCirculationDO.getPlatformId());
            //把数据设置到对象中
            if (!SqlHelper.retBool(prescriptionCirculationMapper.insert(prescriptionCirculationDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPrescriptionCirculation(PrescriptionCirculationDO param) {
        if (param == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        LambdaQueryWrapper<PrescriptionCirculationDO> prescriptionCirculationDOWrapper = Wrappers.lambdaQuery();
            prescriptionCirculationDOWrapper.eq(PrescriptionCirculationDO::getPlatformId, param.getPlatformId())
                    .eq(PrescriptionCirculationDO::getPrescriptionNo, param.getPrescriptionNo());
        List<PrescriptionCirculationDO> prescriptionCirculationDOList = prescriptionCirculationMapper.selectList(prescriptionCirculationDOWrapper);
        return ResponseResult.okResult(prescriptionCirculationDOList);
    }

    @Override
    public ResponseResult getByIdPrescriptionCirculation(Long platformId) {
        //根据id来查询对象
        PrescriptionCirculationDO prescriptionCirculationDO = prescriptionCirculationMapper.selectById(platformId);
        //判断对象是否为空,为空返回错误
        if (prescriptionCirculationDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        PrescriptionCirculationVO prescriptionCirculationVO = getPrescriptionCirculationVO(prescriptionCirculationDO);
        //返回给前端
        return ResponseResult.okResult(prescriptionCirculationVO);
    }

    @Override
    public ResponseResult deletePrescriptionCirculation(Long platformId) {
        if (platformId == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "ID为空");
        }
        //删除数据失败显示删除失败
        if (!SqlHelper.retBool(prescriptionCirculationMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        //给前端返回
        return ResponseResult.okResult("删除成功");
    }

    @Override
    public ResponseResult updatePrescriptionCirculation(PrescriptionCirculationDO prescriptionCirculationDO) {
        //判断对象是否为空,为空返回错误
        if (prescriptionCirculationDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        prescriptionCirculationDO.setUpdateTime(new Date());
        prescriptionCirculationDO.setUpdateUser(1L);
        //判断数据是否修改成功
        if (!SqlHelper.retBool(prescriptionCirculationMapper.updateById(prescriptionCirculationDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        //给前端返回数据
        return ResponseResult.okResult(prescriptionCirculationDO);
    }

    @Override
    public ResponseResult getPagePrescriptionCirculation(PrescriptionCirculationDto dto) {
        //设置分页对象
        Page<PrescriptionCirculationDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页查询条件
        LambdaQueryWrapper<PrescriptionCirculationDO> prescriptionCirculationWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPrescriptionNo())) {
            prescriptionCirculationWrapper.like(PrescriptionCirculationDO::getPrescriptionNo, dto.getPrescriptionNo());
        }
        IPage<PrescriptionCirculationDO> prescriptionCirculationDOIPage = prescriptionCirculationMapper.selectPage(page, prescriptionCirculationWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), prescriptionCirculationDOIPage.getTotal(), prescriptionCirculationDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }

    private PrescriptionCirculationVO getPrescriptionCirculationVO(PrescriptionCirculationDO prescriptionCirculationDO) {
        //把对象放到基本信息中
        PrescriptionCirculationVO prescriptionCirculationVO = new PrescriptionCirculationVO();
        prescriptionCirculationVO.setPlatformId(prescriptionCirculationDO.getPlatformId());
        prescriptionCirculationVO.setSubsequentVisitId(prescriptionCirculationDO.getSubsequentVisitId());
        prescriptionCirculationVO.setPrescriptionNo(prescriptionCirculationDO.getPrescriptionNo());
        prescriptionCirculationVO.setDeliveryType(prescriptionCirculationDO.getDeliveryType());
        prescriptionCirculationVO.setDeliveryTime(prescriptionCirculationDO.getDeliveryTime());
        prescriptionCirculationVO.setDeliveryPeople(prescriptionCirculationDO.getDeliveryPeople());
        prescriptionCirculationVO.setDeliveryStartDate(prescriptionCirculationDO.getDeliveryStartDate());
        prescriptionCirculationVO.setDeliveryEndDate(prescriptionCirculationDO.getDeliveryEndDate());
        prescriptionCirculationVO.setDeliveryFee(prescriptionCirculationDO.getDeliveryFee());
        prescriptionCirculationVO.setTotalFee(prescriptionCirculationDO.getTotalFee());
        prescriptionCirculationVO.setPayMark(prescriptionCirculationDO.getPayMark());
        prescriptionCirculationVO.setTradeNo(prescriptionCirculationDO.getTradeNo());
        prescriptionCirculationVO.setCreateTime(new Date());
        return prescriptionCirculationVO;
    }
}
