package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.SubsequentVisitDO;
import com.wjw.center.admin.param.SubsequentVisitParam;
import com.wjw.common.admin.SubsequentVisitDto;
import com.wjw.common.dto.PageRequestDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.enums.NumberEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.SubsequentVisitMapper;
import com.wjw.service.SubsequentVisitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author pansh
 * @version 1.0
 * @description: pak
 * @date 2022/9/19 11:11
 */

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
@Transactional
public class SubsequentVisitServiceImpl implements SubsequentVisitService {

    private final SubsequentVisitMapper subsequentVisitMapper;

    @Override
    public ResponseResult getSubsequentVisit(SubsequentVisitParam param) {
        //判断参数是否为空
        if (param == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构建查询条件
        LambdaQueryWrapper<SubsequentVisitDO> subsequentVisitDOWrapper  = Wrappers.lambdaQuery();
        //判断参数不能为空
        if (StringUtils.isNotEmpty(param.getPlatformId()) && StringUtils.isNotEmpty(param.getSubsequentVisitId())) {
            subsequentVisitDOWrapper.eq(SubsequentVisitDO::getPlatformId, param.getPlatformId())
                    .eq(SubsequentVisitDO::getSubsequentVisitId, param.getSubsequentVisitId());
        }
        //根据构造条件查询列表
        List<SubsequentVisitDO> subsequentVisitDOList = subsequentVisitMapper.selectList(subsequentVisitDOWrapper);
        return ResponseResult.okResult(subsequentVisitDOList);
    }

    @Override
    public ResponseResult getByIdSubsequentVisit(Long platformId) {
        //根据id来查询列表
        SubsequentVisitDO subsequentVisitDO = subsequentVisitMapper.selectById(platformId);
        //判断参数是否为空
        if (subsequentVisitDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return ResponseResult.okResult(subsequentVisitDO);
    }

    @Override
    public ResponseResult deleteSubsequentVisit(Long platformId) {
        //如果删除失败,显示错误信息
        if (!SqlHelper.retBool(subsequentVisitMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateSubsequentVisit(SubsequentVisitDO subsequentVisitDO) {
        if (subsequentVisitDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        subsequentVisitDO.setUpdateUser(subsequentVisitDO.getSubsequentVisitId());
        if (!SqlHelper.retBool(subsequentVisitMapper.updateById(subsequentVisitDO))) {
            throw new TstRuntimeException("修改数据失败");
        }
        return ResponseResult.okResult(subsequentVisitDO);
    }

    @Override
    public ResponseResult getPageSubsequentVisit(SubsequentVisitDto dto) {
        //设置分页查询
        Page<SubsequentVisitDO> page = new Page<>(dto.getPage(), dto.getPageSize());
        //分页查询条件
        LambdaQueryWrapper<SubsequentVisitDO> subsequentVisitWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getPatientName())) {
            subsequentVisitWrapper.eq(SubsequentVisitDO::getPatientName, dto.getPatientName());
        }
        IPage<SubsequentVisitDO> subsequentVisitDOIPage = subsequentVisitMapper.selectPage(page, subsequentVisitWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), subsequentVisitDOIPage.getTotal(), subsequentVisitDOIPage.getRecords());
        return ResponseResult.okResult(pageResponseResult);
    }

    @Override
    public ResponseResult saveSubsequentVisit(SubsequentVisitDO subsequentVisitDO) {
            //判断参数是否为空
            if (subsequentVisitDO == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            if (subsequentVisitDO.getPatientAge() < NumberEnum.SIX.getValue()) {
                subsequentVisitDO.setGuardianIdName(subsequentVisitDO.getGuardianIdName());
                subsequentVisitDO.setGuardianCertNo(subsequentVisitDO.getGuardianCertNo());
                subsequentVisitDO.setGuardianMobile(subsequentVisitDO.getGuardianMobile());
            }
            //不整费用保留两位,四舍五入
            subsequentVisitDO.setTotalFee(Double.valueOf(subsequentVisitDO.getTotalFee()));
            subsequentVisitDO.setSubsequentVisitId(IdUtil.getSnowflake(1,1).nextId());
            subsequentVisitDO.setProveMark(IdUtil.getSnowflake(1,1).nextId());
            subsequentVisitDO.setRecordsMark(IdUtil.getSnowflake(1,1).nextId());
            subsequentVisitDO.setDeptId(IdUtil.getSnowflake(1,1).nextId());
            //新增数据
            //判断数据是否添加成功
            if (!SqlHelper.retBool(subsequentVisitMapper.insert(subsequentVisitDO))) {
                throw new TstRuntimeException("新增数据失败");
            }
        //返回给前端
        return ResponseResult.okResult();
    }
}
