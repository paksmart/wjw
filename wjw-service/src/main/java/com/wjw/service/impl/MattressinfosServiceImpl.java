package com.wjw.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.domain.MattressinfosDO;
import com.wjw.center.admin.param.MattressinfosParam;
import com.wjw.center.admin.vo.MattressinfosVO;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.service.MattressinfosService;
import com.wjw.mapper.MattressinfosMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 床垫的实时数据服务接口实现
 *
 * @author psk
 * @since 2022-09-14 15:19:57
 * @description 由 psk 创建
 */
@Slf4j
@RequiredArgsConstructor(onConstructor_ =@Autowired)
@Service
@Transactional
public class MattressinfosServiceImpl implements MattressinfosService {

    private final MattressinfosMapper tstMattressinfosMapper;

    @Override
    public ResponseResult saveMattressInfos(MattressinfosParam param) {
        //校验参数
        if (param == null || param.getDevId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        MattressinfosDO mattressinfosDO = new MattressinfosDO();
        mattressinfosDO.setDeviceModelId(param.getDeviceModelId());
        mattressinfosDO.setDevId(param.getDevId());
        mattressinfosDO.setApiKey(param.getApiKey());
        mattressinfosDO.setCode(param.getCode());
        mattressinfosDO.setMessage(param.getMessage());
        mattressinfosDO.setDeviceState(param.getDeviceState());
        mattressinfosDO.setUserState(param.getUserState());
        mattressinfosDO.setHeartRate(param.getHeartRate());
        mattressinfosDO.setRespiratoryRate(param.getRespiratoryRate());
        mattressinfosDO.setCreateTime(new Date());
        tstMattressinfosMapper.insert(mattressinfosDO);
        return ResponseResult.okResult(mattressinfosDO);
    }

    @Override
    public ResponseResult GetMattressInfos(MattressinfosParam param) {
        LambdaQueryWrapper<MattressinfosDO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MattressinfosDO::getDeviceModelId, param.getDeviceModelId());
        if (param.getDevId() != null) {
            wrapper.eq(MattressinfosDO::getDevId, param.getDevId());
        }
        List<MattressinfosDO> mattressinfosDOList = tstMattressinfosMapper.selectList(wrapper);
        return ResponseResult.okResult(mattressinfosDOList);
    }

    @Override
    public ResponseResult getById(Long deviceModelId) {
        MattressinfosDO mattressinfosDO = tstMattressinfosMapper.selectById(deviceModelId);
        if (mattressinfosDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        MattressinfosVO mattressinfosVO = new MattressinfosVO();
        mattressinfosVO.setDeviceModelId(mattressinfosDO.getDeviceModelId());
        mattressinfosVO.setDevId(mattressinfosDO.getDevId());
        mattressinfosVO.setApiKey(mattressinfosDO.getApiKey());
        mattressinfosVO.setCode(mattressinfosDO.getCode());
        mattressinfosVO.setMessage(mattressinfosDO.getMessage());
        mattressinfosVO.setDeviceState(mattressinfosDO.getDeviceState());
        mattressinfosVO.setUserState(mattressinfosDO.getUserState());
        mattressinfosVO.setHeartRate(mattressinfosDO.getHeartRate());
        mattressinfosVO.setRespiratoryRate(mattressinfosDO.getRespiratoryRate());
        return ResponseResult.okResult(mattressinfosVO);
    }

    @Override
    public ResponseResult delete(Long deviceModelId) {
        MattressinfosDO mattressinfosDO = tstMattressinfosMapper.selectById(deviceModelId);
        if (mattressinfosDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        if (mattressinfosDO.getIsDeleted().equals(1)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        mattressinfosDO.setIsDeleted(1);
        if (!SqlHelper.retBool(tstMattressinfosMapper.updateById(mattressinfosDO))) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_OPERATOR_AUTH);
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult update(MattressinfosDO mattressinfosDO) {
        mattressinfosDO.setUpdateUser(1L);
        mattressinfosDO.setUpdateTime(new Date());
        tstMattressinfosMapper.selectById(mattressinfosDO);
        return ResponseResult.okResult(mattressinfosDO);
    }


//    @Override
//    public ResponseResult getLogPath(Long deviceModelId) {
//return null;
//    }
}
