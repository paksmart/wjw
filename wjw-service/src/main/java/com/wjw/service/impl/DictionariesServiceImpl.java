package com.wjw.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wjw.center.admin.dictionaries.*;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.utils.AddressResolutionUtil;
import com.wjw.mapper.*;
import com.wjw.service.DictionariesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @description: 实现类
* @author psk
* @date 2022/10/17 9:16
* @version 1.0
*/

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Transactional
public class DictionariesServiceImpl implements DictionariesService {

    private final DictionariesSubjectMapper dictionariesSubjectMapper;
    private final DictionariesNationMapper dictionariesNationMapper;
    private final DictionariesDoctorTitleMapper dictionariesDoctorTitleMapper;
    private final DictionariesCertTypeMapper dictionariesCertTypeMapper;
    private final DictionariesBusinessMapper dictionariesBusinessMapper;
    private final DictionariesSexMapper dictionariesSexMapper;
    private final DictionariesAreaMapper dictionariesAreaMapper;
    private final DictionariesIcdMapper dictionariesIcdMapper;

    @Override
    public ResponseResult dictionaries(DictionariesNation dictionariesNation) {
        //查询民族字典
        LambdaQueryWrapper<DictionariesNation> dictionariesNationWrapper = Wrappers.lambdaQuery();
        dictionariesNationWrapper.eq(DictionariesNation::getCode, dictionariesNation.getCode());
        List<DictionariesNation> dictionariesNationList = dictionariesNationMapper.selectList(dictionariesNationWrapper);
        return ResponseResult.okResult(dictionariesNationList);
    }

    @Override
    public ResponseResult dictionariesSubjectCode(DictionariesSubject dictionariesSubject) {
        //查询科目代码
        LambdaQueryWrapper<DictionariesSubject> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(DictionariesSubject::getCode, dictionariesSubject.getCode());
        List<DictionariesSubject> dictionariesSubjectList = dictionariesSubjectMapper.selectList(wrapper);
        //返回给前端
        return ResponseResult.okResult(dictionariesSubjectList);
    }

    @Override
    public ResponseResult dictionariesTitle(DictionariesDoctorTitle dictionariesDoctorTitle) {
        //查询医疗人员
        LambdaQueryWrapper<DictionariesDoctorTitle> dictionariesDoctorTitleLambdaQueryWrapper = Wrappers.lambdaQuery();
        dictionariesDoctorTitleLambdaQueryWrapper.eq(DictionariesDoctorTitle::getCode, dictionariesDoctorTitle.getCode());
        List<DictionariesDoctorTitle> dictionariesDoctorTitleList = dictionariesDoctorTitleMapper.selectList(dictionariesDoctorTitleLambdaQueryWrapper);
        return ResponseResult.okResult(dictionariesDoctorTitleList);
    }

    @Override
    public ResponseResult dictionariesCertType(DictionariesCertType dictionariesCertType) {
        //查询证件类型字典
        LambdaQueryWrapper<DictionariesCertType> dictionariesCertTypeLambdaQueryWrapper = Wrappers.lambdaQuery();
        dictionariesCertTypeLambdaQueryWrapper.eq(DictionariesCertType::getCode, dictionariesCertType.getCode());
        List<DictionariesCertType> dictionariesCertTypeList = dictionariesCertTypeMapper.selectList(dictionariesCertTypeLambdaQueryWrapper);
        return ResponseResult.okResult(dictionariesCertTypeList);
    }

    @Override
    public ResponseResult dictionariesBusiness(DictionariesBusiness dictionariesBusiness) {
        //业务类型字典
        LambdaQueryWrapper<DictionariesBusiness> dictionariesBusinessLambdaQueryWrapper = Wrappers.lambdaQuery();
        dictionariesBusinessLambdaQueryWrapper.eq(DictionariesBusiness::getCode, dictionariesBusiness.getCode());
        List<DictionariesBusiness> dictionariesBusinessList = dictionariesBusinessMapper.selectList(dictionariesBusinessLambdaQueryWrapper);
        return ResponseResult.okResult(dictionariesBusinessList);
    }

    @Override
    public ResponseResult dictionariesSex(DictionariesSex dictionariesSex) {
        //查询性别字典
        LambdaQueryWrapper<DictionariesSex> dictionariesSexLambdaQueryWrapper = Wrappers.lambdaQuery();
        dictionariesSexLambdaQueryWrapper.eq(DictionariesSex::getCode, dictionariesSex.getCode());
        List<DictionariesSex> dictionariesSexList = dictionariesSexMapper.selectList(dictionariesSexLambdaQueryWrapper);
        return ResponseResult.okResult(dictionariesSexList);
    }

    @Override
    public ResponseResult dictionariesArea(DictionariesArea dictionariesArea) {
        //地区字典
        LambdaQueryWrapper<DictionariesArea> dictionariesAreaWrapper = Wrappers.lambdaQuery();
        dictionariesAreaWrapper
                .eq(DictionariesArea::getCode, dictionariesArea.getCode())
                .eq(DictionariesArea::getName, dictionariesArea.getName());
        List<DictionariesArea> dictionariesAreaList = dictionariesAreaMapper.selectList(dictionariesAreaWrapper);
        //通过工具类来区分市,省,区
        AddressResolutionUtil.addressResolution(dictionariesArea.getName());
        //返回给前端
        return ResponseResult.okResult(dictionariesAreaList);
    }

    @Override
    public ResponseResult dictionariesIcd(DictionariesIcd dictionariesIcd) {
        //查询疾病字典
        LambdaQueryWrapper<DictionariesIcd> dictionariesIcdLambdaQueryWrapper = Wrappers.lambdaQuery();
        dictionariesIcdLambdaQueryWrapper.eq(DictionariesIcd::getCode, dictionariesIcd.getCode());
        List<DictionariesIcd> dictionariesIcdList = dictionariesIcdMapper.selectList(dictionariesIcdLambdaQueryWrapper);
        return ResponseResult.okResult(dictionariesIcdList);
    }
}
