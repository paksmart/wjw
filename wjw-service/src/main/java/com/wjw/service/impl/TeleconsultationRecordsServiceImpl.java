package com.wjw.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.wjw.center.admin.dictionaries.DictionariesCertType;
import com.wjw.center.admin.dictionaries.DictionariesDoctorTitle;
import com.wjw.center.admin.dictionaries.DictionariesSubject;
import com.wjw.center.admin.domain.TeleconsultationRecordsAppliedListDO;
import com.wjw.center.admin.domain.TeleconsultationRecordsDO;
import com.wjw.center.admin.domain.TeleconsultationRecordsResultListDO;
import com.wjw.common.admin.TeleconsultationRecordsDto;
import com.wjw.common.dto.PageResponseResult;
import com.wjw.common.dto.ResponseResult;
import com.wjw.common.enums.AppHttpCodeEnum;
import com.wjw.common.exception.TstRuntimeException;
import com.wjw.mapper.*;
import com.wjw.service.TeleconsultationRecordsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @description: psk
* @author pansh
* @date 2022/9/29 10:34
* @version 1.0
*/

@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
public class TeleconsultationRecordsServiceImpl implements TeleconsultationRecordsService {

    private final TeleconsultationRecordsMapper teleconsultationRecordsMapper;
    private final TeleconsultationRecordsAppliedListMapper teleconsultationRecordsAppliedListMapper;
    private final TeleconsultationRecordsResultListMapper teleconsultationRecordsResultListMapper;
    private final DictionariesDoctorTitleMapper dictionariesDoctorTitleMapper;
    private final DictionariesSubjectMapper dictionariesSubjectMapper;
    private final DictionariesCertTypeMapper dictionariesCertTypeMapper;

    @Override
    public ResponseResult saveTeleconsultationRecords(TeleconsultationRecordsDO teleconsultationRecordsDO) {
        //如果对象为空,返回错误信息
        if (teleconsultationRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //会诊费用保留两位小数
        teleconsultationRecordsDO.setTotalFee(Double.valueOf(teleconsultationRecordsDO.getTotalFee()));
        //远程会诊全程留痕记录标识
        teleconsultationRecordsDO.setPatientId(IdUtil.getSnowflake(1, 1).nextId());
        teleconsultationRecordsDO.setSessionId(IdUtil.getSnowflake(1, 1).nextId());
        teleconsultationRecordsDO.setApplyDocId(IdUtil.getSnowflake(1, 1).nextId());
        teleconsultationRecordsDO.setTeleconsultationId(IdUtil.getSnowflake(1, 1).nextId());
        teleconsultationRecordsDO.setRecordsMark(IdUtil.getSnowflake(1,1).nextId());
        if (!SqlHelper.retBool(teleconsultationRecordsMapper.insert(teleconsultationRecordsDO))) {
            throw new TstRuntimeException("新增数据失败");
        }
        //新增互联网医院远程会诊记录监管(接收申请机构医生信息列表)
        List<TeleconsultationRecordsAppliedListDO> appliedList = teleconsultationRecordsDO.getAppliedList();
        if (CollectionUtils.isNotEmpty(appliedList)) {
            for (TeleconsultationRecordsAppliedListDO teleconsultationRecordsAppliedListDO : appliedList) {
                teleconsultationRecordsAppliedListDO.setId(teleconsultationRecordsDO.getTeleconsultationId());
                if (!SqlHelper.retBool(teleconsultationRecordsAppliedListMapper.insert(teleconsultationRecordsAppliedListDO))) {
                    throw new TstRuntimeException("新增数据失败");
                }
            }
        }

        //新增互联网医院远程会诊记录监管(会诊结果列表)
        List<TeleconsultationRecordsResultListDO> resultList = teleconsultationRecordsDO.getResultList();
        if (CollectionUtils.isNotEmpty(resultList)) {
            for (TeleconsultationRecordsResultListDO teleconsultationRecordsResultListDO : resultList) {
                teleconsultationRecordsResultListDO.setId(teleconsultationRecordsDO.getTeleconsultationId());
                if (!SqlHelper.retBool(teleconsultationRecordsResultListMapper.insert(teleconsultationRecordsResultListDO))) {
                    throw new TstRuntimeException("新增数据失败");
                }
            }
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getTeleconsultationRecords(TeleconsultationRecordsDO teleconsultationRecordsDO) {
        if (teleconsultationRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //构造查询条件
        LambdaQueryWrapper<TeleconsultationRecordsDO> teleconsultationRecordsWrapper = Wrappers.lambdaQuery();
        teleconsultationRecordsWrapper.eq(TeleconsultationRecordsDO::getPlatformId, teleconsultationRecordsDO.getPlatformId());
        teleconsultationRecordsWrapper.eq(TeleconsultationRecordsDO::getTeleconsultationId, teleconsultationRecordsDO.getTeleconsultationId());
        //根据平台id和会诊业务id来查询所有
        List<TeleconsultationRecordsDO> teleconsultationRecordsDOList = teleconsultationRecordsMapper.selectList(teleconsultationRecordsWrapper);

        //查询互联网医院远程会诊记录监管(接收申请机构医生信息列表)
        LambdaQueryWrapper<TeleconsultationRecordsAppliedListDO> teleconsultationRecordsAppliedListDWrapper = Wrappers.lambdaQuery();
        teleconsultationRecordsAppliedListDWrapper.eq(TeleconsultationRecordsAppliedListDO::getAppliedOrgCode,teleconsultationRecordsDO.getPlatformId());
        List<TeleconsultationRecordsAppliedListDO> teleconsultationRecordsAppliedListDOList = teleconsultationRecordsAppliedListMapper.selectList(teleconsultationRecordsAppliedListDWrapper);
        teleconsultationRecordsDO.setAppliedList(teleconsultationRecordsAppliedListDOList);


        //查询互联网医院远程会诊记录监管(会诊结果列表)
        LambdaQueryWrapper<TeleconsultationRecordsResultListDO> teleconsultationRecordsResultListWrapper  = Wrappers.lambdaQuery();
        teleconsultationRecordsResultListWrapper.eq(TeleconsultationRecordsResultListDO::getOrgCode,teleconsultationRecordsDO.getPlatformId());
        List<TeleconsultationRecordsResultListDO> teleconsultationRecordsResultListDOS = teleconsultationRecordsResultListMapper.selectList(teleconsultationRecordsResultListWrapper);
        teleconsultationRecordsDO.setResultList(teleconsultationRecordsResultListDOS);

        teleconsultationRecordsDOList.add(teleconsultationRecordsDO);

        //返回给前端
        return ResponseResult.okResult(teleconsultationRecordsDOList);
    }

    @Override
    public ResponseResult getByIdTeleconsultationRecords(Long platformId) {
        //根据id来查询
        TeleconsultationRecordsDO teleconsultationRecordsDO = teleconsultationRecordsMapper.selectById(platformId);
        //如果为空返回错误信息
        if (teleconsultationRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询互联网医院远程会诊记录监管(接收申请机构医生信息列表)详情
        TeleconsultationRecordsAppliedListDO teleconsultationRecordsAppliedListDO = teleconsultationRecordsAppliedListMapper.selectById(platformId);
        //查询互联网医院远程会诊记录监管(会诊结果列表)
        TeleconsultationRecordsResultListDO teleconsultationRecordsResultListDO = teleconsultationRecordsResultListMapper.selectById(platformId);
        //返回给前端
        return ResponseResult.okResult(teleconsultationRecordsDO);
    }

    @Override
    public ResponseResult deleteTeleconsultationRecords(Long platformId) {
        //根据id删除
        if (!SqlHelper.retBool(teleconsultationRecordsMapper.deleteById(platformId))) {
            throw new TstRuntimeException("删除数据失败");
        }
        return ResponseResult.okResult();
}

    @Override
    public ResponseResult updateTeleconsultationRecords(TeleconsultationRecordsDO teleconsultationRecordsDO) {
        //如果对象为空,返回错误
        if (teleconsultationRecordsDO == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //修改互联网医院远程会诊记录监管
        teleconsultationRecordsMapper.updateById(teleconsultationRecordsDO);

        //修改互联网医院远程会诊记录监管(接收申请机构医生信息列表)
        //先删除原信息
        LambdaQueryWrapper<TeleconsultationRecordsAppliedListDO> teleconsultationRecordsAppliedListWrapper = Wrappers.lambdaQuery();
        teleconsultationRecordsAppliedListWrapper.eq(TeleconsultationRecordsAppliedListDO::getAppliedOrgCode, teleconsultationRecordsDO.getPlatformId());
        teleconsultationRecordsAppliedListMapper.delete(teleconsultationRecordsAppliedListWrapper);
        //新增新信息
        List<TeleconsultationRecordsAppliedListDO> appliedList = teleconsultationRecordsDO.getAppliedList();
        if (CollectionUtils.isNotEmpty(appliedList)) {
            for (TeleconsultationRecordsAppliedListDO teleconsultationRecordsAppliedListDO : appliedList) {
                teleconsultationRecordsAppliedListDO.setId(teleconsultationRecordsDO.getTeleconsultationId());
                teleconsultationRecordsAppliedListMapper.insert(teleconsultationRecordsAppliedListDO);
            }
        }

        //修改互联网医院远程会诊记录监管(会诊结果列表)
        //先删除原信息
        LambdaQueryWrapper<TeleconsultationRecordsResultListDO> teleconsultationRecordsResultListWrapper = Wrappers.lambdaQuery();
        teleconsultationRecordsResultListWrapper.eq(TeleconsultationRecordsResultListDO::getOrgCode, teleconsultationRecordsDO.getPlatformId());
        teleconsultationRecordsResultListMapper.delete(teleconsultationRecordsResultListWrapper);
        //新增新信息
        List<TeleconsultationRecordsResultListDO> resultListDOList = teleconsultationRecordsDO.getResultList();
        if (CollectionUtils.isNotEmpty(resultListDOList)) {
            for (TeleconsultationRecordsResultListDO recordsAppliedListDO : resultListDOList) {
                recordsAppliedListDO.setId(teleconsultationRecordsDO.getTeleconsultationId());
                teleconsultationRecordsResultListMapper.insert(recordsAppliedListDO);
            }
        }
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getPageTeleconsultationRecords(TeleconsultationRecordsDto dto) {
        //设置分页条件
        Page<TeleconsultationRecordsDO> page = new Page<>(dto.getPage(), dto.getPageSize());

        //分页条件查询
        LambdaQueryWrapper<TeleconsultationRecordsDO> teleconsultationRecordsWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotEmpty(dto.getDiagianName())) {
            teleconsultationRecordsWrapper.like(TeleconsultationRecordsDO::getDiagianName, dto.getDiagianName());
        }
        IPage<TeleconsultationRecordsDO> teleconsultationRecordsDOIPage = teleconsultationRecordsMapper.selectPage(page, teleconsultationRecordsWrapper);
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getPageSize(), teleconsultationRecordsDOIPage.getTotal(), teleconsultationRecordsDOIPage.getRecords());

        return ResponseResult.okResult(pageResponseResult);
    }
}
