package com.wjw.service;

import com.wjw.center.admin.domain.RemoteClinicRecordDO;
import com.wjw.center.admin.param.RemoteClinicRecordParam;
import com.wjw.common.admin.RemoteClinicRecordDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/20 10:33
* @version 1.0
*/

public interface RemoteClinicRecordService {
    ResponseResult saveRemoteClinicRecord(RemoteClinicRecordDO remoteClinicRecordDO);

    ResponseResult getRemoteClinicRecord(RemoteClinicRecordParam param);

    ResponseResult getByIdRemoteClinicRecord(Long id);

    ResponseResult deleteRemoteClinicRecord(Long id);

    ResponseResult updateRemoteClinicRecord(RemoteClinicRecordDO remoteClinicRecordDO);

    ResponseResult getPageRemoteClinicRecord(RemoteClinicRecordDto dto);
}
