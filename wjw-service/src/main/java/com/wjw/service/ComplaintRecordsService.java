package com.wjw.service;


import com.wjw.center.admin.domain.ComplaintRecordsDO;
import com.wjw.center.admin.param.ComplaintRecordsParam;
import com.wjw.common.admin.ComplaintRecordsDto;
import com.wjw.common.dto.ResponseResult;

/**
* @description: psk
* @author pansh
* @date 2022/9/21 9:04
* @version 1.0
*/
public interface ComplaintRecordsService {
    ResponseResult saveComplaintRecords(ComplaintRecordsDO complaintRecordsDO);

    ResponseResult getComplaintRecords(ComplaintRecordsParam param);

    ResponseResult getByIdComplaintRecords(Long platformId);

    ResponseResult deleteComplaintRecords(Long platformId);

    ResponseResult updateComplaintRecords(ComplaintRecordsDO complaintRecordsDO);

    ResponseResult getPageComplaintRecords(ComplaintRecordsDto dto);
}
