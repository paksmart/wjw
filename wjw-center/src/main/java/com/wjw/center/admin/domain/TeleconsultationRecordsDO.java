package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description teleconsultation_records
 * @author psk
 * @date 2022-09-29
 */
@Data
@TableName("tst_teleconsultation_records")
@ApiModel("诊疗管理:互联网医院远程会诊记录监管")
public class TeleconsultationRecordsDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 会诊业务id
    */
    @ApiModelProperty("会诊业务id")
    private Long teleconsultationId;

    /**
    * 远程会诊类型(1:普通会诊，2:紧急会诊)
    */
    @ApiModelProperty("远程会诊类型(1:普通会诊，2:紧急会诊)")
    private Integer teleconsultationType;

    /**
    * 初步诊断编码(icd编码)
    */
    @ApiModelProperty("初步诊断编码(icd编码)")
    private String diagianCode;

    /**
    * 初步诊断名称
    */
    @ApiModelProperty("初步诊断名称")
    private String diagianName;

    /**
    * 会诊模式(1:点名会诊，2:会诊中心，3:远程查房，9:其他)
    */
    @ApiModelProperty("会诊模式(1:点名会诊，2:会诊中心，3:远程查房，9:其他)")
    private Integer teleconsultationMode;

    /**
    * 会诊说明
    */
    @ApiModelProperty("会诊说明")
    private String teleconsultationExplain;

    /**
    * 会诊目的
    */
    @ApiModelProperty("会诊目的")
    private String teleconsultationPurpose;

    /**
    * 申请机构编码
    */
    @ApiModelProperty("申请机构编码")
    private String applyOrgCode;

    /**
    * 申请机构名称
    */
    @ApiModelProperty("申请机构名称")
    private String applyOrgName;

    /**
    * 申请机构医生id
    */
    @ApiModelProperty("申请机构医生id")
    private Long applyDocId;

    /**
    * 申请机构医生姓名
    */
    @ApiModelProperty("申请机构医生姓名")
    private String applyDocName;

    /**
    * 申请机构医生手机号
    */
    @ApiModelProperty("申请机构医生手机号")
    private String applyDocIdMobile;

    /**
    * 申请医生证件类型(证件类型字典)
    */
    @ApiModelProperty("申请医生证件类型(证件类型字典)")
    private Integer applyDocCertType;

    /**
    * 申请医生证件号
    */
    @ApiModelProperty("申请医生证件号")
    private String applyDocCertNo;

    /**
    * 申请机构医生职称
    */
    @ApiModelProperty("申请机构医生职称")
    private String applyDocTitle;

    /**
    * 申请机构医生科目编码(科目字典)
    */
    @ApiModelProperty("申请机构医生科目编码(科目字典)")
    private String applyDocSubjectCode;

    /**
    * 申请机构医生科目名称
    */
    @ApiModelProperty("申请机构医生科目名称")
    private String applyDocSubjectName;

    /**
    * 申请机构医生科室名称
    */
    @ApiModelProperty("申请机构医生科室名称")
    private String applyDeptName;

    /**
     * 接收申请机构医生信息列表
     */
    @ApiModelProperty("接收申请机构医生信息列表")
    @TableField(exist = false)
    private List<TeleconsultationRecordsAppliedListDO> appliedList = new ArrayList<>();
    /**
    * 会诊费用(保留两位小数)
    */
    @ApiModelProperty("会诊费用(保留两位小数)")
    private Double totalFee;

    /**
    * 支付状态(0:未支付，1:已支付，2:退款)
    */
    @ApiModelProperty("支付状态(0:未支付，1:已支付，2:退款)")
    private Integer payStatus;

    /**
    * 会诊状态(1:待处理，2:会诊中，3:拒绝，4:取消，5:结束)
    */
    @ApiModelProperty("会诊状态(1:待处理，2:会诊中，3:拒绝，4:取消，5:结束)")
    private Integer status;

    /**
    * 会话id
    */
    @ApiModelProperty("会话id")
    private Long sessionId;

    /**
    * 会话开始时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("会话开始时间")
    private Date sessionStartTime;

    /**
    * 会话结束时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("会话结束时间")
    private Date sessionEndTime;

    /**
    * 远程会诊全程留痕记录标识(根据标识可查询对应沟通记录，包含图文，语音，视频记录(互联网医院需提供根据记录标识查询记录接口)
    */
    @ApiModelProperty("远程会诊全程留痕记录标识(根据标识可查询对应沟通记录，包含图文，语音，视频记录(互联网医院需提供根据记录标识查询记录接口)")
    private Long recordsMark;

    /**
    * 支付方式(1:支付宝，2:微信，3:银联，9:其他)
    */
    @ApiModelProperty("支付方式(1:支付宝，2:微信，3:银联，9:其他)")
    private Integer payMode;

    /**
    * 订单号
    */
    @ApiModelProperty("订单号")
    private String orderNum;

    /**
     * 会诊结果列表
     */
    @ApiModelProperty("会诊结果列表")
    @TableField(exist = false)
    private List<TeleconsultationRecordsResultListDO> resultList = new ArrayList<>();

    /**
    * 就诊人id
    */
    @ApiModelProperty("就诊人id")
    private Long patientId;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public TeleconsultationRecordsDO() {}
}
