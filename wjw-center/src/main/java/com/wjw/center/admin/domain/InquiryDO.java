package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @description inquiry
 * @author psk
 * @date 2022-09-17
 */
@Data
@TableName("tst_inquiry")
public class InquiryDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    private Long platformId;

    /**
    * 资讯id
    */
    private Long inquiryId;

    /**
    * 资讯医(药，护)师所属专业代码(诊疗科目字典)
    */
    private String subjectCode;

    /**
     * 咨询医(药,护)师所属专业名称(诊疗科目名称)
     */
    private String subjectName;

    /**
    * 科室id
    */
    private Long deptId;

    /**
    * 科室名称
    */
    private String deptName;

    /**
    * 诊疗科目编码(科目字典)
    */
    private String diagnosisSubjectCode;

    /**
    * 诊疗科目名称
    */
    private String diagnosisSubjectName;

    /**
    * 医(药，护)师ID
    */
    private Long staffId;

    /**
    * 医(药，护)师姓名
    */
    private String staffName;

    /**
    * 医(药，护)师证件类型(证件类型字典)
    */
    private Integer staffCertType;

    /**
    * 医(药,护)师证件号
    */
    private String staffCertNo;

    /**
    * 就诊人身份证号
    */
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    private String patientHealthCard;

    /**
    * 就诊人姓名
    */
    private String patientName;

    /**
    * 就诊人年龄
    */
    private Integer patientAge;

    /**
    * 就诊人性别
    */
    private Integer patientSex;

    /**
    * 就诊人电话
    */
    private String patientMobile;

    /**
    * 监护人姓名(儿童小于6岁需要)
    */
    private String guardianIdName;

    /**
    * 证件类型字典(证件类型字典)
    */
    private Integer guardianCertType;

    /**
    * 监护人证件号(儿童小于6岁需要)
    */
    private String guardianCertNo;

    /**
    * 监护人手机(儿童小于6岁需要)
    */
    private String guardianMobile;

    /**
    * 咨询类型(1:图文咨询，2:音频咨询，3:视频咨询，9:其他)
    */
    private Integer inquiryType;

    /**
    * 咨询属性(1:诊疗咨询，2:药事咨询，3:医护咨询，9:其他)
    */
    private Integer inquiryAttribute;

    /**
    * 咨询护理分类(inquiryattribute=3 必填)
    */
    private String inquiryCategory;

    /**
    * 咨询时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date applyDate;

    /**
    * 开始时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
    * 结束时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
    * 字符渠道(1:支付宝，2:微信，3:银联，9:其他)
    */
    private Integer paymentChannel;

    /**
    * 咨询价格(单位元，保留两位小数)
    */
    private Double inquiryPrice;

    /**
    * 咨询问题
    */
    private String content;

    /**
    * 是否回复(0:未回复，1:回复)
    */
    private Integer answerMark;

    /**
    * 拒绝时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date refuseTime;

    /**
    * 拒绝原因
    */
    private String refuseReason;

    /**
    * 拒绝类型(1:医生主动拒绝，2:超时未回复系统自动拒绝)
    */
    private Integer refuseType;

    /**
    * 在线咨询全程留痕记录唯一标识(根据标识可查询对应沟通记录，包含图文，语音，视频记录(互联网医院需提供根据记录标识查询记录接口))
    */
    private Long recordsMark;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
    * 创建人
    */
    private Long createUser;

    /**
    * 修改人
    */
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    private Integer isDeleted;

    public InquiryDO() {}
}
