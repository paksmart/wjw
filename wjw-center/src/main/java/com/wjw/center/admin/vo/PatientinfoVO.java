package com.wjw.center.admin.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description patientinfo
 * @author psk
 * @date 2022-09-17
 */
@Data
@ApiModel("patientinfo")
public class PatientinfoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 主键
    */
    @ApiModelProperty("主键")
    private Long id;

    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 用户id
    */
    @ApiModelProperty("用户id")
    private String userId;

    /**
    * 用户姓名
    */
    @ApiModelProperty("用户姓名")
    private String userName;

    /**
    * 手机号
    */
    @ApiModelProperty("手机号")
    private String mobile;

    /**
    * 用户证件类型(证件类型字典)
    */
    @ApiModelProperty("用户证件类型(1:身份证,2:护照,3:军官证,4:台胞证,5:同乡证,99:其他)")
    private Integer certType;

    /**
    * 用户证件号(新生儿童证件号为母亲证件号胎数，如34***1201，01为胎数)
    */
    @ApiModelProperty("用户证件号(新生儿童证件号为母亲证件号胎数，如34***1201，01为胎数)")
    private String certNo;

    /**
    * 年龄(岁)
    */
    @ApiModelProperty("年龄(岁)")
    private Integer age;

    /**
    * 性别
    */
    @ApiModelProperty("性别(0:未知的性别,1:男,2:女,5:女性变为男性,6:男性变为女性,9:未说明的性别)")
    private Integer sex;

    /**
    * 出生年月(yyyy-mm-dd)
    */
    @ApiModelProperty("出生年月(yyyy-mm-dd)")
    private Date birthday;

    /**
    * 民族
    */
    @ApiModelProperty("民族(民族(1:汉族,2:蒙古族,3:回族,4:藏族,5:维吾尔族,6:苗族,7:彝族,8:壮族,9:布依族,10:朝鲜族,11:满族,12:侗族\\n\" +\n" +
            "            \"     * 13:瑶族,14:白族,15:土家族,16:哈尼族,17:哈萨克族,18:傣族,19:黎族,20:傈僳族,21:佤族,22:畲族,23:高山族,\\n\" +\n" +
            "            \"     * 24:拉祜族,25:水族,26:东乡族,27:纳西族,28:景颇族,29:柯尔克孜族,30:土族,31:达幹尔族,32:仫佬族,33:羌族,\\n\" +\n" +
            "            \"     * 34:布朗族,35:撒拉族,36:毛南族,37:仡佬族,38:锡伯族,39:阿昌族,40:普米族,41:塔吉克族,42:怒族,43:乌孜别克族,\\n\" +\n" +
            "            \"     * 44:俄罗斯族,45:鄂温克族,46:德昂族,47:保安族,48:裕固族,49:京族,50:塔塔尔族,51:独龙族,52:鄂伦春族,53:赫哲族,\\n\" +\n" +
            "            \"     * 54:门巴族,55:珞巴族,56:基诺族))")
    private Integer nation;

    /**
    * 省代码(地区字典)
    */
    @ApiModelProperty("省代码(地区字典)")
    private Integer provinceCode;

    /**
    * 市代码(地区字典)
    */
    @ApiModelProperty("市代码(地区字典)")
    private Integer cityCode;

    /**
    * 区代码(地区字典)
    */
    @ApiModelProperty("区代码(地区字典)")
    private Integer areaCode;

    /**
    * 地址
    */
    @ApiModelProperty("地址")
    private String address;

    /**
    * 监管人证件类型(证件类型字典)
    */
    @ApiModelProperty("监管人证件类型(证件类型字典)")
    private Integer guardianCardType;

    /**
    * 监护人证件号(年龄小于6需要监护人)
    */
    @ApiModelProperty("监护人证件号(年龄小于6需要监护人)")
    private String guardianCardNo;

    /**
    * 监护人姓名(年龄小于6需要监护人)
    */
    @ApiModelProperty("监护人姓名(年龄小于6需要监护人)")
    private String guardianName;

    /**
    * 用户就诊人信息(如没就诊人，默认用户本身为就诊人)
    */
    @ApiModelProperty("用户就诊人信息(如没就诊人，默认用户本身为就诊人)")
    private Long patients;

    /**
    * 就诊人id
    */
    @ApiModelProperty("就诊人id")
    private String patientsId;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    @ApiModelProperty("就诊人证件类型(证件类型字典)")
    private Integer patientCertType;

    /**
    * 就诊人证件号码
    */
    @ApiModelProperty("就诊人证件号码")
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人姓名
    */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人手机号
    */
    @ApiModelProperty("就诊人手机号")
    private String patientMobile;

    /**
    * 就诊人是否实名认证(0:未实名，1:已实名)
    */
    @ApiModelProperty("就诊人是否实名认证(0:未实名，1:已实名)")
    private Integer realNameStatus;

    /**
    * 就诊人是否本人(0:非本人，1;本人)
    */
    @ApiModelProperty("就诊人是否本人(0:非本人，1;本人)")
    private Integer selfMark;

    /**
    * 性别(性别字典)
    */
    @ApiModelProperty("性别(性别字典)")
    private Integer patientSex;

    /**
    * 年龄(就诊人年龄)
    */
    @ApiModelProperty("年龄(就诊人年龄)")
    private Integer patientAge;

    /**
    * 生日(yyyy-mm-dd)
    */
    @ApiModelProperty("生日(yyyy-mm-dd)")
    private Date patientBirthday;

    /**
    * 就诊人民族(民族字典)
    */
    @ApiModelProperty("就诊人民族(民族字典)")
    private Integer patientNation;

    /**
    * 就诊人省代码
    */
    @ApiModelProperty("就诊人省代码")
    private Integer patientProvinceCode;

    /**
    * 就诊人市代码
    */
    @ApiModelProperty("就诊人市代码")
    private Integer patientCityCode;

    /**
    * 就诊人区代码
    */
    @ApiModelProperty("就诊人区代码")
    private Integer patientAreaCode;

    /**
    * 就诊人监护人证件类型
    */
    @ApiModelProperty("就诊人监护人证件类型")
    private Integer patientGuardianCertType;

    /**
    * 就诊人监护人证件号(儿童<6岁需要)
    */
    @ApiModelProperty("就诊人监护人证件号(儿童<6岁需要)")
    private String patientGuardianCertNo;

    /**
    * 就诊人监护人姓名(儿童<6岁需要)
    */
    @ApiModelProperty("就诊人监护人姓名(儿童<6岁需要)")
    private String patientGuardianName;

    /**
    * 就诊人状态(1:正常，2:删除)
    */
    @ApiModelProperty("就诊人状态(1:正常，2:删除)")
    private Integer patientStatus;

    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    public PatientinfoVO() {}
}
