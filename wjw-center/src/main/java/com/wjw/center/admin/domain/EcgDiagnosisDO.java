package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description ecg_diagnosis
 * @author psk
 * @date 2022-09-28
 */
@Data
@Accessors(chain = true)
@TableName("tst_ecg_diagnosis")
@ApiModel("诊疗管理:互联网医院远程心电图诊断记录监管")
public class EcgDiagnosisDO  implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 心电图诊断记录id
    */
    @ApiModelProperty("心电图诊断记录id")
    private Long ecgDiagnosisId;

    /**
    * 心电检查类别代码
    */
    @ApiModelProperty("心电检查类别代码")
    private String ecgType;

    /**
    * 心电检查类别名称
    */
    @ApiModelProperty("心电检查类别名称")
    private String ecgName;

    /**
    * 诊断申请时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("诊断申请时间")
    private Date applyDate;

    /**
    * 初步诊断编码
    */
    @ApiModelProperty("初步诊断编码")
    private String diagnosisCode;

    /**
    * 初步诊断名称
    */
    @ApiModelProperty("初步诊断名称")
    private String diagnosisName;

    /**
    * 就诊人简要病史描述
    */
    @ApiModelProperty("就诊人简要病史描述")
    private String diseasesHistory;

    /**
    * 会诊目的
    */
    @ApiModelProperty("会诊目的")
    private String purpose;

    /**
    * 会诊人病历描述
    */
    @ApiModelProperty("会诊人病历描述")
    private String patientCaseDesc;

    /**
    * 远程心电诊断时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("远程心电诊断时间")
    private Date reportDate;

    /**
    * 心电描述
    */
    @ApiModelProperty("心电描述")
    private String ecgDesc;

    /**
    * 心电诊断意见
    */
    @ApiModelProperty("心电诊断意见")
    private String ecgOpinion;

    /**
    * 心电图异常状态(0:无状态，1:有状态)
    */
    @ApiModelProperty("心电图异常状态(0:无状态，1:有状态)")
    private Integer abnormalStatus;

    /**
    * 心电图异常描述
    */
    @ApiModelProperty("心电图异常描述")
    private String abnormalDesc;

    /**
    * 是否支付(0:未支付，1:已支付，2:已退款)
    */
    @ApiModelProperty("是否支付(0:未支付，1:已支付，2:已退款)")
    private Integer payStatus;

    /**
    * 支付方式(1:支付宝，2:微信，3:银联，9:其他)
    */
    @ApiModelProperty("支付方式(1:支付宝，2:微信，3:银联，9:其他)")
    private Integer payMode;

    /**
    * 支付价格(保留两位小数)
    */
    @ApiModelProperty("支付价格(保留两位小数)")
    private Double ecgPrice;

    /**
    * 影像dicom文件下载地址
    */
    @ApiModelProperty("影像dicom文件下载地址")
    private String fileUrl;

    /**
    * 申请机构编码
    */
    @ApiModelProperty("申请机构编码")
    private String applyOrgCode;

    /**
    * 申请机构名称
    */
    @ApiModelProperty("申请机构名称")
    private String applyOrgName;

    /**
    * 申请机构科目编码(科目字典)
    */
    @ApiModelProperty("申请机构科目编码(科目字典)")
    private String applySubjectCode;

    /**
    * 申请机构科目名称
    */
    @ApiModelProperty("申请机构科目名称")
    private String applySubjectName;

    /**
    * 申请机构科室id
    */
    @ApiModelProperty("申请机构科室id")
    private Long applyDeptId;

    /**
    * 申请机构科室名称
    */
    @ApiModelProperty("申请机构科室名称")
    private String applyDeptName;

    /**
    * 申请机构医生id
    */
    @ApiModelProperty("申请机构医生id")
    private Long applyDocId;

    /**
    * 申请机构医生姓名
    */
    @ApiModelProperty("申请机构医生姓名")
    private String applyDocName;

    /**
    * 申请机构医生手机号
    */
    @ApiModelProperty("申请机构医生手机号")
    private String applyDocMobile;

    /**
    * 申请医生证件类型(证件类型字典)
    */
    @ApiModelProperty("申请医生证件类型(证件类型字典)")
    private Integer applyDocCertType;

    /**
    * 申请医生证件号
    */
    @ApiModelProperty("申请医生证件号")
    private String applyDocCertNo;

    /**
    * 申请机构医生职称
    */
    @ApiModelProperty("申请机构医生职称")
    private String applyDocTitle;

    /**
    * 接收申请信息列表
    */
    @ApiModelProperty("接收申请信息列表")
    @TableField(exist = false)
    private List<EcgDiagnosisAppliedListDO> appliedList = new ArrayList<>();

    /**
    * 就诊人id
    */
    @ApiModelProperty("就诊人id")
    private Long patientId;

    /**
    * 就诊人姓名
    */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    @ApiModelProperty("就诊人证件类型(证件类型字典)")
    private Integer patientCertType;

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人证件号
    */
    @ApiModelProperty("就诊人证件号")
    private String patientCertNo;

    /**
    * 就诊人年龄
    */
    @ApiModelProperty("就诊人年龄")
    private Integer patientAge;

    /**
    * 就诊人性别(性别字典)
    */
    @ApiModelProperty("就诊人性别(性别字典)")
    private Integer patientSex;

    /**
    * 就诊人生日
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("就诊人生日")
    private Date patientBirthday;

    /**
    * 就诊人手机号
    */
    @ApiModelProperty("就诊人手机号")
    private String patientMobile;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public EcgDiagnosisDO() {}
}
