package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description pathology_diagnosis_applied_list
 * @author psk
 * @date 2022-09-29
 */
@Data
@TableName("tst_pathology_diagnosis_applied_list")
public class PathologyDiagnosisAppliedListDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
    * 接受申请机构编码
    */
    @ApiModelProperty("接受申请机构编码")
    private String appliedOrgCode;

    /**
    * 接受申请机构名称
    */
    @ApiModelProperty("接受申请机构名称")
    private String appliedOrgName;

    /**
    * 接受申请机构科目编码(科目子弹)
    */
    @ApiModelProperty("接受申请机构科目编码(科目子弹)")
    private String appliedSubjectCode;

    /**
    * 接受申请机构科目名称
    */
    @ApiModelProperty("接受申请机构科目名称")
    private String appliedSubjectName;

    /**
    * 接受申请机构科室id
    */
    @ApiModelProperty("接受申请机构科室id")
    private String appliedDeptId;

    /**
    * 接受申请机构科室名称
    */
    @ApiModelProperty("接受申请机构科室名称")
    private String appliedDeptName;

    /**
    * 接受申请医生id
    */
    @ApiModelProperty("接受申请医生id")
    private Long appliedDocId;

    /**
    * 接受申请医生姓名
    */
    @ApiModelProperty("接受申请医生姓名")
    private String appliedDocName;

    /**
    * 接受申请医生证件类型(证件类型字典)
    */
    @ApiModelProperty("接受申请医生证件类型(证件类型字典)")
    private Integer appliedDocCertType;

    /**
    * 接受申请医生证件号
    */
    @ApiModelProperty("接受申请医生证件号")
    private String appliedDocCertNo;

    /**
    * 接受申请医生手机号
    */
    @ApiModelProperty("接受申请医生手机号")
    private String appliedDocMobile;

    /**
    * 接受申请医生职称(职称字典)
    */
    @ApiModelProperty("接受申请医生职称(职称字典)")
    private String appliedDocTitle;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 互联网医院远程病理诊断记录监管(接受申请信息列表
    */
    @ApiModelProperty("互联网医院远程病理诊断记录监管(接受申请信息列表")
    private Integer isDeleted;

}
