package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description diagnosis_medicines
 * @author psk
 * @date 2022-10-18
 */
@Data
@TableName("diagnosis_medicines")
public class DiagnosisMedicinesDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
    * 药品上下架 0:上架 1:下架
    */
    private Integer medicineshow;

    /**
    * medicinename
    */
    private String medicinename;

    /**
    * 药品库存
    */
    private Integer diagnosisMedicinestock;

    /**
    * 销量
    */
    private Integer sales;

    /**
    * 药品别名
    */
    private String medicinealias;

    /**
    * 进价
    */
    private BigDecimal buyprice;

    /**
    * 售价
    */
    private BigDecimal saleprice;

    /**
    * grainbuyprice
    */
    private BigDecimal grainbuyprice;

    /**
    * grainname
    */
    private String grainname;

    /**
    * grainsaleprice
    */
    private BigDecimal grainsaleprice;

    /**
    * grammage
    */
    private BigDecimal grammage;

    /**
    * 药品单位
    */
    private String medicineunit;

    public DiagnosisMedicinesDO() {}
}
