package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @description subsequent_visit
 * @author psk
 * @date 2022-09-19
 */
@Data
@TableName("tst_subsequent_visit")
public class SubsequentVisitDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    private Long platformId;

    /**
    * 复诊id
    */
    private Long subsequentVisitId;

    /**
    * 诊疗科目代码(科目字典)
    */
    private String diagnosisSubjectCode;

    /**
    * 诊疗科目名称
    */
    private String diagnosisSubjectName;

    /**
    * 复诊药师科目编码(科目字典)
    */
    private String subjectCode;

    /**
    * 复诊药师科目名称
    */
    private String subjectName;

    /**
    * 复诊药师科室id
    */
    private Long deptId;

    /**
    * 复诊药师姓名
    */
    private String deptName;

    /**
    * 复诊药师证件类型(证件类型字典)
    */
    private Integer docCertType;

    /**
    * 复诊药师证件号
    */
    private String docCertNo;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    private Integer patientCertType;

    /**
    * 就诊人证件号
    */
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    private String patientHealthCard;

    /**
    * 就诊人姓名
    */
    private String patientName;

    /**
    * 就诊人年龄
    */
    private Integer patientAge;

    /**
    * 就诊人性别(性别字典)
    */
    private Integer patientSex;

    /**
    * 就诊人手机号
    */
    private String patientMobile;

    /**
    * 监护人姓名(儿童小于6岁需要)
    */
    private String guardianIdName;

    /**
    * 证件类型字典(证件类型字典)
    */
    private Integer guardianCertType;

    /**
    * 监护人证件号(儿童小于6岁需要)
    */
    private String guardianCertNo;

    /**
    * 监护人手机(儿童小于6岁需要)
    */
    private String guardianMobile;

    /**
    * 复诊类型(1:图文交流，2:语音交流，3:视频交流，9:其他)
    */
    private Integer subsequentVisitType;

    /**
     *复诊费用(复诊价格(保留两位小数))
     */
    private Double totalFee;

    /**
    * 卡类型(1:医院就诊卡，2:医保卡，3:医院病历号(门 诊号)
    */
    private Integer cardType;

    /**
    * 卡号
    */
    private String cardNo;

    /**
    * 就诊人简要病史描述
    */
    private String diseasesHistory;

    /**
    * 就诊人诊断(复诊就诊人在实体医院的诊断名称，如有多条，使用统一分隔符进行分隔)
    */
    private String originalDiagnosis;

    /**
    * 就诊人复诊证明标识(证明材料文件唯一标识(互联网医院需提供证明标识查询复诊证明文件接口))
    */
    private Long proveMark;

    /**
    * 在线复诊申请时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date applyDate;

    /**
    * 复诊开始时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
    * 复诊结束时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
    * 本次诊断
    */
    private String thisDiagnosis;

    /**
    * 未确诊原因
    */
    private String undiagnosedCause;

    /**
    * 是否回复(0:未回复，1:回复)
    */
    private Integer answerMark;

    /**
    * 复诊拒绝/取消 时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date refuseTime;

    /**
    * 复诊拒绝/取消 原因
    */
    private String refuseReason;

    /**
    * 复诊拒绝类别(1:医生主动拒绝，2:超时 未回复系统自动拒绝)
    */
    private Integer refuseType;

    /**
    * 在线复诊全程留痕记录唯一标识(根据标识可查询对应沟通记录，包含图文，语音，视频记录(互联网医院需要提供根据记录标识查询记录接口)
    */
    private Long recordsMark;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
    * 创建人
    */
    private Long createUser;

    /**
    * 修改人
    */
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    private Integer isDeleted;

    public SubsequentVisitDO() {}
}
