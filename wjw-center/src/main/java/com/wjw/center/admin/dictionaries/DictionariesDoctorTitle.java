package com.wjw.center.admin.dictionaries;
/**
* @description: psk
* @author pansh
* @date 2022/9/24 10:15
* @version 1.0
*/

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


@ColumnWidth(30)//注释在具体属性上,设置单独列。注释在类上,统一设置列宽
@HeadRowHeight(30)//设置表头行高
@ContentRowHeight(20)//统一设置数据行行高
@ApiModel(value = "医生职称字典", description = "")
@Data
@TableName("tst_dictionaries_doctor_title")
@EqualsAndHashCode(callSuper = false)
public class DictionariesDoctorTitle {

    /**
     * 编码
     */
    @ExcelProperty(value = "职称代码",index = 0)
    @ApiModelProperty("职称代码")
    private String code;

    /**
     * 名称
     */
    @ExcelProperty(value = "职称（type_code：TITLE）",index = 1)
    @ApiModelProperty("职称")
    private String name;
}
