package com.wjw.center.admin.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description prescription_circulation
 * @author psk
 * @date 2022-09-19
 */
@Data
@ApiModel("诊疗管理:互联网在线处方流转(配送,发药,购药)信息")
public class PrescriptionCirculationParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 主键
    */
    @ApiModelProperty("主键")
    private Long id;

    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 复诊id
    */
    @ApiModelProperty("复诊id")
    private String subsequentVisitId;

    /**
    * 处方唯一号
    */
    @ApiModelProperty("处方唯一号")
    private String prescriptionNo;

    /**
    * 处方配送方式(配送方式:1:配药取药，2:医院药房快递配送，3:药企物流配送，4:药店物流配送，5:药店自取，6:自动失效，9:其他)
    */
    @ApiModelProperty("处方配送方式(配送方式:1:配药取药，2:医院药房快递配送，3:药企物流配送，4:药店物流配送，5:药店自取，6:自动失效，9:其他)")
    private Integer deliveryType;

    /**
    * 处方配送时间(物流配送，医院药房发药或药店自购时间，自动失效时间)
    */
    @ApiModelProperty("处方配送时间(物流配送，医院药房发药或药店自购时间，自动失效时间)")
    private Date deliveryTime;

    /**
    * 配送人(deliverytype=2，3是需要传入)
    */
    @ApiModelProperty("配送人(deliverytype=2，3是需要传入)")
    private String deliveryPeople;

    /**
    * 处方开始配送日期(deliverytype=2，3是需要传入)
    */
    @ApiModelProperty("处方开始配送日期(deliverytype=2，3是需要传入)")
    private Date deliveryStartDate;

    /**
    * 处方完成配送日期(deliverytype=2，3是需要传入)
    */
    @ApiModelProperty("处方完成配送日期(deliverytype=2，3是需要传入)")
    private Date deliveryEndDate;

    /**
    * 处方配送金额(deliverytype=20，30是需要传入，保留两位小数)
    */
    @ApiModelProperty("处方配送金额(deliverytype=20，30是需要传入，保留两位小数)")
    private Double deliveryFee;

    /**
    * 处方总金额(保留两位小数)
    */
    @ApiModelProperty("处方总金额(保留两位小数)")
    private Double totalFee;

    /**
    * 是否支付(0:未支付，1:已支付)
    */
    @ApiModelProperty("是否支付(0:未支付，1:已支付)")
    private Integer payMark;

    /**
    * 第三方支付交流水号(在线支付流水号，医院结算单号，药店结算单号)
    */
    @ApiModelProperty("第三方支付交流水号(在线支付流水号，医院结算单号，药店结算单号)")
    private String tradeNo;

    public PrescriptionCirculationParam() {}
}
