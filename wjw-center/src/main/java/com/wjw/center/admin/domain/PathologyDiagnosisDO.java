package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description pathology_diagnosis
 * @author psk
 * @date 2022-09-27
 */
@Data
@TableName("tst_pathology_diagnosis")
public class PathologyDiagnosisDO implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 平台机构
    */
        private Long platformId;

    /**
    * 远程病理诊断记录id
    */
    private Long pathologyDiagnosisId;

    /**
    * 病理检查类别代码(1:细胞学诊断，2:常规组织学，3:术中冰冻诊断)
    */
    private String pathologyType;

    /**
    * 病理检查类别名称
    */
    private String pathologyName;

    /**
    * 诊断申请时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date applyDate;

    /**
    * 初步诊断编码
    */
    private String diagnosisCode;

    /**
    * 初步诊断名称
    */
    private String diagnosisName;

    /**
    * 就诊人简要病史描述
    */
    private String diseasesHistory;

    /**
    * 会诊目的
    */
    private String purpose;

    /**
    * 就诊人病历描述
    */
    private String patientCaseDesc;

    /**
    * 远程病理诊断时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date reportDate;

    /**
    * 远程病理描述
    */
    private String pathologyDesc;

    /**
    * 远程病理诊断意见
    */
    private String pathologyOpinion;

    /**
    * 是否支付(0:未支付，1:已支付，2:已退款)
    */
    private Integer payStatus;

    /**
    * 支付方式(1:支付宝，2:微信，3:银联，9:其他)
    */
    private Integer payMode;

    /**
    * 支付价格(保留两位小数)
    */
    private Double pathologyPrice;

    /**
    * 影像dicom 文件下载地址
    */
    private String fileUrl;

    /**
    * 申请机构编码
    */
    private String applyOrgCode;

    /**
    * 申请机构名称
    */
    private String applyOrgName;

    /**
    * 申请机构医生科目编码(科目字典)
    */
    private String applySubjectCode;

    /**
    * 申请机构医生科目名称
    */
    private String applySubjectName;

    /**
    * 申请机构医生科室名称
    */
    private String applyDeptName;

    /**
    * 申请机构科室编码
    */
    private String applyDeptId;

    /**
    * 申请机构医生id
    */
    private Long applyDocId;

    /**
    * 申请机构医生姓名
    */
    private String applyDocName;

    /**
    * 申请机构医生手机号
    */
    private String applyDocMobile;

    /**
    * 申请医生证件类型(证件类型字典)
    */
    private Integer applyDocCertType;

    /**
    * 申请医生证件号
    */
    private String applyDocCertNo;

    /**
    * 申请机构医生职称
    */
    private String applyDocTitle;

    /**
     * 接受申请医生信息列表
     */
    @TableField(exist = false)
    private List<PathologyDiagnosisAppliedListDO> appliedList = new ArrayList<>();

    /**
    * 就诊人id
    */
    private Long patientId;

    /**
    * 就诊人姓名
    */
    private String patientName;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    private Integer patientCertType;

    /**
    * 就诊人证件号
    */
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    private String patientHealthCard;

    /**
    * 就诊人年龄
    */
    private Integer patientAge;

    /**
     * 就诊人生日(yyyy-mm-dd)
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date patientBirthday;

    /**
    * 就诊人性别(性别字典)
    */
    private Integer patientSex;

    /**
    * 就诊人手机号
    */
    private String patientMobile;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
    * 创建人
    */
    private Long createUser;

    /**
    * 修改人
    */
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    private Integer isDeleted;

    public PathologyDiagnosisDO() {}
}
