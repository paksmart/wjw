package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @description filing_staff
 * @author zhengkai.blog.csdn.net
 * @date 2022-11-12
 */
@Data
@TableName("tst_filing_staff")
public class FilingStaffDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 平台机构id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("平台机构id")
    private Long platformId;
    /**
    * 医疗人员id
    */
    @ApiModelProperty("医疗人员id")
    private Long staffId;

    /**
    * 医疗人员姓名
    */
    @ApiModelProperty("医疗人员姓名")
    private String staffName;

    /**
    * 医疗人员证件号(证件类型字典)
    */
    @ApiModelProperty("医疗人员证件号(证件类型字典)")
    private Integer staffCertType;

    /**
    * 医疗人员证件号
    */
    @ApiModelProperty("医疗人员证件号")
    private String staffCertNo;

    /**
    * 是否实名认证(1:实名，2:未实名)
    */
    @ApiModelProperty("是否实名认证(1:实名，2:未实名)")
    private Integer realNameStatus;

    /**
    * 医疗人员图片(图片大小不超过50k，图片字节流须base64编码后再传输，图片格式为png)
    */
    @ApiModelProperty("医疗人员图片(图片大小不超过50k，图片字节流须base64编码后再传输，图片格式为png)")
    private String staffPic;

    /**
    * 临床工作年限
    */
    @ApiModelProperty("临床工作年限")
    private Integer workYears;

    /**
    * 民族(民族字典)
    */
    @ApiModelProperty("民族(民族字典)")
    private Integer nation;

    /**
    * 医疗人员所属的诊疗科目代码(科目字典(多个以统一分隔分隔))
    */
    @ApiModelProperty("医疗人员所属的诊疗科目代码(科目字典(多个以统一分隔分隔))")
    private String subjectCode;

    /**
    * 医疗人员所属的诊疗科目名(多个以统一分隔符分隔)
    */
    @ApiModelProperty("医疗人员所属的诊疗科目名(多个以统一分隔符分隔)")
    private String subjectName;

    /**
    * 医疗人员所属 科室编码(多个以统一分隔符分隔)
    */
    @ApiModelProperty("医疗人员所属 科室编码(多个以统一分隔符分隔)")
    private String deptId;

    /**
    * 医疗人员所属的科室名称(多个以统一分隔符分隔)
    */
    @ApiModelProperty("医疗人员所属的科室名称(多个以统一分隔符分隔)")
    private String deptName;

    /**
    * 资格证书编码(人员为医生，药师时需要)
    */
    @ApiModelProperty("资格证书编码(人员为医生，药师时需要)")
    private String qualificationCode;

    /**
    * 执业证书编码(人员为医生，护士时需要)
    */
    @ApiModelProperty("执业证书编码(人员为医生，护士时需要)")
    private String practiceCode;

    /**
    * 第一执业机构编码
    */
    @ApiModelProperty("第一执业机构编码")
    private String firstPractisingOrgCode;

    /**
    * 第一执业机构名称
    */
    @ApiModelProperty("第一执业机构名称")
    private String firstPractisingOrgName;

    /**
    * 执业证书发证日期(药师，护士为资格证书)
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("执业证书发证日期(药师，护士为资格证书)")
    private Date practiceDate;

    /**
    * 医疗人员手机号
    */
    @ApiModelProperty("医疗人员手机号")
    private String mobile;

    /**
    * 医疗人员职称id(详见职称字典)
    */
    @ApiModelProperty("医疗人员职称id(详见职称字典)")
    private String titleId;

    /**
    * 互联网医疗人员医疗责任险保单号
    */
    @ApiModelProperty("互联网医疗人员医疗责任险保单号")
    private String policyNo;

    /**
    * 医疗责任险生效日期
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("医疗责任险生效日期")
    private Date policyStartDate;

    /**
    * 医疗责任险到期日期
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("医疗责任险到期日期")
    private Date policyEndDate;

    /**
    * 医疗人员职称资格证编号
    */
    @ApiModelProperty("医疗人员职称资格证编号")
    private String titleCode;

    /**
    * 医疗人员执业类别(1:医生，2:护士，3:药师)
    */
    @ApiModelProperty("医疗人员执业类别(1:医生，2:护士，3:药师)")
    private Integer practiceType;

    /**
    * 是否备案(0:不备案或取消备案，1:备案，未备案或取消备案将竞争开展服务)
    */
    @ApiModelProperty("是否备案(0:不备案或取消备案，1:备案，未备案或取消备案将竞争开展服务)")
    private Integer filingMark;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public FilingStaffDO() {}
}
