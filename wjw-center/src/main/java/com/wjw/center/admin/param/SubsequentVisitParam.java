package com.wjw.center.admin.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description subsequent_visit
 * @author psk
 * @date 2022-09-19
 */
@Data
@ApiModel("诊疗管理:互联网在线复诊信息")
public class SubsequentVisitParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 主键
    */
    @ApiModelProperty("主键")
    private Long id;

    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 复诊id
    */
    @ApiModelProperty("复诊id")
    private String subsequentVisitId;

    /**
    * 诊疗科目代码(科目字典)
    */
    @ApiModelProperty("诊疗科目代码(科目字典)")
    private String diagnosisSubjectCode;

    /**
    * 诊疗科目名称
    */
    @ApiModelProperty("诊疗科目名称")
    private String diagnosisSubjectName;

    /**
    * 复诊药师科目编码(科目字典)
    */
    @ApiModelProperty("复诊药师科目编码(科目字典)")
    private String subjectCode;

    /**
    * 复诊药师科目名称
    */
    @ApiModelProperty("复诊药师科目名称")
    private String subjectName;

    /**
    * 复诊药师科室id
    */
    @ApiModelProperty("复诊药师科室id")
    private String deptId;

    /**
    * 复诊药师姓名
    */
    @ApiModelProperty("复诊药师姓名")
    private String deptName;

    /**
    * 复诊药师证件类型(证件类型字典)
    */
    @ApiModelProperty("复诊药师证件类型(证件类型字典)")
    private Integer docCertType;

    /**
    * 复诊药师证件号
    */
    @ApiModelProperty("复诊药师证件号")
    private String docCertNo;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    @ApiModelProperty("就诊人证件类型(证件类型字典)")
    private Integer patientCertType;

    /**
    * 就诊人证件号
    */
    @ApiModelProperty("就诊人证件号")
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人姓名
    */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人年龄
    */
    @ApiModelProperty("就诊人年龄")
    private Integer patientAge;

    /**
    * 就诊人性别(性别字典)
    */
    @ApiModelProperty("就诊人性别(性别字典)")
    private Integer patientSex;

    /**
    * 就诊人手机号
    */
    @ApiModelProperty("就诊人手机号")
    private String patientMobile;

    /**
    * 监护人姓名(儿童小于6岁需要)
    */
    @ApiModelProperty("监护人姓名(儿童小于6岁需要)")
    private String guardianIdName;

    /**
    * 证件类型字典(证件类型字典)
    */
    @ApiModelProperty("证件类型字典(证件类型字典)")
    private Integer guardianCertType;

    /**
    * 监护人证件号(儿童小于6岁需要)
    */
    @ApiModelProperty("监护人证件号(儿童小于6岁需要)")
    private String guardianCertNo;

    /**
    * 监护人手机(儿童小于6岁需要)
    */
    @ApiModelProperty("监护人手机(儿童小于6岁需要)")
    private String guardianMobile;

    /**
    * 复诊类型(1:图文交流，2:语音交流，3:视频交流，9:其他)
    */
    @ApiModelProperty("复诊类型(1:图文交流，2:语音交流，3:视频交流，9:其他)")
    private Integer subsequentVisitType;

    /**
    * 卡类型(1:医院就诊卡，2:医保卡，3:医院病历号(门 诊号)
    */
    @ApiModelProperty("卡类型(1:医院就诊卡，2:医保卡，3:医院病历号(门 诊号)")
    private Integer cardType;

    /**
    * 卡号
    */
    @ApiModelProperty("卡号")
    private String cardNo;

    /**
    * 就诊人简要病史描述
    */
    @ApiModelProperty("就诊人简要病史描述")
    private String diseasesHistory;

    /**
    * 就诊人诊断(复诊就诊人在实体医院的诊断名称，如有多条，使用统一分隔符进行分隔)
    */
    @ApiModelProperty("就诊人诊断(复诊就诊人在实体医院的诊断名称，如有多条，使用统一分隔符进行分隔)")
    private String originalDiagnosis;

    /**
    * 就诊人复诊证明标识(证明材料文件唯一标识(互联网医院需提供证明标识查询复诊证明文件接口))
    */
    @ApiModelProperty("就诊人复诊证明标识(证明材料文件唯一标识(互联网医院需提供证明标识查询复诊证明文件接口))")
    private String proveMark;

    /**
    * 在线复诊申请时间
    */
    @ApiModelProperty("在线复诊申请时间")
    private Date applyDate;

    /**
    * 复诊开始时间
    */
    @ApiModelProperty("复诊开始时间")
    private Date startDate;

    /**
    * 复诊结束时间
    */
    @ApiModelProperty("复诊结束时间")
    private Date endDate;

    /**
    * 本次诊断
    */
    @ApiModelProperty("本次诊断")
    private String thisDiagnosis;

    /**
    * 未确诊原因
    */
    @ApiModelProperty("未确诊原因")
    private String undiagnosedCause;

    /**
    * 是否回复(0:未回复，1:回复)
    */
    @ApiModelProperty("是否回复(0:未回复，1:回复)")
    private Integer answerMark;

    /**
    * 复诊拒绝/取消 时间
    */
    @ApiModelProperty("复诊拒绝/取消 时间")
    private Date refuseTime;

    /**
    * 复诊拒绝/取消 原因
    */
    @ApiModelProperty("复诊拒绝/取消 原因")
    private String refuseReason;

    /**
    * 复诊拒绝类别(1:医生主动拒绝，2:超时 未回复系统自动拒绝)
    */
    @ApiModelProperty("复诊拒绝类别(1:医生主动拒绝，2:超时 未回复系统自动拒绝)")
    private Integer refuseType;

    /**
    * 在线复诊全程留痕记录唯一标识(根据标识可查询对应沟通记录，包含图文，语音，视频记录(互联网医院需要提供根据记录标识查询记录接口)
    */
    @ApiModelProperty("在线复诊全程留痕记录唯一标识(根据标识可查询对应沟通记录，包含图文，语音，视频记录(互联网医院需要提供根据记录标识查询记录接口)")
    private String recordsMark;

    public SubsequentVisitParam() {}
}
