package com.wjw.center.admin.dictionaries;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @description dictionaries_org_rank
 * @author psk
 * @date 2022-09-27
 */

@ColumnWidth(30)//注释在具体属性上,设置单独列。注释在类上,统一设置列宽
@HeadRowHeight(30)//设置表头行高
@ContentRowHeight(20)//统一设置数据行行高
@ApiModel(value = "医师执业范围编码表", description = "")
@Data
@TableName("tst_dictionaries_practice_area")
@EqualsAndHashCode(callSuper = false)
public class DictionariesPracticeArea implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 代码
    */
    @ExcelProperty(value = "代码",index = 0)
    @ApiModelProperty("代码")
    private String code;

    /**
    * 名称
    */
    @ExcelProperty(value = "专业名称(type_code: PRACTICE_AREA)",index = 1)
    @ApiModelProperty("专业名称")
    private String name;

    public DictionariesPracticeArea() {}
}
