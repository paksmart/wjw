package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description prescription_comment
 * @author psk
 * @date 2022-09-28
 */
@Data
@TableName("tst_prescription_comment")
@ApiModel("诊疗管理:互联网在线处方点评监管")
public class PrescriptionCommentDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 处方点评id
    */
    @ApiModelProperty("处方点评id")
    private Long commentId;

    /**
    * 处方对应复诊id
    */
    @ApiModelProperty("处方对应复诊id")
    private Long visitId;

    /**
    * 处方唯一号
    */
    @ApiModelProperty("处方唯一号")
    private Long  prescriptionNo;

    /**
    * 开方医生姓名
    */
    @ApiModelProperty("开方医生姓名")
    private String docName;

    /**
    * 开方医生职称(祥见字典)
    */
    @ApiModelProperty("开方医生职称id(祥见字典)")
    private String docTitleId;

    /**
    * 处方所属科室代码
    */
    @ApiModelProperty("处方所属科室代码")
    private String deptId;

    /**
    * 处方所属科室名称
    */
    @ApiModelProperty("处方所属科室名称")
    private String deptName;

    /**
    * 诊断icd码(多个诊断，按统一分隔符分隔，icd字典)
    */
    @ApiModelProperty("诊断icd码(多个诊断，按统一分隔符分隔，icd字典)")
    private String icdCode;

    /**
    * 初步诊断名称(多个诊断，对应整顿码多个按统一分隔符分隔)
    */
    @ApiModelProperty("初步诊断名称(多个诊断，对应整顿码多个按统一分隔符分隔)")
    private String icdName;

    /**
    * 处方明细列表
    */
    @ApiModelProperty("处方明细列表")
    @TableField(exist = false)
    private List<PrescriptionPrescriptionDetailDO> prescriptionDetailList = new ArrayList<>();

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    @ApiModelProperty("就诊人证件类型(证件类型字典)")
    private Integer patientCertType;

    /**
    * 就诊人证件号
    */
    @ApiModelProperty("就诊人证件号")
    private String patientCertNo;

    /**
    * 就诊人姓名
    */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人年龄
    */
    @ApiModelProperty("就诊人年龄")
    private Integer patientAge;

    /**
    * 就诊人性别(性别字典)
    */
    @ApiModelProperty("就诊人性别(性别字典)")
    private Integer patientSex;

    /**
    * 就诊人手机号
    */
    @ApiModelProperty("就诊人手机号")
    private String patientMobile;

    /**
    * 监护人证件类型(证件类型字典)
    */
    @ApiModelProperty("监护人证件类型(证件类型字典)")
    private Integer guardianCartType;

    /**
    * 监护人身份证
    */
    @ApiModelProperty("监护人身份证")
    private String guardianCartNo;

    /**
    * 监护人手机号
    */
    @ApiModelProperty("监护人手机号")
    private String guardianMobile;

    /**
    * 处方点评结果状态(0:不合格，1:合格)
    */
    @ApiModelProperty("处方点评结果状态(0:不合格，1:合格)")
    private Integer commentStatus;

    /**
    * 点评备注
    */
    @ApiModelProperty("点评备注")
    private String revalNotes;

    /**
    * 点评药师
    */
    @ApiModelProperty("点评药师")
    private String pharName;

    /**
    * 点评时间(yyyy-mm-dd hh:mm:ss)
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("点评时间(yyyy-mm-dd hh:mm:ss)")
    private Date commentTime;

    /**
    * 抗菌药数量(汇总的单抗菌药数量)
    */
    @ApiModelProperty("抗菌药数量(汇总的单抗菌药数量)")
    private Integer antibacterialDrugNum;

    /**
    * 激素药数量(汇总的激素药数量)
    */
    @ApiModelProperty("激素药数量(汇总的激素药数量)")
    private Integer hormoneNum;

    /**
    * 注射剂数量(汇总的注射剂数量)
    */
    @ApiModelProperty("注射剂数量(汇总的注射剂数量)")
    private Integer injectionNum;

    /**
    * 国家基础药品品种数(汇总的国家基础药品品种数)
    */
    @ApiModelProperty("国家基础药品品种数(汇总的国家基础药品品种数)")
    private Integer baseDrugNum;

    /**
    * 药品通用名数(汇总的通用名数量)
    */
    @ApiModelProperty("药品通用名数(汇总的通用名数量)")
    private Integer commonNameNum;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public PrescriptionCommentDO() {}
}
