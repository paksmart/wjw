package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description prescription_prescription_detail
 * @author psk
 * @date 2022-09-28
 */
@Data
@TableName("tst_prescription_prescription_detail")
@ApiModel("诊疗管理:互联网在线处方信息(处方明细表)")
public class PrescriptionPrescriptionDetailDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * id
    */
    @ApiModelProperty("id")
    private Long id;

    /**
    * 药品代码(药品信息对应的ypid，参考国家药品标准(没有传 0000 ))
    */
    @ApiModelProperty("药品代码(药品信息对应的ypid，参考国家药品标准(没有传 0000 ))")
    private String drugCode;

    /**
    * 医院药品代码(医院对应药品代码)
    */
    @ApiModelProperty("医院药品代码(医院对应药品代码)")
    private String hospDrugCode;

    /**
    * 药品名称
    */
    @ApiModelProperty("药品名称")
    private String drugName;

    /**
    * 药品规格
    */
    @ApiModelProperty("药品规格")
    private String drugSpec;

    /**
    * 药品包装
    */
    @ApiModelProperty("药品包装")
    private String drugPack;

    /**
    * 药品包装单位
    */
    @ApiModelProperty("药品包装单位")
    private String drugPackUnit;

    /**
    * 药品产地
    */
    @ApiModelProperty("药品产地")
    private String drugPlace;

    /**
    * 药品用法
    */
    @ApiModelProperty("药品用法")
    private String drugUsage;

    /**
    * 药品使用频次
    */
    @ApiModelProperty("药品使用频次")
    private String drugFrequency;

    /**
    * 药品剂量
    */
    @ApiModelProperty("药品剂量")
    private String drugDose;

    /**
    * 药品批次
    */
    @ApiModelProperty("药品批次")
    private String drugBatch;

    /**
    * 药品总量
    */
    @ApiModelProperty("药品总量")
    private String drugTotalDose;

    /**
    * 药品总量单位
    */
    @ApiModelProperty("药品总量单位")
    private String drugTotalDoseUnit;

    /**
    * 药品使用天数
    */
    @ApiModelProperty("药品使用天数")
    private Integer drugUseDays;

    /**
    * 是否otc(0:不是，1:是)
    */
    @ApiModelProperty("是否otc(0:不是，1:是)")
    private Integer otcMark;

    /**
    * 药品备注
    */
    @ApiModelProperty("药品备注")
    private String remark;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 删除(0:未删除，1:已删除)
    */
    @ApiModelProperty("删除(0:未删除，1:已删除)")
    private Integer isDeleted;

    public PrescriptionPrescriptionDetailDO() {}
}
