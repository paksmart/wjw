package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @description remote_clinic_record
 * @author psk
 * @date 2022-09-20
 */
@Data
@TableName("tst_remote_clinic_record")
public class RemoteClinicRecordDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 门诊记录id
    */
    private Long clinicRecordId;

    /**
    * 门诊类型(1:普通门诊，2:紧急门诊)
    */
    private Integer recordType;

    /**
    * 预约序号
    */
    private String orderNum;

    /**
    * 业务记录状态(0:预约成功，1:挂号成功，2:已取消，3:已爽约，4:待支付，5:已支付，6:已退款，7:退款失败，8:退款中，9预约挂号中，10:应诊，11:确认中，12:已就诊，13:已过期，14:已完成)
    */
    private Integer status;

    /**
    * 是否医保(0:不是，1:是)
    */
    private Integer medicalInsuranceMark;

    /**
    * 申请机构编码
    */
    private String applyOrgCode;

    /**
    * 申请机构名称
    */
    private String applyOrgName;

    /**
    * 申请机构医生id
    */
    private Long applyDocId;

    /**
    * 申请机构医生姓名
    */
    private String applyDocName;

    /**
    * 申请机构医生手机号
    */
    private String applyDocIdMobile;

    /**
    * 申请医生证件类型(证件类型字典)
    */
    private Integer applyDocCertType;

    /**
    * 申请机构医生证件号
    */
    private String applyDocCertNo;

    /**
    * 申请机构医生职称(职称字典)
    */
    private String applyDocTitle;

    /**
    * 申请医生所在科目编码(科目字典)
    */
    private String applyDoctorSubjectCode;

    /**
    * 申请医生所在科目名称
    */
    private String applyDoctorSubjectName;

    /**
    * 申请医生所在科室编码
    */
    private String applyDeptId;

    /**
    * 申请医生所在科室名称
    */
    private String applyDeptName;

    /**
    * 接收申请机构医生id
    */
    private Long appliedDocId;

    /**
    * 接收申请机构医生姓名
    */
    private String appliedDocName;

    /**
    * 接收申请机构医生手机号
    */
    private String appliedDocIdMobile;

    /**
    * 接收申请机构医生身份证
    */
    private String appliedDocCertNo;

    /**
    * 接收申请机构医生职称(职称字典)
    */
    private String appliedDocTitle;

    /**
    * 接收申请医生所在科目编码
    */
    private String appliedDoctorSubjectCode;

    /**
    * 接收申请医生所在科目名称
    */
    private String appliedDoctorSubjectName;

    /**
    * 接收申请机构唯一序号
    */
    private String appliedOrgCode;

    /**
    * 接收申请机构名称
    */
    private String appliedOrgName;

    /**
    * 接收申请医生所在科室编码
    */
    private String appliedDeptId;

    /**
    * 接收申请医生所在科室
    */
    private String appliedDeptName;

    /**
    * 申请时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date applyDate;

    /**
    * 门诊开始时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    /**
    * 门诊结束时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    /**
    * 门诊价格(保留两位数)
    */
    private Double clinicPrice;

    /**
    * 是否支付(0:未支付，1:已支付，3:已退款)
    */
    private Integer payStatus;

    /**
    * 诊疗信息
    */
    private String diagnosisInfo;

    /**
    * 申请医生结论
    */
    private String applySummary;

    /**
    * 接收申请医生结论
    */
    private String appliedSummary;

    /**
    * 支付流水号
    */
    private String paySerialNo;

    /**
    * 取消描述(取消时必填)
    */
    private String cancelDesc;

    /**
    * 就诊人id
    */
    private Long patientId;

    /**
    * 就诊人姓名
    */
    private String patientName;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    private Integer patientCertType;

    /**
    * 就诊人证件号
    */
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    private String patientHealthCard;

    /**
    * 就诊人年龄
    */
    private Integer patientAge;

    /**
    * 就诊人生日(yyyy-mm-dd)
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date  patientBirthday;

    /**
    * 就诊人性别(性别字典)
    */
    private Integer patientSex;

    /**
    * 就诊人手机号
    */
    private String patientMobile;

    /**
    * 远程诊疗全程留痕记录标识(根据标识可查询对应沟通记录，包含图文，语音，视频记录(互联网医院提供根据记录标识查询记录接口)
    */
    private Long recordsMark;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
    * 创建人
    */
    private Long createUser;

    /**
    * 修改人
    */
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    private Integer isDeleted;

    public RemoteClinicRecordDO() {}
}
