package com.wjw.center.admin.dictionaries;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
* @description: psk
* @author pansh
* @date 2022/9/22 13:56
* @version 1.0
*/


@ColumnWidth(30)//注释在具体属性上,设置单独列。注释在类上,统一设置列宽
@HeadRowHeight(30)//设置表头行高
@ContentRowHeight(20)//统一设置数据行行高
@ApiModel(value = "地区字典", description = "")
@Data
@TableName("tst_dictionaries_address")
@EqualsAndHashCode(callSuper = false)
public class DictionariesRegion {
    /**
     * 编码
     */
    @ExcelProperty(value = "编码",index = 0)
    @ApiModelProperty("编码")
    private String code;

    /**
     * 名称
     */
    @ExcelProperty(value = "名称（type_code：AREA）",index = 1)
    @ApiModelProperty("名称")
    private String name;

    /**
     * 父级id
     */
    @ExcelProperty(value = "父级ID",index = 2)
    @ApiModelProperty("父级ID")
    private String pid;

//    @ExcelProperty(value = "创建时间", index = 3)
//    @TableField(fill = FieldFill.INSERT)
//    @ApiModelProperty(value = "创建时间")
//    private Date createTime;
//
//    @ExcelProperty(value = "最后修改时间", index = 4)
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    @ApiModelProperty(value = "最后修改时间")
//    private Date updateTime;
//
//
//    /**
//     * 逻辑删除（0 未删除、1 删除）
//     */
//    @ExcelProperty(value = "逻辑删除", index = 5)
//    @TableField(fill = FieldFill.INSERT)
//    @ApiModelProperty(value = "逻辑删除（0 未删除、1 删除）")
//    private Integer deleteFlag;
//
//    @ExcelProperty(value = "最后修改时间", index = 6)
//    @Version
//    @TableField(fill = FieldFill.INSERT)
//    @ApiModelProperty(value = "版本号（用于乐观锁， 默认为 1）")
//    private Integer version;
}
