package com.wjw.center.admin.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description imaging_diagnosis
 * @author psk
 * @date 2022-09-28
 */
@Data
@ApiModel("诊疗管理:互联网医院远程影像诊断记录监管")
public class ImagingDiagnosisParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 远程影像诊断id
    */
    @ApiModelProperty("远程影像诊断id")
    private String imagingDiagnosisId;

    /**
    * 就诊人简要病史描述
    */
    @ApiModelProperty("就诊人简要病史描述")
    private String diseasesHistory;

    /**
    * 初步诊断编码(会诊前初步诊断编码，如有多条，使用统一分隔符分隔，icd编码)
    */
    @ApiModelProperty("初步诊断编码(会诊前初步诊断编码，如有多条，使用统一分隔符分隔，icd编码)")
    private String diagianCode;

    /**
    * 初步诊断名称(会诊前初步诊断名称，如有多条，使用统一分隔符分隔)
    */
    @ApiModelProperty("初步诊断名称(会诊前初步诊断名称，如有多条，使用统一分隔符分隔)")
    private String diagianName;

    /**
    * 会诊目的(申请医生就诊人目前存在问题提出诊断要达到的目的)
    */
    @ApiModelProperty("会诊目的(申请医生就诊人目前存在问题提出诊断要达到的目的)")
    private String purpose;

    /**
    * 就诊人病历描述
    */
    @ApiModelProperty("就诊人病历描述")
    private String patientCaseDesc;

    /**
    * 影像诊断检查类别代码(x，ct，mri，dsa，b，详见影像诊断检查类别字典)
    */
    @ApiModelProperty("影像诊断检查类别代码(x，ct，mri，dsa，b，详见影像诊断检查类别字典)")
    private String imagingType;

    /**
    * 影像诊断检查类别名称
    */
    @ApiModelProperty("影像诊断检查类别名称")
    private String imagingName;

    /**
    * 检查身体部位
    */
    @ApiModelProperty("检查身体部位")
    private String bodyParts;

    /**
    * 诊断申请时间
    */
    @ApiModelProperty("诊断申请时间")
    private Date applyDate;

    /**
    * 诊断报告时间
    */
    @ApiModelProperty("诊断报告时间")
    private Date reportDate;

    /**
    * 影像描述
    */
    @ApiModelProperty("影像描述")
    private String imagingDesc;

    /**
    * 影像诊断意见
    */
    @ApiModelProperty("影像诊断意见")
    private String imagingOpinion;

    /**
    * 是否支付(0:未支付，1:已支付，2:已退款)
    */
    @ApiModelProperty("是否支付(0:未支付，1:已支付，2:已退款)")
    private Integer payStatus;

    /**
    * 支付方式(1:支付宝，2:微信，3:银联，9:其他)
    */
    @ApiModelProperty("支付方式(1:支付宝，2:微信，3:银联，9:其他)")
    private Integer payMode;

    /**
    * 影像诊断价格(保留两位小数)
    */
    @ApiModelProperty("影像诊断价格(保留两位小数)")
    private Double imagingPrice;

    /**
    * 影像dicom文件下载地址
    */
    @ApiModelProperty("影像dicom文件下载地址")
    private String fileUrl;

    /**
    * 申请机构名称
    */
    @ApiModelProperty("申请机构名称")
    private String applyOrgName;

    /**
    * 申请机构编码
    */
    @ApiModelProperty("申请机构编码")
    private String applyOrgCode;

    /**
    * 申请机构科目编码(科目字典)
    */
    @ApiModelProperty("申请机构科目编码(科目字典)")
    private String applySubjectCode;

    /**
    * 申请机构科目名称
    */
    @ApiModelProperty("申请机构科目名称")
    private String applySubjectName;

    /**
    * 申请机构科室编码
    */
    @ApiModelProperty("申请机构科室编码")
    private String applyDeptId;

    /**
    * 申请机构科室名称
    */
    @ApiModelProperty("申请机构科室名称")
    private String applyDeptName;

    /**
    * 申请医生id
    */
    @ApiModelProperty("申请医生id")
    private String applyDocId;

    /**
    * 申请医生姓名
    */
    @ApiModelProperty("申请医生姓名")
    private String applyDocName;

    /**
    * 申请医生手机号
    */
    @ApiModelProperty("申请医生手机号")
    private String applyDocMobile;

    /**
    * 申请医生证件类型(证件类型字典)
    */
    @ApiModelProperty("申请医生证件类型(证件类型字典)")
    private Integer applyDocCertType;

    /**
    * 申请医生证件号
    */
    @ApiModelProperty("申请医生证件号")
    private String applyDocCertNo;

    /**
    * 申请医生职称(职称字典)
    */
    @ApiModelProperty("申请医生职称(职称字典)")
    private String applyDocTitle;

    /**
    * 接收申请列表
    */
    @ApiModelProperty("接收申请列表")
    private String appliedList;

    /**
    * 就诊人id
    */
    @ApiModelProperty("就诊人id")
    private String patientId;

    /**
    * 就诊人姓名
    */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    @ApiModelProperty("就诊人证件类型(证件类型字典)")
    private Integer patientCertType;

    /**
    * 就诊人身份证
    */
    @ApiModelProperty("就诊人身份证")
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人年龄
    */
    @ApiModelProperty("就诊人年龄")
    private Integer patientAge;

    /**
    * 就诊人性别(性别字典)
    */
    @ApiModelProperty("就诊人性别(性别字典)")
    private Integer patientSex;

    /**
    * 就诊人电话
    */
    @ApiModelProperty("就诊人电话")
    private String patientMobile;

    /**
    * 就诊人生日(yyyy-mm-dd)
    */
    @ApiModelProperty("就诊人生日(yyyy-mm-dd)")
    private Date patientBirthday;

    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public ImagingDiagnosisParam() {}
}
