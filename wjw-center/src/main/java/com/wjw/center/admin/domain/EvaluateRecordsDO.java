package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @description evaluate_records
 * @author psk
 * @date 2022-09-20
 */
@Data
@TableName("tst_evaluate_records")
@ApiModel("质量信息:互联网医院业务评价记录")
public class EvaluateRecordsDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 评价id
    */
    @ApiModelProperty("评价id")
    private Long evaluateId;

    /**
    * 被评价的业务类型(业务字典)
    */
    @ApiModelProperty("被评价的业务类型(业务字典)")
    private Integer businessType;

    /**
    * 被评价的业务id
    */
    @ApiModelProperty("被评价的业务id")
    private Long businessId;

    /**
    * 被评价人员科目代码(科目字典)
    */
    @ApiModelProperty("被评价人员科目代码(科目字典)")
    private String subjectCode;

    /**
    * 被评价人员科目名称
    */
    @ApiModelProperty("被评价人员科目名称")
    private String subjectName;

    /**
    * 被评价人员科室代码
    */
    @ApiModelProperty("被评价人员科室代码")
    private String deptId;

    /**
    * 被评价人员科室名称
    */
    @ApiModelProperty("被评价人员科室名称")
    private String deptName;

    /**
    * 医疗人员类型(1:医生，2:药师，3:护士)
    */
    @ApiModelProperty("医疗人员类型(1:医生，2:药师，3:护士)")
    private Integer staffType;

    /**
    * 医疗人员id(医生，药师，护士在医 院系统的id)
    */
    @ApiModelProperty("医疗人员id(医生，药师，护士在医 院系统的id)")
    private Long staffId;

    /**
    * 人员姓名
    */
    @ApiModelProperty("人员姓名")
    private String staffName;

    /**
    * 人员证件类型
    */
    @ApiModelProperty("人员证件类型")
    private Integer staffCertType;

    /**
    * 人员证件号
    */
    @ApiModelProperty("人员证件号")
    private String staffCertNo;

    /**
    * 人员手机号
    */
    @ApiModelProperty("人员手机号")
    private String staffMobile;

    /**
    * 满意度(1:非常不满意，2:不满意，3:一般，4:满意，5:非常满意)
    */
    @ApiModelProperty("满意度(1:非常不满意，2:不满意，3:一般，4:满意，5:非常满意)")
    private Integer satisfaction;

    /**
    * 评分(1-10)
    */
    @ApiModelProperty("评分(1-10)")
    private Integer score;

    /**
    * 评价内容
    */
    @ApiModelProperty("评价内容")
    private String content;

    /**
    * 评价人id
    */
    @ApiModelProperty("评价人id")
    private Long evaluatorId;

    /**
    * 评价人姓名
    */
    @ApiModelProperty("评价人姓名")
    private String evaluatorName;

    /**
    * 评价时间
    */
    @JsonFormat( timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("评价时间")
    private Date evaluateDate;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public EvaluateRecordsDO() {}
}
