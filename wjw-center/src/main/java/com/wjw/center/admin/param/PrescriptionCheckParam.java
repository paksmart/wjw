package com.wjw.center.admin.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description prescription_check
 * @author psk
 * @date 2022-09-19
 */
@Data
@ApiModel("诊疗管理:患者信息,就诊人基本信息")
public class PrescriptionCheckParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 处方唯一号
    */
    @ApiModelProperty("处方唯一号")
    private String prescriptionNo;

    /**
    * 药剂师id
    */
    @ApiModelProperty("药剂师id")
    private String pharId;

    /**
    * 药剂师姓名
    */
    @ApiModelProperty("药剂师姓名")
    private String pharName;


    /**
    * 药剂师证件类型
    */
    @ApiModelProperty("药剂师证件类型")
    private Integer pharCertType;

    /**
    * 药剂师证件号码
    */
    @ApiModelProperty("药剂师证件号码")
    private String pharCertNo;

    /**
    * 审核药师电子签名文件url地址(审核药师电子签名url地址)
    */
    @ApiModelProperty("审核药师电子签名文件url地址(审核药师电子签名url地址)")
    private String pharCaSignUrl;

    /**
    * 审核状态(0:未审核，1:审核通过，2:驳回)
    */
    @ApiModelProperty("审核状态(0:未审核，1:审核通过，2:驳回)")
    private Integer checkStatus;

    /**
    * 审核意见
    */
    @ApiModelProperty("审核意见")
    private String checkOpinion;

    /**
    * 复诊id
    */
    @ApiModelProperty("复诊id")
    private String subsequentVisitId;

    public PrescriptionCheckParam() {}
}
