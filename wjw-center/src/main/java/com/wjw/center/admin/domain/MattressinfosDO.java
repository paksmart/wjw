package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @description tst_mattressinfos
 * @author zhengkai.blog.csdn.net
 * @date 2022-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tst_mattressinfos")
public class MattressinfosDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @TableField("device_model_id")
    /**
    * 设备型号id
    */
    private Long deviceModelId;

    /**
    * 设备sn号
    */
    @TableField("dev_id")
    private String devId;

    /**
     * 访问秘钥
     */
    private String apiKey;

    /**
    * 返回状态(200:成功 600:key错误 601:参数错)
    */
    private Long code;

    /**
    * 返回信息
    */
    private String message;

    /**
    * 设备状态 1: 在线 2: 离线
    */
    private Integer deviceState;

    /**
    * 用户状态 1: 在床 2: 离床 3: 体动
    */
    private Integer userState;

    /**
    * 心率(仅在床状态有效)
    */
    private Long heartRate;

    /**
    * 呼吸率(仅在床状态有效)
    */
    private Long respiratoryRate;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 创建人
    */
    private Long createUser;

    /**
    * 修改人
    */
    private Long updateUser;

    /**
    * 是否删除 0: 否 1: 是
    */
    private Integer isDeleted;

    public MattressinfosDO() {}
}
