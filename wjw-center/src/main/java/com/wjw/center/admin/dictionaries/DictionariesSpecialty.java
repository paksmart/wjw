package com.wjw.center.admin.dictionaries;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* @description:
* @author pansh
* @date 2022/9/24 9:35
* @version 1.0
*/


@ColumnWidth(30)//注释在具体属性上,设置单独列。注释在类上,统一设置列宽
@HeadRowHeight(30)//设置表头行高
@ContentRowHeight(20)//统一设置数据行行高
@ApiModel(value = "国家诊疗科目字典(专业字典)", description = "")
@Data
@TableName("tst_dictionaries_specialty")
@EqualsAndHashCode(callSuper = false)
public class DictionariesSpecialty {

    /**
     * 编码
     */
    @ExcelProperty(value = "专科代码",index = 0)
    @ApiModelProperty("专科代码")
    private String code;

    /**
     * 名称
     */
    @ExcelProperty(value = "专科名称（type_code：SUBJECT）",index = 1)
    @ApiModelProperty("专科名称")
    private String name;
}
