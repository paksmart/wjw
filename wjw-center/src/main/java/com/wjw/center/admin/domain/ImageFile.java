package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("tst_image")
@Data
public class ImageFile {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private byte[] image;

}
