package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description patientinfo
 * @author psk
 * @date 2022-09-17
 */
@Data
@TableName("tst_patientinfo_patients")
public class patientinfoPatientsDo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 就诊人id
    */
    private Long patientId;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    private Integer patientCertType;

    /**
    * 就诊人证件号码
    */
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    private String patientHealthCard;

    /**
    * 就诊人姓名
    */
    private String patientName;

    /**
    * 就诊人手机号
    */
    private String patientMobile;

    /**
    * 就诊人是否实名认证(0:未实名，1:已实名)
    */
    private Integer realNameStatus;

    /**
    * 就诊人是否本人(0:非本人，1;本人)
    */
    private Integer selfMark;

    /**
    * 性别(性别字典)
    */
    private Integer patientSex;

    /**
    * 年龄(就诊人年龄)
    */
    private Integer patientAge;

    /**
    * 生日(yyyy-mm-dd)
    */
    private String patientBirthday;

    /**
    * 就诊人民族(民族字典)
    */
    private Integer patientNation;

    /**
    * 就诊人省代码
    */
    private Integer patientProvinceCode;

    /**
    * 就诊人市代码
    */
    private Integer patientCityCode;

    /**
    * 就诊人区代码
    */
    private Integer patientAreaCode;

    /**
    * 就诊人监护人证件类型
    */
    private Integer patientGuardianCertType;

    /**
    * 就诊人监护人证件号(儿童<6岁需要)
    */
    private String patientGuardianCertNo;

    /**
    * 就诊人监护人姓名(儿童<6岁需要)
    */
    private String patientGuardianName;

    /**
    * 就诊人状态(1:正常，2:删除)
    */
    private Integer patientStatus;

    /**
     * 更新时间(yyyy-mm-dd hh:mm:ss)
     */
    private Date updateTime;

    public patientinfoPatientsDo() {}
}
