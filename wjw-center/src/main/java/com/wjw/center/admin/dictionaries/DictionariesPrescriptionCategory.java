package com.wjw.center.admin.dictionaries;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @description dictionaries_org_rank
 * @author psk
 * @date 2022-09-27
 */

@ColumnWidth(30)//注释在具体属性上,设置单独列。注释在类上,统一设置列宽
@HeadRowHeight(30)//设置表头行高
@ContentRowHeight(20)//统一设置数据行行高
@ApiModel(value = "处方分类代码表", description = "")
@Data
@TableName("tst_dictionaries_prescription_category")
@EqualsAndHashCode(callSuper = false)
public class DictionariesPrescriptionCategory implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 编码
    */
    @ExcelProperty(value = "编码",index = 0)
    @ApiModelProperty("编码")
    private String code;

    /**
    * 名称
    */
    @ExcelProperty(value = "名称(type_code: PRESCRIPTION_CATEGORY)",index = 1)
    @ApiModelProperty("名称")
    private String name;

    public DictionariesPrescriptionCategory() {}
}
