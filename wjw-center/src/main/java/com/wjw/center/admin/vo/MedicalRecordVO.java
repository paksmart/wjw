package com.wjw.center.admin.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description medical_record
 * @author psk
 * @date 2022-09-17
 */
@Data
@ApiModel("诊疗管理:患者电子病例信息")
public class MedicalRecordVO implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 平台机构id
    */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 书写医师编码(医生在互联网医院的编码)
    */
    @ApiModelProperty("书写医师编码(医生在互联网医院的编码)")
    private String recordDocCode;

    /**
    * 书写医师姓名
    */
    @ApiModelProperty("书写医师姓名")
    private String recordDocName;

    /**
    * 就诊人id
    */
    @ApiModelProperty("就诊人id")
    private Long patientId;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    @ApiModelProperty("就诊人证件类型(证件类型字典)")
    private Integer patientCertType;

    /**
    * 就诊人证件号
    */
    @ApiModelProperty("就诊人证件号")
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人姓名
    */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人年龄
    */
    @ApiModelProperty("就诊人年龄")
    private Integer patientAge;

    /**
    * 就诊人性别
    */
    @ApiModelProperty("就诊人性别")
    private Integer patientSex;

    /**
    * 就诊人手机号
    */
    @ApiModelProperty("就诊人手机号")
    private String patientMobile;

    /**
    * 就诊业务唯一标识(对应的业务标识(如为复诊业务的病历时，需可根据此标识查询到对应到复诊记录)
    */
    @ApiModelProperty("就诊业务唯一标识(对应的业务标识(如为复诊业务的病历时，需可根据此标识查询到对应到复诊记录)")
    private String visitSerialNo;

    /**
    * 病历编号(病历唯一识别)
    */
    @ApiModelProperty("病历编号(病历唯一识别)")
    private String caseNo;

    /**
    * 病历对应业务类型(业务字典(默认30，复诊)
    */
    @ApiModelProperty("病历对应业务类型(业务字典(默认30，复诊)")
    private Integer caseType;

    /**
    * 疾病编码(疾病字典(多个用统一分隔符隔开))
    */
    @ApiModelProperty("疾病编码(疾病字典(多个用统一分隔符隔开))")
    private String diseaseCode;

    /**
    * 疾病名称(疾病字典(多个用统一分隔符隔开))
    */
    @ApiModelProperty("疾病名称(疾病字典(多个用统一分隔符隔开))")
    private String diseaseName;

    /**
    * 病例概要
    */
    @ApiModelProperty("病例概要")
    private String caseSummary;

    /**
    * 主诉
    */
    @ApiModelProperty("主诉")
    private String chiefComplaint;

    /**
    * 初步诊断
    */
    @ApiModelProperty("初步诊断")
    private String preliminaryDiagnosis;

    /**
    * 诊断意见
    */
    @ApiModelProperty("诊断意见")
    private String diagnosticOpinion;

    /**
    * 现病史
    */
    @ApiModelProperty("现病史")
    private String hpi;

    /**
    * 既往病史
    */
    @ApiModelProperty("既往病史")
    private String anamnesis;

    /**
    * 家族史
    */
    @ApiModelProperty("家族史")
    private String familyHistory;

    /**
    * 体格检查
    */
    @ApiModelProperty("体格检查")
    private String physicalExamination;

    /**
    * 辅助检查
    */
    @ApiModelProperty("辅助检查")
    private String supplementaryExamination;

    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public MedicalRecordVO() {}
}
