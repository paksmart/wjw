package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description patientinfo
 * @author psk
 * @date 2022-09-17
 */
@Data
@TableName("tst_patientinfo")
public class PatientinfoDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 平台机构id
     */
    @TableId(type = IdType.AUTO)
    private Long platformId;

    /**
    * 用户id
    */
    private Long userId;

    /**
    * 用户姓名
    */
    private String userName;

    /**
    * 手机号
    */
    private String mobile;

    /**
    * 用户证件类型(证件类型字典)
    */
    private Integer certType;

    /**
    * 用户证件号(新生儿童证件号为母亲证件号胎数，如34***1201，01为胎数)
    */
    private String certNo;

    /**
    * 年龄(岁)
    */
    private Integer age;

    /**
    * 性别
    */
    private Integer sex;

    /**
    * 出生年月(yyyy-mm-dd)
    */
    private Date birthday;

    /**
    * 民族
    */
    private Integer nation;

    /**
    * 省代码(地区字典)
    */
    private Integer provinceCode;

    /**
    * 市代码(地区字典)
    */
    private Integer cityCode;

    /**
    * 区代码(地区字典)
    */
    private Integer areaCode;

    /**
    * 地址
    */
    private String address;

    /**
    * 监管人证件类型(证件类型字典)
    */
    private Integer guardianCardType;

    /**
    * 监护人证件号(年龄小于6需要监护人)
    */
    private String guardianCardNo;

    /**
    * 监护人姓名(年龄小于6需要监护人)
    */
    private String guardianName;

    /**
     * 用户就诊人信息(如没就诊人，默认用户本身为就诊人)
     */
    @TableField(exist = false)
    private List<patientinfoPatientsDo> patients = new ArrayList<>();

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
    * 创建人
    */
    private Long createUser;

    /**
    * 修改人
    */
    private Long updateUser;

    /**
     * 是否删除(0:否，1:是)
     */
    @TableLogic
    private Integer isDeleted;

    public PatientinfoDO() {}
}
