package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description imaging_diagnosis_applied_list
 * @author psk
 * @date 2022-09-26
 */
@Data
@TableName("tst_imaging_diagnosis_applied_list")
public class ImagingDiagnosisAppliedListDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 平台机构id
     */
    @ApiModelProperty("平台机构主键")
    private Long id;

    /**
    * 接收申请机构编码
    */
    private String appliedOrgCode;

    /**
    * 接收申请机构名称
    */
    private String appliedOrgName;

    /**
    * 接收申请医生id
    */
    private String appliedDocId;

    /**
    * 接收申请医生姓名
    */
    private String appliedDocName;

    /**
    * 接收申请医生科目编码(科目字典)
    */
    private String appliedSubjectCode;

    /**
    * 接收申请医生科目名称
    */
    private String appliedSubjectName;

    /**
    * 接收申请医生科室编码
    */
    private String appliedDeptId;

    /**
    * 接收申请医生科室名称
    */
    private String appliedDeptName;

    /**
    * 接收申请医生证件类型(证件类型字典)
    */
    private Integer appliedDocCertType;

    /**
    * 接收申请医生证件号
    */
    private String appliedDoCertNo;

    /**
    * 接收申请医生手机号
    */
    private String appliedDocMobile;

    /**
    * 接收申请医生职称(职称字典)
    */
    private String appliedDocTitle;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
    * 创建人
    */
    private Long createUser;

    /**
    * 修改人
    */
    private Long updateUser;

    /**
    * 远程影像(接收申请列表
    */
    @TableLogic
    private Integer isDeleted;

    public ImagingDiagnosisAppliedListDO() {}
}
