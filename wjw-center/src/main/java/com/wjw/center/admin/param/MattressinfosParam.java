package com.wjw.center.admin.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description mattressinfos
 * @author zhengkai.blog.csdn.net
 * @date 2022-09-14
 */
@Data
@ApiModel("床垫:实时数据")
public class MattressinfosParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 设备型号id
    */
    @ApiModelProperty("设备型号id")
    private Long deviceModelId;

    /**
    * 设备sn号
    */
    @ApiModelProperty("设备sn号")
    private String devId;

    /**
     * 访问秘钥
     */
    @ApiModelProperty("访问秘钥")
    private String apiKey;

    /**
    * 返回状态(200:成功 600:key错误 601:参数错)
    */
    @ApiModelProperty("返回状态(200:成功 600:key错误 601:参数错)")
    private Long code;

    /**
    * 返回信息
    */
    @ApiModelProperty("返回信息")
    private String message;

    /**
    * 设备状态 1: 在线 2: 离线
    */
    @ApiModelProperty("设备状态 1: 在线 2: 离线")
    private Integer deviceState;

    /**
    * 用户状态 1: 在床 2: 离床 3: 体动
    */
    @ApiModelProperty("用户状态 1: 在床 2: 离床 3: 体动")
    private Integer userState;

    /**
    * 心率(仅在床状态有效)
    */
    @ApiModelProperty("心率(仅在床状态有效)")
    private Long heartRate;

    /**
    * 呼吸率(仅在床状态有效)
    */
    @ApiModelProperty("呼吸率(仅在床状态有效)")
    private Long respiratoryRate;


    public MattressinfosParam() {}
}
