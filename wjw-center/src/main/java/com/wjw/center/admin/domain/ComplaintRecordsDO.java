package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description complaint_records
 * @author psk
 * @date 2022-09-21
 */
@Data
@TableName("tst_complaint_records")
@ApiModel("质量信息:互联网医院投诉建议记录")
public class ComplaintRecordsDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 平台机构id
    */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 投诉建议id
    */
    @ApiModelProperty("投诉建议id")
    private Long complaintId;

    /**
    * 投诉建议的业务类型(业务字典)
    */
    @ApiModelProperty("投诉建议的业务类型(业务字典)")
    private Integer complaintType;

    /**
    * 投诉建议内容
    */
    @ApiModelProperty("投诉建议内容")
    private String content;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @ApiModelProperty("是否删除(0:否，1:是)")
    @TableLogic
    private Integer isDeleted;

    public ComplaintRecordsDO() {}
}
