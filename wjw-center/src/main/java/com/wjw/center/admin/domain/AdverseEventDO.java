package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @description adverse_event
 * @author psk
 * @date 2022-09-20
 */
@Data
@Accessors(chain = true)
@TableName("tst_adverse_event")
@ApiModel("质量信息:互联网医院不良事件记录")
public class AdverseEventDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 平台机构id
    */
    @TableId(type = IdType.UUID)
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 事件记录号
    */
    @NotNull(message = "事件记录号不能为空")
    @ApiModelProperty("事件记录号")
    private Long eventId;

    /**
    * 事件业务类型(业务字典)
    */
    @ApiModelProperty("事件业务类型(业务字典)")
    private Integer eventType;

    /**
    * 就诊人ID
    */
    @ApiModelProperty("就诊人ID")
    private Long patientId;

    /**
     * 就诊人姓名
     */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人证件类型(证件类型字典)
    */
    @ApiModelProperty("就诊人证件类型(证件类型字典)")
    private Integer patientCertType;

    /**
    * 就诊人证件号
    */
    @ApiModelProperty("就诊人证件号")
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人手机号
    */
    @ApiModelProperty("就诊人手机号")
    private String patientMobile;

    /**
    * 上报科室id
    */
    @ApiModelProperty("上报科室id")
    private Long reportDeptId;

    /**
    * 上报科室名称
    */
    @ApiModelProperty("上报科室名称")
    private String reportDeptName;

    /**
    * 上报人
    */
    @ApiModelProperty("上报人")
    private String reportPerson;

    /**
    * 上报时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("上报时间")
    private Date reportDate;

    /**
    * 不良事件描述
    */
    @ApiModelProperty("不良事件描述")
    private String eventDesc;

    /**
    * 事件发生时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("事件发生时间")
    private Date eventDate;

    /**
    * 发生的主要原因
    */
    @ApiModelProperty("发生的主要原因")
    private String eventReason;

    /**
    * 采取的措施
    */
    @ApiModelProperty("采取的措施")
    private String takeSteps;

    /**
    * 造成的不良影响(对就诊人或医生)
    */
    @ApiModelProperty("造成的不良影响(对就诊人或医生)")
    private String adverseEffects;

    /**
    * 解决措施
    */
    @ApiModelProperty("解决措施")
    private String solutions;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @TableLogic
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public AdverseEventDO() {}
}
