package com.wjw.center.admin.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description prescription
 * @author psk
 * @date 2022-09-28
 */
@Data
@ApiModel("诊疗管理:互联网在线处方信息")
public class PrescriptionParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 主键
    */
    @ApiModelProperty("主键")
    private Long id;

    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 处方唯一号(用户对外查备和核销查询)
    */
    @ApiModelProperty("处方唯一号(用户对外查备和核销查询)")
    private String subsequentVisitId;

    /**
    * 处方来源(1:咨询，2:复诊，9:其他)
    */
    @ApiModelProperty("处方来源(1:咨询，2:复诊，9:其他)")
    private Integer prescriptionSource;

    /**
    * 上次就诊诊断名称(前一次就诊诊断(不限就诊场景(多个诊断按统一分隔符分隔))
    */
    @ApiModelProperty("上次就诊诊断名称(前一次就诊诊断(不限就诊场景(多个诊断按统一分隔符分隔))")
    private String originalDiagnosis;

    /**
    * 开方医师所属专业代码(科目字典)
    */
    @ApiModelProperty("开方医师所属专业代码(科目字典)")
    private String subjectCode;

    /**
    * 开方医师所属专业名称(诊疗科目名称)
    */
    @ApiModelProperty("开方医师所属专业名称(诊疗科目名称)")
    private String subjectName;

    /**
    * 医师所属科室代码
    */
    @ApiModelProperty("医师所属科室代码")
    private String deptId;

    /**
    * 医师所属科室名称
    */
    @ApiModelProperty("医师所属科室名称")
    private String deptName;

    /**
    * 医师id
    */
    @ApiModelProperty("医师id")
    private String docId;

    /**
    * 医师证件类型(证件类型字典)
    */
    @ApiModelProperty("医师证件类型(证件类型字典)")
    private Integer docCertType;

    /**
    * 医师证件号
    */
    @ApiModelProperty("医师证件号")
    private String docCertNo;

    /**
    * 医师姓名
    */
    @ApiModelProperty("医师姓名")
    private String docName;

    /**
    * 审方药师id
    */
    @ApiModelProperty("审方药师id")
    private String checkDocId;

    /**
    * 审方药师姓名
    */
    @ApiModelProperty("审方药师姓名")
    private String checkDocName;

    /**
    * 审方药师证件类型(证件类型字典)
    */
    @ApiModelProperty("审方药师证件类型(证件类型字典)")
    private Integer checkDocCertType;

    /**
    * 审方药师证件号
    */
    @ApiModelProperty("审方药师证件号")
    private String checkDocCertNo;

    /**
    * 审方药师审核时间
    */
    @ApiModelProperty("审方药师审核时间")
    private Date checkDate;

    /**
    * 就诊人身份证号
    */
    @ApiModelProperty("就诊人身份证号")
    private String patientCertNo;

    /**
    * 就诊人电子健康卡
    */
    @ApiModelProperty("就诊人电子健康卡")
    private String patientHealthCard;

    /**
    * 就诊人姓名
    */
    @ApiModelProperty("就诊人姓名")
    private String patientName;

    /**
    * 就诊人年龄
    */
    @ApiModelProperty("就诊人年龄")
    private Integer patientAge;

    /**
    * 监护人证件类型(证件类型字典)
    */
    @ApiModelProperty("监护人证件类型(证件类型字典)")
    private Integer guardianCertType;

    /**
    * 监护人证件号
    */
    @ApiModelProperty("监护人证件号")
    private String guardianCertNo;

    /**
    * 监护人姓名
    */
    @ApiModelProperty("监护人姓名")
    private String guardianName;

    /**
    * 监护人手机号
    */
    @ApiModelProperty("监护人手机号")
    private String guardianMobile;

    /**
    * 费别(1:自费，2:医保)
    */
    @ApiModelProperty("费别(1:自费，2:医保)")
    private Integer costType;

    /**
    * 卡类型(1:医院就诊卡，2:医保卡，3:医院病历号(门诊号)
    */
    @ApiModelProperty("卡类型(1:医院就诊卡，2:医保卡，3:医院病历号(门诊号)")
    private Integer cardType;

    /**
    * 卡号
    */
    @ApiModelProperty("卡号")
    private String cardNo;

    /**
    * 过敏信息
    */
    @ApiModelProperty("过敏信息")
    private String allergyInfo;

    /**
    * 病史摘要
    */
    @ApiModelProperty("病史摘要")
    private String diseasesHistory;

    /**
    * 处方状态(0:无效，1:有效)
    */
    @ApiModelProperty("处方状态(0:无效，1:有效)")
    private Integer prescriptionStatus;

    /**
    * 医保处方备案号(处方医保报销时对应的标识)
    */
    @ApiModelProperty("医保处方备案号(处方医保报销时对应的标识)")
    private String prescriptionRecordNo;

    /**
    * 是否经过合理用药(0:未经过，1:经过)
    */
    @ApiModelProperty("是否经过合理用药(0:未经过，1:经过)")
    private Integer rationalDrugMark;

    /**
    * 合理用药审核结果(rational_drug_mark=1 必填)
    */
    @ApiModelProperty("合理用药审核结果(rational_drug_mark=1 必填)")
    private String rationalDrugResult;

    /**
    * 医生处方ca签名，文件url(文件url，png/pdf格式)
    */
    @ApiModelProperty("医生处方ca签名，文件url(文件url，png/pdf格式)")
    private String docCaSignUrl;

    /**
    * 处方单url(处方筏文件文件地址，pdf格式)
    */
    @ApiModelProperty("处方单url(处方筏文件文件地址，pdf格式)")
    private String prescriptionNoteUrl;

    /**
    * 就诊人病历文件url(pdf格式)
    */
    @ApiModelProperty("就诊人病历文件url(pdf格式)")
    private String medicalRecordUrl;

    /**
    * 诊断icd码(多个诊断，按统一分隔符分隔，icd字典)
    */
    @ApiModelProperty("诊断icd码(多个诊断，按统一分隔符分隔，icd字典)")
    private String icdCode;

    /**
    * 初步诊断名称(多个诊断，对应诊断码多个按统一分隔符分隔)
    */
    @ApiModelProperty("初步诊断名称(多个诊断，对应诊断码多个按统一分隔符分隔)")
    private String icdName;

    /**
    * 处方类型(1:西药，2:中成药，3:中药，对于西药和中成药合在一起的处方，统一传 1)
    */
    @ApiModelProperty("处方类型(1:西药，2:中成药，3:中药，对于西药和中成药合在一起的处方，统一传 1)")
    private Integer prescriptionType;

    /**
    * 帖数(中药处方必填)
    */
    @ApiModelProperty("帖数(中药处方必填)")
    private Integer packetsNum;

    /**
    * 处方日期
    */
    @ApiModelProperty("处方日期")
    private Date prescriptionDate;

    /**
    * 处方有效天数
    */
    @ApiModelProperty("处方有效天数")
    private Integer effectiveDay;

    /**
    * 处方开始日期(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("处方开始日期(yyyy-mm-dd hh:mm:ss)")
    private Date startDate;

    /**
    * 处方结束日期(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("处方结束日期(yyyy-mm-dd hh:mm:ss)")
    private Date endDate;

    /**
    * 处方金额(单位元，保留两位小数)
    */
    @ApiModelProperty("处方金额(单位元，保留两位小数)")
    private Double totalFee;

    /**
    * 是否支付(0:未支付，1:已支付)
    */
    @ApiModelProperty("是否支付(0:未支付，1:已支付)")
    private Integer payMark;

    /**
    * 处方核销状态(0:未核销，1:已核销)
    */
    @ApiModelProperty("处方核销状态(0:未核销，1:已核销)")
    private Integer verificationStatus;

    /**
    * 处方明细列表
    */
    @ApiModelProperty("处方明细列表")
    private String prescriptionDetailList;

    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public PrescriptionParam() {}
}
