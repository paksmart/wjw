package com.wjw.center.admin.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description complaint_records
 * @author psk
 * @date 2022-09-21
 */
@Data
@ApiModel("complaint_records")
public class ComplaintRecordsVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * 主键
     */
    private Long id;

    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 投诉建议id
    */
    @ApiModelProperty("投诉建议id")
    private String complaintId;

    /**
    * 投诉建议的业务类型(业务字典)
    */
    @ApiModelProperty("投诉建议的业务类型(业务字典)")
    private Integer complaintType;

    /**
    * 投诉建议内容
    */
    @ApiModelProperty("投诉建议内容")
    private String content;

    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public ComplaintRecordsVO() {}
}
