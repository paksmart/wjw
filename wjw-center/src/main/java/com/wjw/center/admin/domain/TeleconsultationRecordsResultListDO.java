package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description teleconsultation_records_result_list
 * @author psk
 * @date 2022-09-29
 */
@Data
@TableName("tst_teleconsultation_records_result_list")
@ApiModel("诊疗管理:互联网医院远程会诊记录监管(会诊结果列表)")
public class TeleconsultationRecordsResultListDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
    * 会诊机构编码
    */
    @ApiModelProperty("会诊机构编码")
    private String orgCode;

    /**
    * 会诊机构名称
    */
    @ApiModelProperty("会诊机构名称")
    private String orgName;

    /**
    * 会诊机构科目编码(科目字典)
    */
    @ApiModelProperty("会诊机构科目编码(科目字典)")
    private String subjectCode;

    /**
    * 会诊机构科目名称
    */
    @ApiModelProperty("会诊机构科目名称")
    private String subjectName;

    /**
    * 会诊机构医生科室id
    */
    @ApiModelProperty("会诊机构医生科室id")
    private String deptId;

    /**
    * 会诊机构医生科室名称
    */
    @ApiModelProperty("会诊机构医生科室名称")
    private String deptName;

    /**
    * 会诊机构医生id
    */
    @ApiModelProperty("会诊机构医生id")
    private String docId;

    /**
    * 会诊机构医生名称
    */
    @ApiModelProperty("会诊机构医生名称")
    private String docName;

    /**
    * 会诊机构医生证件(证件类型字典)
    */
    @ApiModelProperty("会诊机构医生证件(证件类型字典)")
    private Integer docCertType;

    /**
    * 会诊机构医生证件号
    */
    @ApiModelProperty("会诊机构医生证件号")
    private String docCertNo;

    /**
    * 会诊机构医生手机号
    */
    @ApiModelProperty("会诊机构医生手机号")
    private String docMobile;

    /**
    * 会诊机构医生职称(职称字典)
    */
    @ApiModelProperty("会诊机构医生职称(职称字典)")
    private String docTitle;

    /**
    * 会诊地址
    */
    @ApiModelProperty("会诊地址")
    private String address;

    /**
    * 会诊结果
    */
    @ApiModelProperty("会诊结果")
    private String opinion;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill =FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除(0:否，1:是)
    */
    @ApiModelProperty("是否删除(0:否，1:是)")
    private Integer isDeleted;

    public TeleconsultationRecordsResultListDO() {}
}
