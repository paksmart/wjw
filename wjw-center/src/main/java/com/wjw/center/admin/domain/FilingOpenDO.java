package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description filing_open
 * @author psk
 * @date 2022-09-17
 */
@Data
@TableName("tst_filing_open")
@ApiModel("备案信息:医疗人员开通备案")
public class FilingOpenDO implements Serializable {

    private static final long serialVersionUID = 1L;


//    /**
//     * 平台机构主键
//     */
//    @ApiModelProperty("平台机构主键")
//    private Long id;

    /**
    * 平台机构id
    */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("平台机构id")
    private Long platformId;

    /**
    * 待开通业务的医疗人员id
    */
    @ApiModelProperty("待开通业务的医疗人员id")
    private Long  staffId;

    /**
    * 待开通业务的医疗人员
    */
    @ApiModelProperty("待开通业务的医疗人员")
    private String staffName;

    /**
    * 待开通的医疗人员证件类型(证件类型字典)
    */
    @ApiModelProperty("待开通的医疗人员证件类型(1:身份证,2:护照,3:军官证,4:台胞证,5:同乡证,99:其他)")
    private Integer staffCertType;

    /**
    * 待开通的医疗人员证件号
    */
    @ApiModelProperty("待开通的医疗人员证件号")
    private String staffCertNo;

    /**
    * 开通业务类型(业务类型字典(多个用统一分隔字符隔开)
    */
    @ApiModelProperty("开通业务类型(10:挂号,20:图文咨询,21:语音咨询,22:视频咨询,30:在线复诊,31:在线处方,32:药师审方,40:检查检验预约,41:体检预约,42:住院预约,50:在线签约,60:护理业务,70:远程门诊,71:远程会诊,72:远程影像,73:远程心电,74:远程病理,75:远程转诊,100:其他(多个用统一分隔字符隔开)")
    private String businessType;

    /**
    * 是否备案(0:不备案，1:备案)
    */
    @ApiModelProperty("是否备案(0:不备案，1:备案)")
    private Integer filingMark;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新时间(yyyy-mm-dd hh:mm:ss)
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("更新时间(yyyy-mm-dd hh:mm:ss)")
    private Date updateTime;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
    * 修改人
    */
    @ApiModelProperty("修改人")
    private Long updateUser;

    /**
    * 是否删除
    */
    @TableLogic
    @ApiModelProperty("是否删除")
    private Integer isDeleted;

    public FilingOpenDO() {}
}
