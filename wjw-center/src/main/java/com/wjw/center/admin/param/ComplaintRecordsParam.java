package com.wjw.center.admin.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description complaint_records
 * @author psk
 * @date 2022-09-21
 */
@Data
@ApiModel("质量信息:互联网医院投诉建议记录")
public class ComplaintRecordsParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 平台机构id
    */
    @ApiModelProperty("平台机构id")
    private String platformId;

    /**
    * 投诉建议id
    */
    @ApiModelProperty("投诉建议id")
    private String complaintId;

    /**
    * 投诉建议的业务类型(业务字典)
    */
    @ApiModelProperty("投诉建议的业务类型(业务字典)")
    private Integer complaintType;

    /**
    * 投诉建议内容
    */
    @ApiModelProperty("投诉建议内容")
    private String content;

    public ComplaintRecordsParam() {}
}
