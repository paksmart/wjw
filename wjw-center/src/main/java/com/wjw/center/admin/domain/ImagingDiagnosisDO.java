package com.wjw.center.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author psk
 * @description imaging_diagnosis
 * @date 2022-09-26
 */
@Data
@TableName("tst_imaging_diagnosis")
public class ImagingDiagnosisDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * 平台机构id
     */
    private Long platformId;

    /**
     * 远程影像诊断id
     */
    private Long imagingDiagnosisId;

    /**
     * 就诊人简要病史描述
     */
    private String diseasesHistory;

    /**
     * 初步诊断编码(会诊前初步诊断编码，如有多条，使用统一分隔符分隔，icd编码)
     */
    private String diagianCode;

    /**
     * 初步诊断名称(会诊前初步诊断名称，如有多条，使用统一分隔符分隔)
     */
    private String diagianName;

    /**
     * 会诊目的(申请医生就诊人目前存在问题提出诊断要达到的目的)
     */
    private String purpose;

    /**
     * 就诊人病历描述
     */
    private String patientCaseDesc;

    /**
     * 影像诊断检查类别代码(x，ct，mri，dsa，b，详见影像诊断检查类别字典)
     */
    private String imagingType;

    /**
     * 影像诊断检查类别名称
     */
    private String imagingName;

    /**
     * 检查身体部位
     */
    private String bodyParts;

    /**
     * 诊断申请时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date applyDate;

    /**
     * 诊断报告时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date reportDate;

    /**
     * 影像描述
     */
    private String imagingDesc;

    /**
     * 影像诊断意见
     */
    private String imagingOpinion;

    /**
     * 是否支付(0:未支付，1:已支付，2:已退款)
     */
    private Integer payStatus;

    /**
     * 支付方式(1:支付宝，2:微信，3:银联，9:其他)
     */
    private Integer payMode;

    /**
     * 影像诊断价格(保留两位小数)
     */
    private Double imagingPrice;

    /**
     * 影像dicom文件下载地址
     */
    private String fileUrl;

    /**
     * 申请机构名称
     */
    private String applyOrgName;

    /**
     * 申请机构编码
     */
    private String applyOrgCode;

    /**
     * 申请机构科目编码(科目字典)
     */
    private String applySubjectCode;

    /**
     * 申请机构科目名称
     */
    private String applySubjectName;

    /**
     * 申请机构科室编码
     */
    private String applyDeptId;

    /**
     * 申请机构科室名称
     */
    private String applyDeptName;

    /**
     * 申请医生id
     */
    private Long applyDocId;

    /**
     * 申请医生姓名
     */
    private String applyDocName;

    /**
     * 申请医生手机号
     */
    private String applyDocMobile;

    /**
     * 申请医生证件类型(证件类型字典)
     */
    private Integer applyDocCertType;

    /**
     * 申请医生证件号
     */
    private String applyDocCertNo;

    /**
     * 申请医生职称(职称字典)
     */
    private String applyDocTitle;

    /**
     * 接收申请列表
     */
    @TableField(exist = false)
    private List<ImagingDiagnosisAppliedListDO> appliedList = new ArrayList<>();

    /**
     * 就诊人id
     */
    private Long patientId;

    /**
     * 就诊人姓名
     */
    private String patientName;

    /**
     * 就诊人证件类型(证件类型字典)
     */
    private Integer patientCertType;

    /**
     * 就诊人身份证
     */
    private String patientCertNo;

    /**
     * 就诊人电子健康卡
     */
    private String patientHealthCard;

    /**
     * 就诊人年龄
     */
    private Integer patientAge;

    /**
     * 就诊人性别(性别字典)
     */
    private Integer patientSex;

    /**
     * 就诊人电话
     */
    private String patientMobile;

    /**
     * 就诊人生日(yyyy-mm-dd)
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date patientBirthday;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间(yyyy-mm-dd hh:mm:ss)
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 修改人
     */
    private Long updateUser;

    /**
     * 是否删除(0:否，1:是)
     */
    @TableLogic
    private Integer isDeleted;

    public ImagingDiagnosisDO() {
    }
}
